package com.ctc.people.cfm.testfixtures;

public class Fixture {
	
	
	public static String getSampleActivityDocsJsonString(){
		return "{activityDocs:["
				+ "{"+
				"\"whoMap\":"+
				"{ \"0032800000dt5ZZAAY\": \"Document_Related_To__c\" },"+
				"\"whatMap\":"+
				"{ \"a0N2800000AnDchEAF\": \"Vacancy__c\" },"+
				"\"skillGroupIds\": "
				+ "["+
					"\"G000003\","+
					"\"G000004\","+
					"\"G000005\""+
				"],"+
				"\"resourceType\": \"Attachment\","+
				"\"resourceName\": \"Ameer Khalaf cv.docx\","+
				"\"resourceId\": \"00P2800000F6aIGEAZ\","+
				"\"orgId\": \"00D28000001u4NZ\","+
				"\"nameSpace\": \"\","+
				"\"isSelected\": true,"+
				"\"isEmailService\": null,"+
				"\"enableSkillParsing\": true,"+
				"\"docType\": \"Resume\","+
				"\"attachmentName\": null,"+
				"\"attachmentId\": null,"+
				"\"amazonS3Folder\": \"temp003\","+
				"\"userId\": \"andy12345\","+
				
				"\"activityId\": \"00T2800000risfEEAQ\""+
				"}"
		+ "]}";
	}
	
	public static String getSampleActivityDocsJsonString2(){
		return "{\"activityDocs\":[{\"whoMap\":{\"PeopleCloud1__Document_Related_To__c\":\"0032800000g67H6AAI\"},\"whatMap\":{\"PeopleCloud1__Account__c\":\"00128000011AkdJAAS\",\"Candidate_Management__c\":\"a0S28000003yOLmEAM\",\"Vacancy__c\":\"a0T28000008VsKmEAK\"},\"resourceType\":\"Attachment\",\"resourceName\":\"Shailey Gandhi.pdf\",\"resourceId\":\"00P2800000HHtydEAD\",\"orgId\":\"00D28000000aqIL\",\"nameSpace\":\"PeopleCloud1__\",\"docType\":\"Other Document\",\"amazonS3Folder\":\"temp003\"}]}";
	}

}
