package com.ctc.people.cfm.adaptor.salesforce;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.DeleteResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;

public class SfHelperTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void test() throws ConnectionException {
//		SforgEx org = SfHelper.getSfOrg("00D28000000TdHk");
//		assertNotNull(org);
		
		PartnerConnection c = SalesforceAgent.getConnectionByOAuth("00D28000000TdHk");
		System.out.println(c.getUserInfo().getUserName());
	}
	
	@Test public void testDeleteResource(){
		
		try {
			PartnerConnection c = SalesforceAgent.getConnectionByOAuth("00D28000000TdHk");
			System.out.println(c.getUserInfo().getUserName());
			DeleteResult[] srsTmp  = c.delete(new String[]{"0690I000009UY3tQAG"});
			for (int i = 0; i < srsTmp.length; i++) {
				if (!srsTmp[i].isSuccess()) {
					
					com.sforce.soap.partner.Error[] errors = srsTmp[i].getErrors();
					for (com.sforce.soap.partner.Error error : errors) {
						System.out.println(error);
						//logger.error("[ " + results[i].getId() +" ] - " + error.getMessage());
					}
				}
			}
			
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//https://ap7.lightning.force.com/one/one.app#/sObject/ContentDocument/0690I000009UY2RQAW
		//https://ap7.lightning.force.com/one/one.app#/sObject/ContentDocument/0690I000009UY3tQAG
		
	}
	
	@Test
	public void testGetRecordType(){

		try {
			PartnerConnection c = SalesforceAgent.getConnectionByOAuth("00D28000000TdHk");
			System.out.println(c.getUserInfo().getUserName());
			String rid = SfHelper.getRecordTypeId(c, Constant.DEFAULT_CONTACT_RECORD_TYPE);
			System.out.println(rid);
			
			c = SalesforceAgent.getConnectionByOAuth("00D90000000gtFz");
			System.out.println(c.getUserInfo().getUserName());
			rid = SfHelper.getRecordTypeId(c, Constant.DEFAULT_CONTACT_RECORD_TYPE);
			System.out.println(rid);
			
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	@Test
	public void testGetCTCAdminAccountId(){

		try {
			PartnerConnection c = SalesforceAgent.getConnectionByOAuth("00D28000000TdHk");
			System.out.println(c.getUserInfo().getUserName());
			String rid = SfHelper.getCTCAdminAccountId(c);
			System.out.println(rid);
			
			c = SalesforceAgent.getConnectionByOAuth("00D90000000gtFz");
			System.out.println(c.getUserInfo().getUserName());
			rid = SfHelper.getCTCAdminAccountId(c);
			System.out.println(rid);
			
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
