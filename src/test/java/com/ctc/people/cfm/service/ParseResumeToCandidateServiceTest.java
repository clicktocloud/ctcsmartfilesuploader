package com.ctc.people.cfm.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.ParseResumeToCandidateRequest;
import com.ctc.people.cfm.entity.ParseResumeToCandidateResponse;

public class ParseResumeToCandidateServiceTest {
	
	public static List<String> defaultSkillGroups = Arrays.asList(new String[]{"G000001","G000002","G000003","G000004","G000005","G000006","G000007","G000008","G000009","G000010","G000011"});
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseToCandidateByRequest() throws FileNotFoundException, IOException {
		ParseResumeToCandidateRequest request  = new ParseResumeToCandidateRequest();
        
        request.setBucketName("temp003");
        request.setDocType(Constant.RESUME_DOC_TYPE);
        request.setEnableSkillParsing(true);
        //request.setNsPrefix("");
        request.setOrgId( "00D90000000gtFz");
        request.setSkillGroupIds(defaultSkillGroups);
        request.setWhoId( "" );
        
        String filename = "AndysResumeNew.docx";
        request.setAttachmentName(filename);
        
        File file = new File(Constant.ATTACHMENT_FOLDER + filename);
        byte[] content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
        request.setAttachment(content);
        
        ParseResumeToCandidateService service = new ParseResumeToCandidateService();
		ParseResumeToCandidateResponse response = service.parseToCandidate( request );
		
		System.out.println("Candidate Id : " + response.getCandidateId());
		
		assertNotNull(response.getCandidateId());
	}

}
