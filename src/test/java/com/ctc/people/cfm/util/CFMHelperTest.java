package com.ctc.people.cfm.util;

import static com.ctc.people.cfm.testfixtures.Fixture.getSampleActivityDocsJsonString;
import static com.ctc.people.cfm.testfixtures.Fixture.getSampleActivityDocsJsonString2;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.ActivityDocs;

public class CFMHelperTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetValueFromPropertiesFile(){
		assertEquals("/Users/andy/Downloads/ctcenhanced/",CTCFileUtils.getValueFromPropertyFile("tempFolder", "information.properties"));
		assertEquals("/Users/andy/Downloads/ctcenhanced/",CFMHelper.getValueFromPropertyFile("tempFolder", "information.properties"));
	}

	@Test
	public void test() {
		String jsonString = getSampleActivityDocsJsonString();
		
		ActivityDocs docs = CFMHelper.fetchActivityDocsFromJson(jsonString);
		assertNotNull(docs);
		assertEquals(1, docs.getActivityDocs().size());
		assertEquals("Attachment",docs.getActivityDocs().get(0).getResourceType());
		
		assertEquals("Ameer Khalaf cv.docx",docs.getActivityDocs().get(0).getResourceName());
		assertEquals("temp003",docs.getActivityDocs().get(0).getAmazonS3Folder());
		assertEquals("andy12345",docs.getActivityDocs().get(0).getUserId());
		System.out.println(docs.getActivityDocs().get(0).getUserId());
		
		assertNull(docs.getActivityDocs().get(0).getAttachmentId());
	}
	
	@Test
	public void test2() {
		String jsonString = getSampleActivityDocsJsonString2();
		
		ActivityDocs docs = CFMHelper.fetchActivityDocsFromJson(jsonString);
		assertNotNull(docs);
		assertEquals(1, docs.getActivityDocs().size());
		
		assertEquals("temp003",docs.getActivityDocs().get(0).getAmazonS3Folder());
		
		assertNull(docs.getActivityDocs().get(0).getAttachmentId());
	}
	
	

}
