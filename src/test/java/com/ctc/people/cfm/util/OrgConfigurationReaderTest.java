package com.ctc.people.cfm.util;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;


public class OrgConfigurationReaderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testGetPartnerConnection_InSalesforceConnectionHelper() throws ConnectionException {
		//PartnerConnection conn = SalesforceAgent.getConnectionByOAuth("00D90000000gtFz");
		
		
		PartnerConnection conn = SalesforceAgent.getConnectionByOAuth("00D0l0000000Sd0");
		
		System.out.println(conn.getUserInfo().getUserId());
	}
	
	@Ignore
	@Test
	public void testBuild(){
		SalesforceAgent.build("00D90000000gtFz","");
	}

}
