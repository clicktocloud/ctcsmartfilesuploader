package com.ctc.people.cfm.extension.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;

public class CandidateFilesServiceTest {
	public static List<String> defaultSkillGroups = Arrays.asList(new String[]{"G000001","G000002","G000003","G000004","G000005","G000006","G000007","G000008","G000009","G000010","G000011"});
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMake() throws FileNotFoundException, IOException {
		CTCFilesOwner owner = new CTCFilesOwner();
		
		//test for nest org
		owner.setOrgId("00D90000000eaLJ");
		owner.setWhoId("0039000002AyE3w");
		owner.setBucketName("ctctestinstance");
		owner.setNamespace("PeopleCloud1__");
		
		//test for test org
		//owner.setOrgId("00D28000000TdHk");
		//owner.setBucketName("temp003");
		
		
		//owner.setUserId("0052800000646Es");
		
		
		owner.setEnableSkillParsing(true);
		owner.setSkillGroups(defaultSkillGroups);
		owner.setWhoIdField(  "PeopleCloud1__"+Constant.WEB_DOC_FIELD_WHO_ID);
		
		
		
		CTCFile ctcfile = new CTCFile();
		
		ctcfile.setOwner(owner);
		ctcfile.setOriginalFileName("AndysResumeNew.docx");
		ctcfile.setType(Constant.RESUME_DOC_TYPE);
		File file = new File(Constant.ATTACHMENT_FOLDER + ctcfile.getOriginalFileName());
        byte[] content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
		ctcfile.setFileContent(content);
        
		owner.getFiles().add(ctcfile);
		
		ctcfile = new CTCFile();
		ctcfile.setOwner(owner);
		ctcfile.setOriginalFileName("AndysAttachment-01.csv");
		ctcfile.setType(Constant.OTHER_DOC_TYPE);

		file = new File(Constant.ATTACHMENT_FOLDER + ctcfile.getOriginalFileName());
		content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
		ctcfile.setFileContent(content);
        
		owner.getFiles().add(ctcfile);
		
		
		ctcfile = new CTCFile();
		ctcfile.setOwner(owner);
		ctcfile.setOriginalFileName("AndysAttachment-02.xlsx");
		ctcfile.setType(Constant.OTHER_DOC_TYPE);

		file = new File(Constant.ATTACHMENT_FOLDER + ctcfile.getOriginalFileName());
		content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
		ctcfile.setFileContent(content);
        
		owner.getFiles().add(ctcfile);
		
        try{
        	CandidateFilesService.make(owner);
        	System.out.println( owner.getWhoId());
        	assertNotNull( owner.getWhoId());
        }catch(Exception e){
        	fail("Failed to make candidate files");
        }
		
	}

}
