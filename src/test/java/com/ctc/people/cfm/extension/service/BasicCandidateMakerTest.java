package com.ctc.people.cfm.extension.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;

public class BasicCandidateMakerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		

		
		CTCFilesOwner filesOwner = new CTCFilesOwner();
		filesOwner.setOrgId("00D90000000gtFz");
		filesOwner.setWhoId("0039000002BkYqT");
		//filesOwner.setUserId(userId);
		List<CTCFile> files = new ArrayList<CTCFile>();
		filesOwner.setFiles(files);
		
		CTCFile file = new CTCFile();
		file.setOwner(filesOwner);
		file.setOriginalFileName("AndysResumeNew_test.docx");
		file.setNewFileName("AndysResumeNew_14933565717533390.docx");
		file.setType(Constant.RESUME_DOC_TYPE);
		files.add(file);
		
		
		
		BasicCandidateMaker.make(Arrays.asList(new CTCFilesOwner[]{filesOwner}));
	}

}
