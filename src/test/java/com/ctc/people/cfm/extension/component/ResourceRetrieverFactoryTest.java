package com.ctc.people.cfm.extension.component;
import static com.ctc.people.cfm.testfixtures.Fixture.getSampleActivityDocsJsonString;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.ActivityDocs;
import com.ctc.people.cfm.util.CFMHelper;

public class ResourceRetrieverFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetRetrieverInstancePartnerConnectionArrayListOfActivityDocString() {
		ArrayList<ActivityDoc> docs = new ArrayList<ActivityDoc>();
		ActivityDoc doc = new ActivityDoc();
		doc.setAttachmentId("testid");
		docs.add(doc);
		
		ResourceRetriever r = ResourceRetrieverFactory.getRetrieverInstance(null, docs, null);
		assertTrue(r instanceof AttachmentRetriever);
		
		//
		docs.remove(doc);
		doc = new ActivityDoc();
		doc.setResourceType(ResourceSelector.FILE_RESOURCE);
		doc.setResourceId("test");
		
		docs.add(doc);
		r = ResourceRetrieverFactory.getRetrieverInstance(null, docs, null);
		assertTrue(r instanceof FileRetriever);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetRetrieverInstanceStringPartnerConnectionArrayListOfActivityDocString() {
		ResourceRetriever r = ResourceRetrieverFactory.getRetrieverInstance(ResourceSelector.ATTACHMENT_RESOURCE,null, null, null);
		assertTrue(r instanceof AttachmentRetriever);
		
		r = ResourceRetrieverFactory.getRetrieverInstance(ResourceSelector.FILE_RESOURCE,null, null, null);
		assertTrue(r instanceof FileRetriever);
		
		r = ResourceRetrieverFactory.getRetrieverInstance(ResourceRetrieverFactory.BULKATTACHMENTS_RESOURCE,null, null, null);
		assertTrue(r instanceof BulkAttachmentsRetriever);
		
		r = ResourceRetrieverFactory.getRetrieverInstance("other",null, null, null);
		assertNull(r );
		
		r = ResourceRetrieverFactory.getRetrieverInstance("",null, null, null);
		assertNull(r );
		
	}
	
	@SuppressWarnings("deprecation")
	@Test 
	public void testGetRetrieverInstance(){
		ActivityDocs docs = CFMHelper.fetchActivityDocsFromJson(getSampleActivityDocsJsonString());
		ResourceRetriever r = ResourceRetrieverFactory.getRetrieverInstance(null, docs.getActivityDocs(), null);
		assertTrue(r instanceof AttachmentRetriever);
	}

}
