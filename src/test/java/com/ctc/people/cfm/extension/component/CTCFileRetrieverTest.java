package com.ctc.people.cfm.extension.component;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.people.cfm.adaptor.aws.s3.FileDownloader2S3;
import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.component.CTCFileRetriever;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.SforgEx;

public class CTCFileRetrieverTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		SforgEx sforg = SfHelper.getSfOrg("00D28000001G335");
		
		CTCFilesOwner filesOwner = new CTCFilesOwner();
		List<CTCFile> files = new ArrayList<CTCFile>();
		filesOwner.setFiles(files);
		
		CTCFile file = new CTCFile();
		file.setOwner(filesOwner);
		file.setNewFileName("AndysResumeNew_14933565717533390.docx");
		file.setType(Constant.RESUME_DOC_TYPE);
		files.add(file);
		
		FileDownloader2S3.download(sforg, files);
		
		CTCFileRetriever fileRtrv = new CTCFileRetriever(
				filesOwner, Constant.ATTACHMENT_FOLDER);
		fileRtrv.retrieve();
		
		CTCFile resume = fileRtrv.getResume();
		
		assertTrue(new File(resume.getPath()).exists());
	}

}
