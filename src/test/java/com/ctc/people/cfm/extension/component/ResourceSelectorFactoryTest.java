package com.ctc.people.cfm.extension.component;
import static com.ctc.people.cfm.testfixtures.Fixture.getSampleActivityDocsJsonString;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.ActivityDocs;
import com.ctc.people.cfm.util.CFMHelper;

public class ResourceSelectorFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSelectorInstancePartnerConnectionActivityDocString() {
		
		ActivityDoc doc = new ActivityDoc();
		doc.setResourceType(ResourceSelector.ATTACHMENT_RESOURCE);
		
		ResourceSelector rs = ResourceSelectorFactory.getResourceSelector(null, doc, "");
		assertTrue(rs instanceof AttachmentSelector);
		
		
		doc.setResourceType(ResourceSelector.FILE_RESOURCE);
		
		rs = ResourceSelectorFactory.getResourceSelector(null, doc, "");
		assertTrue(rs instanceof FileSelector);
	}

	
	@Test 
	public void testGetRetrieverInstance(){
		ActivityDocs docs = CFMHelper.fetchActivityDocsFromJson(getSampleActivityDocsJsonString());
		ResourceSelector rs = ResourceSelectorFactory.getResourceSelector(null, docs.getActivityDocs().get(0), "");
		assertTrue(rs instanceof AttachmentSelector);
		
	}

}
