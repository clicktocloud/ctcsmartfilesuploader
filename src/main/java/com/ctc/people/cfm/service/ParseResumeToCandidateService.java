package com.ctc.people.cfm.service;


import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.adaptor.other.FilesRemover;
import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.component.SingleCandidateParseRunner;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.ParseResumeToCandidateRequest;
import com.ctc.people.cfm.entity.ParseResumeToCandidateResponse;
import com.ctc.people.cfm.monitor.CandidateImportMonitor;
import com.ctc.people.cfm.util.CFMHelper;
import com.ctc.people.cfm.util.UploadedFile;

/**
 * This class is responsible for running a thread of SingleCandidateParseRunner to parse a resume file to Candidate object,
 * and monitor the status of all threads and remove the temp file of resume after all threds end .
 * 
 * For more details , please refer to {@link com.ctc.people.cfm.component.SingleCandidateParseRunner}
 * 
 * @author andy 
 */
public class ParseResumeToCandidateService {
	
	 private static Logger logger = Logger.getLogger("com.ctc.people");
  
    //private Map<String, HashMap<String, String>> errorMsgMap = new HashMap<String, HashMap<String,String>>();	// to store error messages
    //private List<Thread> threadList = new ArrayList<Thread>();
    //private Map<String, Candidate> successfulCandidateMap = new HashMap<String, Candidate>();

    /**
	 * Parse a request to candidate
	 *  - Only for remotely being invokied via http
	 *  
	 * @param Request - ParseResumeToCandidateRequest
	 * @return ParseResumeToCandidateResponse
	 */
    public ParseResumeToCandidateResponse parseToCandidate(ParseResumeToCandidateRequest eServiceRequest) {
		
		//convert ParseResumeToCandidateRequest object to CTCFile object
		logger.info("Convert request to CTCFile object.");
		CTCFile resume = createCTCFile( eServiceRequest );
		
		if( resume == null ){
			logger.info("Failed to convert request to CTCFile object");
			return null;
		}
    	
		//invoke 
		return parseToCandidate( resume );
    }
    
	/**
	 * Launch a thread to parse a resume file to candidate
	 * 
	 * @param resume - CTCFile
	 * @return ParseResumeToCandidateResponse
	 */
	 public ParseResumeToCandidateResponse parseToCandidate(CTCFile resume) {
		 
		 CandidateImportMonitor importObserver = new CandidateImportMonitor();

	    	
        ParseResumeToCandidateResponse response = new ParseResumeToCandidateResponse();
       
        String orgId = resume.getOwner().getOrgId();

        try{
        	if(resume.getSforg() == null){
        		logger.info("Get the configration of Org ["+orgId+"]");
        		resume.setSforg( SfHelper.getSfOrg( orgId ));
        	}
        	
        	//start a thread to run the SingleCandidateParseRunner
            SingleCandidateParseRunner isc = new SingleCandidateParseRunner(resume ,importObserver);
            Thread isct = new Thread(isc);
            isct.setName("importSingleCanidateThread_" + CTCFileUtils.getFileName(resume.getPath()));

            // add the thread created to threadList
            importObserver.addThread(isct);
            isct.start();
            
            //Scan the status of thread running
            logger.info("Scan the status of thread running");
  
            
        }
        catch(Throwable e) {
            logger.error("Failed to run SingleCandidateParseRunner thread !"+ e);
            importObserver.addErrorMsg(resume.getNewFileName(), CandidateImportMonitor.OTHER_ERROR, e.getMessage());
        }finally{
        	
        	 CandidateImportMonitor.scanAllThread(importObserver.getThreadList());
        	 
        	 //remove the temp resume file
        	 FilesRemover.remove(Arrays.asList(new CTCFile[]{ resume }));
        	 
        	//set return message to reponse
             logger.info("Set return messeage to response");
             response = importObserver.buildParseResumeToCandidateResponse( );
             
             importObserver.clear();

            
        }
        
        return response;
        
        
    }
    
	 /**
	  * Convert ParseResumeToCandidateRequest object to CTCFile object
	  * 
	  * @param eServiceRequest - ParseResumeToCandidateRequest
	  * @return CTCFile
	  * @throws Exception
	  */
	private CTCFile createCTCFile( ParseResumeToCandidateRequest eServiceRequest ) {
		CTCFile ctcFile = null;
		
		try {
			String userId = eServiceRequest.getUserId();
			String orgId = eServiceRequest.getOrgId() ;
	    	String whoId = eServiceRequest.getWhoId();
			byte[] bByte = eServiceRequest.getAttachment();
		    String bucketName = eServiceRequest.getBucketName();
		    String namespacePrefix = eServiceRequest.getNsPrefix();
	        List<String> skillGroups = eServiceRequest.getSkillGroupIds();
	        boolean isEnableSkillParsing = eServiceRequest.isEnableSkillParsing();
	        
	        String originalFileName = eServiceRequest.getAttachmentName();
	        
	        String newFileName = eServiceRequest.getAttachmentNameWithTimeStamp();
	        String path  = "";
	        if(StringUtils.isEmpty( newFileName )){
	        	File tempfile = CFMHelper.generateUploadedFile(bByte, originalFileName);
	        	path = tempfile.getAbsolutePath();
	        	newFileName = CTCFileUtils.getFileName(path);
	        	
	        }else{
	        	path = Constant.ATTACHMENT_FOLDER + newFileName;
	        }
	        
	       // System.out.println("namespace is null :" + (namespacePrefix==null));
	
	        //build SfWebDoc object to hold the informations of uploaded resume file
	        ctcFile = new CTCFile();
	        ctcFile.setOwner( new CTCFilesOwner() );
	        ctcFile.getOwner().getFiles().add( ctcFile );
	        ctcFile.getOwner().setUserId(userId);
	        ctcFile.getOwner().setOrgId( orgId );
	        ctcFile.getOwner().setNamespace( namespacePrefix ); 
	        ctcFile.getOwner().setEnableSkillParsing(isEnableSkillParsing);
	        ctcFile.getOwner().setSkillGroups(skillGroups);
	        ctcFile.setOriginalFileName(originalFileName);
	        ctcFile.setNewFileName(newFileName);
	        ctcFile.setPath( path );
	        ctcFile.getOwner().setBucketName( bucketName );
	        
	        ctcFile.getOwner().setWhoId(whoId);//
	       
	        String whoIdField = Constant.WEB_DOC_FIELD_WHO_ID;
	        if(!StringUtils.isEmpty(namespacePrefix)){
	        	whoIdField =namespacePrefix + Constant.WEB_DOC_FIELD_WHO_ID;
	        }
	        ctcFile.getOwner().setWhoIdField( whoIdField);
	        //ctcFile.setWhatId(..);
	        //candidateFile.setWhatIdField(..);
	        ctcFile.setType( Constant.RESUME_DOC_TYPE );
	        
	        ctcFile.setSize(UploadedFile.calculateDisplayFileSize(path));
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Fail to convert request to CTCFile object");
		}
	
	    return ctcFile;
    }

   
}
