package com.ctc.people.cfm.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.aws.s3.FileUploader2S3;
import com.ctc.people.cfm.adaptor.other.FilesRemover;
import com.ctc.people.cfm.adaptor.salesforce.ResourceDeleter;
import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.adaptor.salesforce.WebDocCreator;
import com.ctc.people.cfm.component.CTCFileRetriever;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.ParseResumeToCandidateResponse;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.FilesUploadingException;
import com.ctc.people.cfm.util.CFMHelper;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;

/**
 * This class works as common component of uploading the files of various owner such as Contact, Client  to S3 and Salesforce 
 * which accepts the standard data package of CTCFilesOwner with a list of files which will be uploaded 
 * and do the series of actions as below:
 * 
 * 1. Extract all files of current CTCFilesOwner data object, try to find out the resume file if existing 
 * 	 and create temp physical files on local file system if they does not exist.
 * 2. (optional) If resume file is found , then parser resume to candidate details, load resume to s3 and save candidate details to salesforce
 * 3. Upload non resume files to S3
 * 4. Save non resume files to web document object of Salesforce 
 * 5. Remover all temp files from local file system
 * 
 * 
 *  
 * @author andy
 *
 */
public class CommonFilesMaker {
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	public static void make(CTCFilesOwner owner){
		if(owner == null)
			return;
		
		SforgEx sforg = SfHelper.getSfOrg(owner.getOrgId());
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ owner.getOrgId() +" ], return back.");
			return;
		}
		
		make(owner, sforg);
		
	}
	public static void  make(CTCFilesOwner owner, SforgEx sforg){
		new CommonFilesMaker(owner, sforg).make();
		
	}
	
	
	private CTCFilesOwner owner;
	private SforgEx sforg;
	
	/**
	 * Constructor 
	 * @param owner
	 * @param sforg
	 */
	public CommonFilesMaker(CTCFilesOwner owner, SforgEx sforg) {
		this.owner = owner;
		this.sforg = sforg;	
	}
	
	/**
	 * 
	 * @throws FilesUploadingException
	 */
	public void make() throws FilesUploadingException{
		
		//logger.info(this.getClass().getName() + " Maker is running ...");
		
		
		String orgId = owner.getOrgId();
		
		
		logger.debug("\t orgId=" + orgId);
		

		// get connection to client's instance
		PartnerConnection conn = sforg.getConn();
		if( conn == null ){
			conn = SfHelper.login( orgId );
			sforg.setConn(conn);
		}
		
		if( conn == null ){
			logger.error("Failed to get connection to Salesforce instance [ " + orgId +" ]");
			throw new FilesUploadingException( "Failed to get connection to Salesforce instance [ " + orgId +" ]" ) ;
		}
		
		String namespace = owner.getNamespace();
		if(namespace == null){
			namespace = SalesforceAgent.getNamespace(orgId);
			owner.setNamespace(namespace);
		}
		
		logger.debug("\t namespace=" + namespace);	

		List<CTCFile> files = null;
		
		try {
			/*
			 * STEP 1:  
			 * Extract all files of current CTCFilesOwner data object, try to find out the resume file if existing 
			 * and create temp physical files on local file system if they does not exist
			 */
			CTCFileRetriever fileRtrv = new CTCFileRetriever(
					owner, Constant.ATTACHMENT_FOLDER);
			fileRtrv.retrieve();
			files = fileRtrv.getFiles();
			CTCFile resume = fileRtrv.getResume();

			if( files == null || files.size() == 0 ){
				logger.error(orgId + ": Failed to retrieve any valid file, abort to make file!");
				throw new FilesUploadingException( "Failed to retrieve any valid file ! ");
			}
			
			logger.info("Retrieved files from standard data package of CTCFilesOwner object : " + files.size());
			
			//Check if owner has a resume file
			if (resume != null) {
				logger.info("Found a resume:" + resume.getOriginalFileName() +" ( "+resume.getNewFileName()+" )");
				
				logger.debug("\t Type: " + resume.getType());
				logger.debug("\t Orginal name: " + resume.getOriginalFileName());
				logger.debug("\t Real file name: " + resume.getNewFileName());
				logger.debug("\t Temp file path: " + resume.getPath());
				logger.debug("\t Org id: " + resume.getOwner().getOrgId());
				logger.debug("\t Namespace: " + resume.getOwner().getNamespace());
				logger.debug("\t Bucket: " + resume.getOwner().getBucketName());
				logger.debug("\t WhoId : " + resume.getOwner().getWhoId() +" / " + resume.getOwner().getWhoIdField());
			//	logger.debug("\t WhatId : " + resume.getOwner().getWhatId() +" / " + resume.getOwner().getWhatIdField());
				logger.debug("\t Skill groups : " + CFMHelper.generateSkillGroupsString(resume.getOwner().getSkillGroups()));
				logger.debug("\t Enable skill parsing : " + resume.getOwner().isEnableSkillParsing());
				
				logger.debug("\n\n");
				
				logger.info("Begin to parse resume file to candidate ... ");
				
				/*
				 * STEP 2- : parser resume to candidate details, load resume to s3 and save candidate details to salesforce
				 */
				resume.setSforg(this.sforg);
				ParseResumeToCandidateService service = new ParseResumeToCandidateService();
				ParseResumeToCandidateResponse response = service.parseToCandidate( resume );
				
				//get candidate id from response and assign it to owner's WhoId
				if( response.getCandidateId() != null && !response.getCandidateId().equals("")){
					owner.setWhoId( response.getCandidateId() );
				}else{
					throw new FilesUploadingException("Failed to handle resume file "+resume.getOriginalFileName() + " - org[ " + orgId  + " ]");
				}
				
				//attach non resume files to current candidate
				fileRtrv.attachNonResumeFilesTo(owner.getWhoId());
			} 
			
			/*
			 * STEP 3:  upload non resume files to S3
			 */
			FileUploader2S3.upload(sforg, fileRtrv.getNonResumeFiles());
			/*
			 * STEP 4:  save non resume files to web document object of Salesforce 
			 */
			WebDocCreator.create(conn, fileRtrv.getNonResumeFiles());
			
			
			ResourceDeleter.delete(conn, fileRtrv.getNonResumeFiles());
			
		}catch(FilesUploadingException e){
			throw e;
		}catch (Exception e) {	
			logger.error("Failed to handle the files for whoId["+owner.getWhoId()+"] org[ " + orgId  + " ]", e);
			throw new FilesUploadingException( "Failed to handle the files for whoId["+owner.getWhoId()+"]  org[ " + orgId  + " ]", e );
		}finally{
			/*
			 * STEP 5 : remover all temp files from local file system
			 */
			FilesRemover.remove(files);
			logger.info("===== Completed uploading files for whoId["+owner.getWhoId()+"]  org[ " + orgId  + " ] =====");
			
			
		}	
	}
	
	
}


