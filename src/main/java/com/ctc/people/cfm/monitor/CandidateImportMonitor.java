package com.ctc.people.cfm.monitor;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.adaptor.aws.s3.UploadS3Runnable;
import com.ctc.people.cfm.adaptor.salesforce.InsertSfRunnable;
import com.ctc.people.cfm.entity.ParseResumeToCandidateResponse;


public class CandidateImportMonitor implements Observer {
	

	private static Logger logger = Logger.getLogger(CandidateImportMonitor.class);
	
	public static final String PARSER_ERROR = "ParserError";
	public static final String USR_ERROR = "UsrError";
	public static final String SF_ERROR = "SFError";
	public static final String S3_ERROR = "S3Error";
	public static final String NO_EMAIL_ERROR = "NoEmailError";
	public static final String OTHER_ERROR ="OtherError";
	
	public static final String SF_COMPLETED = "sfCompleted";

	public static final String S3_COMPLETED = "s3Completed";
	
	private final static long TIME_TO_INTERUPT_THREAD = 10000000;			// specific time to terminate the thread
	
	private Map<File, Map<String,Boolean>> parseStatus = new HashMap<File, Map<String,Boolean>>();
	private Map<String, HashMap<String, String>> errorMsgMap = new HashMap<String, HashMap<String,String>>();	// to store error messages
    private List<Thread> threadList = new ArrayList<Thread>();
    private Map<String, Candidate> successfulCandidateMap = new HashMap<String, Candidate>();

	/**
	 * 	TopObserver to monitor the following two things:
	 * 	1. resume upload to S3 (by boolean s3Completed)
	 *  2. new candidate inserted to salesforce (by boolean sfCompleted)
	 */
	@Override
	public void update(Observable o, Object obj){
		File filePath = (File)obj;
		
		synchronized (parseStatus) {
			// s3 upload has completed
			if (o instanceof UploadS3Runnable) {
				parseStatus = updateParseStatusMap(filePath, S3_COMPLETED);
			}
			// Sf insertion has completed
			if (o instanceof InsertSfRunnable) {
				parseStatus = updateParseStatusMap(filePath, SF_COMPLETED);
			}
			// both sf and s3 has completed
			if (parseStatus.get(filePath).containsKey(SF_COMPLETED)
					&& parseStatus.get(filePath).containsKey(S3_COMPLETED)) {

				// if everything is done, remove the backup resumes. All resumes with error and warning messages will be saved.
				parseStatus.remove(filePath);

				try {
					filePath.delete();
				} catch (Exception e) {

				}

				logger.info("Both s3upload and salesforce insertion have completed.");
				logger.info("=================================================");

			}
		}
	}
	
	 public ParseResumeToCandidateResponse buildParseResumeToCandidateResponse( ) {
	        // check all error messages generated.
	        String errorCode = "";
	        String cid4URL = "";
	        ParseResumeToCandidateResponse eServiceResponse = new ParseResumeToCandidateResponse();
	        eServiceResponse.setCandidateId("");
	        eServiceResponse.setIsCandidateExist(false);
	        for(String cv: errorMsgMap.keySet()){
	            if(errorMsgMap.get(cv).containsKey(NO_EMAIL_ERROR)){
	                errorCode = "10001";
	                logger.error("Error: " +  errorMsgMap.get(cv).get(NO_EMAIL_ERROR));
	                eServiceResponse.setMessage(errorMsgMap.get(cv).get(NO_EMAIL_ERROR));
	                break;
	            }
	            else if(errorMsgMap.get(cv).containsKey(S3_ERROR)){
	                errorCode = "10002";
	                logger.error("Error: " +  errorMsgMap.get(cv).get(S3_ERROR));
	                eServiceResponse.setMessage(errorMsgMap.get(cv).get(S3_ERROR));
	                break;
	            }
	            else if(errorMsgMap.get(cv).containsKey(SF_ERROR)){
	                errorCode = "10003";
	                logger.error("Error: " +  errorMsgMap.get(cv).get(SF_ERROR));
	                eServiceResponse.setMessage(errorMsgMap.get(cv).get(SF_ERROR));
	                break;
	            }
	            else if(errorMsgMap.get(cv).containsKey(USR_ERROR)){
	                errorCode = "10000";
	                logger.warn("Warn: " +  errorMsgMap.get(cv).get(USR_ERROR));
	                cid4URL = errorMsgMap.get(cv).get(USR_ERROR).split(":")[0];
	                logger.warn("Duplicate Candiate ID: " +  cid4URL);
	                eServiceResponse.setCandidateId(cid4URL);
	                eServiceResponse.setIsCandidateExist(true);
	                eServiceResponse.setMessage(errorMsgMap.get(cv).get(USR_ERROR));
	            }
	            else if(errorMsgMap.get(cv).containsKey(PARSER_ERROR)){
	                errorCode = "10004";
	                logger.error(errorMsgMap.get(cv).get(PARSER_ERROR));
	                eServiceResponse.setMessage(errorMsgMap.get(cv).get(PARSER_ERROR));
	                break;
	            }
	            else if(errorMsgMap.get(cv).containsKey(OTHER_ERROR)){
	                errorCode = "99999";
	                logger.error(errorMsgMap.get(cv).get(OTHER_ERROR));
	                eServiceResponse.setMessage(errorMsgMap.get(cv).get(OTHER_ERROR));
	                break;
	            }
	        }


	        if(!successfulCandidateMap.keySet().isEmpty() && errorCode.isEmpty()){
	            errorCode = "00000";
	            cid4URL = successfulCandidateMap.keySet().toArray()[0].toString();
	            logger.info("Successful Candidate ID: " +  cid4URL);
	            eServiceResponse.setCandidateId(cid4URL);
	            eServiceResponse.setMessage("Successful Candidate ID: " +  cid4URL);
	        }else if(errorCode.isEmpty()){
	            logger.error("Some unexpected error occurred");
	            errorCode = "99999";
	            eServiceResponse.setMessage("Some unexpected error occurred." );
	        }
	        
	        eServiceResponse.setCode(errorCode);

	        return eServiceResponse;
	    }
	
	/**
	 *  This method aims to scan all threads to see whether they are completed
	 *  so the main thread will continue.
	 * 
	 * @param threadList
	 */
	public static void scanAllThread(List<Thread> threadList){
		// wait here until all threads completed
		logger.debug("Thread List Size:" + threadList.size());
		while(threadList.size() > 0){
			for(int i = 0 ; i < threadList.size();i++){
				Thread t = threadList.get(i);
				logger.debug("Thread Name:" + threadList.get(i).getName());
				try {
					// terminate the thread if specific time elapsed
					if(t.isAlive() && ! t.isInterrupted()){
						logger.debug("waiting "+t.getName()+" for "+ TIME_TO_INTERUPT_THREAD );
						t.join(TIME_TO_INTERUPT_THREAD);
						t.interrupt();
					}
					
					threadList.remove(i);
				} catch (Throwable e) {
					logger.error(e);
				}
			}
		}
		
		
		logger.debug("End scanning all thread");
	}
	
	
	
	/**
	 * This method aims to update the status for each resume parse.
	 * The nested map is Map<String,Boolean> while the outside map is Map<File, Map<String,Boolean>>.
	 * The the nested key is either "s3Completed" or "sfCompleted", value is either true or false.
	 * The outside key is the path of the file.
	 * 
	 * @param filePath
	 * @param action
	 * @return
	 */
	private Map<File, Map<String,Boolean>> updateParseStatusMap(File filePath, String action){
		
		synchronized (parseStatus) {
			// action's value should be either "s3Completed" or "sfCompleted"
			Map<String,Boolean> tempActionStatus = parseStatus.get(filePath);
			if(tempActionStatus == null){
				tempActionStatus = new HashMap<String,Boolean>();
				parseStatus.put(filePath, tempActionStatus);
			}
			tempActionStatus.put(action, true);
			
			return parseStatus;
		}	
	}

	public Map<String, HashMap<String, String>> getErrorMsgMap() {
		synchronized (errorMsgMap) {
			return errorMsgMap;
		}
	}
	
	public Map<String, HashMap<String, String>> addErrorMsg(String key, String type, String msg) {
		
		synchronized (errorMsgMap) {
			HashMap<String, String> tempErrorMap = errorMsgMap.get(key);
			if (tempErrorMap == null) {
				tempErrorMap = new HashMap<String, String>();
				errorMsgMap.put(key, tempErrorMap);
			}
			tempErrorMap.put(type, msg);
		}
		
		return errorMsgMap;
	}

	public List<Thread> getThreadList() {
		synchronized (threadList) {
			return threadList;
		}
	}
	public List<Thread> addThread(Thread t){
		synchronized (threadList) {
			threadList.add(t);
		}
		
		return threadList;
	}

	public Map<String, Candidate> getSuccessfulCandidateMap() {
		synchronized (successfulCandidateMap) {
			return successfulCandidateMap;
		}
	}

	

	

	public Map<String, Candidate> addSuccessfulCandidate( String id, Candidate candidate) {
		synchronized (candidate) {
			this.successfulCandidateMap.put(id, candidate);
			return successfulCandidateMap;
		}
	}

	public void clear() {
		parseStatus.clear();
		errorMsgMap.clear();
		threadList.clear();
		successfulCandidateMap.clear();
	
	}
	
	
	
}
