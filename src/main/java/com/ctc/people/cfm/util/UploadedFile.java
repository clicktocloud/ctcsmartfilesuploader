package com.ctc.people.cfm.util;

import com.ctc.common.util.file.UploadedFileWrapper;
import com.ctc.people.cfm.entity.Constant;

/**
 * This class mainly aims to handle the file name of the resume before uploading to S3.
 * 
 * @author Kevin
 *
 */
public class UploadedFile  extends UploadedFileWrapper{

	
	
	// to refine the file name removing illegal chars and adding a unique random stamp 
	public UploadedFile(String name) {
		super(name);
		
		
	}

	@Override
	public String setUploadFolder() {
		
		setUploadFolder(Constant.ATTACHMENT_FOLDER);
		
		return getUploadFolder();
	}
	
	
}
