package com.ctc.people.cfm.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.ActivityDocs;
import com.google.gson.Gson;

public class CFMHelper {
	
	public static int WEB_DOC_FILE_NAME_LEN_LIMIT = 80;
	
	
	public static ActivityDocs fetchActivityDocsFromJson(String jsonStr){
		ActivityDocs activityDocs=new ActivityDocs();
		Gson jsonFile=new Gson();
		activityDocs=jsonFile.fromJson(jsonStr, ActivityDocs.class);
		return activityDocs;
	}
	
	/**
	 * To fetch values from the property files
	 * 
	 * @param key
	 * @param filename
	 * @return
	 */
	public static String getValueFromPropertyFile(String key, String filename) {
		
		return CTCFileUtils.getValueFromPropertyFile(key, filename);
		/*
		Properties p = new Properties();
		String value = null;
		InputStream in = null;
		try {
			in = CFMHelper.class.getClassLoader().getResourceAsStream(filename);
			p.load(in);
			value = p.getProperty(key);
		} catch (IOException e) {
			// logger.error("IOException :", e);
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (Exception e) {
				}
		}
		return value;*/
	}
	
	
	
	
	 
	 /**
	 * To store fields of uploaded form
	 * 
	 * @param item
	 * @param map
	 * @return
	 */
	public static Map<String, String> setFormField(FileItem item, Map<String, String> map) {
		// to combine all skills to one string
		if(item.getFieldName().equalsIgnoreCase("skillGroups") && map.containsKey("skillGroups")){
			String tmpSkillGroups = null;
			tmpSkillGroups = map.get("skillGroups");
			map.put("skillGroups", tmpSkillGroups +":"+item.getString().trim());
		}else{
			map.put(item.getFieldName(), item.getString().trim());
		}	
		return map;
	}
	


	
	/**
	 * This method aims to restore the file on server in case the upload and insertion fail.
	 * 
	 * @param item
	 * @return
	 * @throws IOException
	 */
	public static File generateUploadedFile(FileItem item) throws Exception {
		// rename the file of resume
        
		UploadedFile uploadedFile = new UploadedFile(item.getName());
		
		return uploadedFile.generateUniqueUploadedFile(item.getInputStream());
		
	}
	
	  /**
     * @author Jack ZHOU
     * @date 11/08/2016
     * this method is generate local file from request from Email Service import candidate request
     * @param bFile FileByte
     * @param docName File Name
     * @return File
     */
    public static File generateUploadedFile(byte[] bFile, String docName) throws Exception{
   
        UploadedFile uploadedFile = new UploadedFile(docName);
        
        return uploadedFile.generateUniqueUploadedFile(bFile);
    		
       
    }
    
	
	 
    
    /**
     * - copy from EnhancedPeopleCloud
     * @author andy 
     * @param originalName
     * @return
     */
 	public static String fixWebDocFileName(String originalName){
 		if(originalName == null || originalName.length()==0)
 			return originalName;
 		
 		// Remove file path if it is part of file name.
 		String fixed = FilenameUtils.getName(originalName);
 		
 		if(fixed.length()> WEB_DOC_FILE_NAME_LEN_LIMIT){
 			fixed = CTCFileUtils.shortFileName(fixed, WEB_DOC_FILE_NAME_LEN_LIMIT);
 		}
 		
 		return fixed;
 		
 		
 	}
 	
 	



  
    /**
	 * 
	 * 
	 * @param fieldMap
	 * @param errorCode
	 * @param cid
	 * @return
	 */
	public static String getURL(Map<String, String> fieldMap, String errorCode, String cid){
		if(cid==""){
			return fieldMap.get("urlprefix")+"apex/ImportCandidateResult?code="+errorCode+"&oid="+fieldMap.get("orgid")+"&bn=" +fieldMap.get("bucketname");
		}else{
			return fieldMap.get("urlprefix")+"apex/ImportCandidateResult?code="+errorCode+"&id="+cid+"&oid="+fieldMap.get("orgid")+"&bn=" +fieldMap.get("bucketname");
		}
		
	}
	
	
	
	
	
	
	
	
	
    
    public static String generateSkillGroupsString(List<String> skillGroupsList){
        String skillGroupsString = "";
        if( skillGroupsList == null)
        	return "";

        for(int i = 0 ; i < skillGroupsList.size(); i++){
            if(i==0){
                skillGroupsString=skillGroupsList.get(0);
                continue;
            }
            skillGroupsString = skillGroupsString + ";" + skillGroupsList.get(i);
        }
        return skillGroupsString;
    }
    
    public static String arrayToString(List<String> srcArray, String splitter, String delimiter){
    	if( srcArray == null )
    		return "";
    	String ret = "";
    	for(int i = 0; i < srcArray.size();i++){
			if(i==0 && srcArray.size() ==1){
				ret = ret + delimiter + srcArray.get(i) + delimiter;
				
			}else if(i==(srcArray.size()-1)){
				ret = ret + delimiter +srcArray.get(i)+delimiter;
				
			}else{
				ret = ret + delimiter+srcArray.get(i)+delimiter + splitter + " " ; 
			}
		}
    	return ret;
    }
    
	
	
}
