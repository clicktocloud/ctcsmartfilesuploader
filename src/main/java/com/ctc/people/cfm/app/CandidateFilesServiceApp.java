package com.ctc.people.cfm.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.extension.service.CandidateFilesService;


public class CandidateFilesServiceApp {
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	public static List<String> defaultSkillGroups = Arrays.asList(new String[]{"G000001","G000002","G000003","G000004","G000005","G000006","G000007","G000008","G000009","G000010","G000011"});
	

	public static void main(String[] args) throws FileNotFoundException, IOException{
		
		CTCFilesOwner owner = new CTCFilesOwner();
		
		
		//owner.setWhoId("0032800000dKjEWAA0");
		
		
		
		owner.setBucketName("temp003");
		//owner.setBucketName("ctctestinstance");
		owner.setEnableSkillParsing(true);
		owner.setNamespace("");
		//owner.setNamespace("PeopleCloud1__");
		owner.setUserId("0052800000646Es");
		owner.setOrgId("00D28000000TdHk");
		//owner.setOrgId("00D90000000eaLJ");
		owner.setSkillGroups(defaultSkillGroups);
		owner.setWhoIdField(owner.getNamespace() +  Constant.WEB_DOC_FIELD_WHO_ID);
		
		
		
		CTCFile ctcfile = new CTCFile();
		
		ctcfile.setOwner(owner);
		ctcfile.setOriginalFileName("AndysResumeNew.docx");
		ctcfile.setType(Constant.RESUME_DOC_TYPE);
		File file = new File(Constant.ATTACHMENT_FOLDER + ctcfile.getOriginalFileName());
        byte[] content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
		ctcfile.setFileContent(content);
        
		owner.getFiles().add(ctcfile);
		
		ctcfile = new CTCFile();
		ctcfile.setOwner(owner);
		ctcfile.setOriginalFileName("AndysAttachment-01.csv");
		ctcfile.setType(Constant.OTHER_DOC_TYPE);

		file = new File(Constant.ATTACHMENT_FOLDER + ctcfile.getOriginalFileName());
		content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
		ctcfile.setFileContent(content);
        
		owner.getFiles().add(ctcfile);
		
		
		ctcfile = new CTCFile();
		ctcfile.setOwner(owner);
		ctcfile.setOriginalFileName("AndysAttachment-02.xlsx");
		ctcfile.setType(Constant.OTHER_DOC_TYPE);

		file = new File(Constant.ATTACHMENT_FOLDER + ctcfile.getOriginalFileName());
		content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
		ctcfile.setFileContent(content);
        
		owner.getFiles().add(ctcfile);
		
        
		CandidateFilesService.make(owner);
	}
}


