package com.ctc.people.cfm.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.ParseResumeToCandidateRequest;
import com.ctc.people.cfm.entity.ParseResumeToCandidateResponse;
import com.ctc.people.cfm.service.ParseResumeToCandidateService;
import com.ctc.people.cfm.util.UploadedFile;

public class ParseResumeToCandidateApp {
	
	public static List<String> defaultSkillGroups = Arrays.asList(new String[]{"G000001","G000002","G000003","G000004","G000005","G000006","G000007","G000008","G000009","G000010","G000011"});
	
	public static void main(String[] args){
		
		try {
			ParseResumeToCandidateApp.parseByRequest();
			
		} catch ( Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
	public static void parseByRequest() throws IOException{
        
        ParseResumeToCandidateRequest request  = new ParseResumeToCandidateRequest();
        
        request.setBucketName("temp003");
        request.setDocType(Constant.RESUME_DOC_TYPE);
        request.setEnableSkillParsing(true);
        request.setNsPrefix("");
        request.setOrgId( "00D28000000TdHk");
        request.setSkillGroupIds(defaultSkillGroups);
        request.setWhoId( "" );
        
        String filename = "AndysResumeNew.docx";
        request.setAttachmentName(filename);
        
        File file = new File(Constant.ATTACHMENT_FOLDER + filename);
        byte[] content = new byte[(int)file.length()];
		IOUtils.read(new FileInputStream(file), content );
        request.setAttachment(content);
        
        ParseResumeToCandidateService service = new ParseResumeToCandidateService();
		ParseResumeToCandidateResponse response = service.parseToCandidate( request );
		
		System.out.println("Candidate Id : " + response.getCandidateId());
	}
	
	public static void parseByCTCFile(){
		CTCFile resume = new CTCFile();
		CTCFilesOwner owner = new CTCFilesOwner();
		resume.setOwner( owner );
		owner.getFiles().add(resume);
		
		
		resume.setType( "Resume");
		
		resume.setOriginalFileName("AndysResumeNew.docx");
	
		resume.setNewFileName("AndysResume_1117.docx");
		resume.setPath(Constant.ATTACHMENT_FOLDER + resume.getNewFileName());
		resume.setSize(UploadedFile.calculateDisplayFileSize(resume.getPath()));
		owner.setOrgId( "00D28000000TdHk" );
		owner.setNamespace("");
		
		owner.setBucketName("temp003");
		
		owner.setWhoId("");
		owner.setWhoIdField("Document_Related_To__c");
		
		owner.setSkillGroups( defaultSkillGroups );
		owner.setEnableSkillParsing(true);
		

		ParseResumeToCandidateService service = new ParseResumeToCandidateService();
		ParseResumeToCandidateResponse response = service.parseToCandidate( resume );
		
		System.out.println("Candidate Id : " + response.getCandidateId());
	}
	

}
