package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.sforce.soap.partner.PartnerConnection;

/**
 * this class accepts the list of ActivityDoc objects of ONE owner, downloads various resources from salesforce to local file system 
 * and generate the standard data package of CTCFilesOwner which can be used by CommonFilesMaker component.
 * 
 * @author andy
 *
 */
public class CombinationRetriever implements ResourceRetriever{
	
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	private PartnerConnection conn;
	private ArrayList<ActivityDoc> allDocs;
	private CTCFilesOwner filesOwner;
	private String folder;
	private String namespace;
	
	/**
	 * Constructor
	 * @param conn - PartnerConnection
	 * @param allDocs - ArrayList<ActivityDoc>
	 * @param folder - String
	 */
	public CombinationRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder){
		this.conn = conn;
		this.allDocs = allDocs;
		this.folder = folder;
		filesOwner= new CTCFilesOwner();
		CTCFileUtils.checkFolder(folder);
	}
	
	/**
	 * Constructor 
	 * @param conn - PartnerConnection
	 * @param allDocs  - ArrayList<ActivityDoc>
	 * @param folder - String
	 * @param namespace - String
	 */
	public CombinationRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder, String namespace){
		this(conn, allDocs, folder);
		this.namespace = namespace;
		
	}
	
	private boolean validate(){
		boolean valid = true;
		if(allDocs == null || allDocs.size() == 0)
			return false;
		
		for(ActivityDoc doc: allDocs){
			boolean hasWhat = doc.getWhatMap() != null && doc.getWhatMap().size() > 0;
			boolean hasWho = doc.getWhoMap() != null && doc.getWhoMap().size() > 0;
			
			valid = hasWhat || hasWho;
			
		}
		
		return valid;
	}
	
	@Override
	public void retrieve() {
		logger.info("Start to download resources from Salesforce. ");
		logger.debug("total resources = " + allDocs.size());
		
		if( ! validate() ){
			throw new ResourceDownloadException("Invalid activityDocs - Each ActivityDoc MUST have whoId or WhatId !");
		}
		
		//retrieve resource for each ActivityDoc object
		for(ActivityDoc doc: allDocs){
			
			String resourceId = doc.getResourceId();
			if(resourceId == null || resourceId.equals("")){
				resourceId = doc.getAttachmentId();
			}
			
			String namespace  = this.namespace;
			if(namespace == null || namespace.equals("")){
				namespace  = doc.getNameSpace();
			}
			
			List<ResourceContent> contents = null;
			
			try {
				//create a ResourceSelector instance due to the resource type of ActivityDoc object
				ResourceSelector selector = ResourceSelectorFactory.getResourceSelector(conn, doc, namespace);
				if(selector != null){
					//get the list of ResourceContent by using the ResourceSelector object
					contents = selector.getResourceContents();
					if(contents == null || contents.size() == 0 ){
						
						logger.warn("Resource does not exist. Resource Id = " + resourceId);
						
					}	
				}

			} catch (Exception e) {
				logger.error("Failed to retrieve resource! Resource Id = " + resourceId);
				logger.error(e);
			}
			
			//handle these ResourceContent objects and concrete the CTCFilesOwner object
			ResourceRetrieverFactory.fillFilesOwner(filesOwner, contents, doc, namespace, folder);
			
		}
		logger.info("Completed downloading files from Salesforce. ");
	}

	public String getNamespace() {
		return namespace;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}	
	
	public List<CTCFilesOwner> getFilesOwners(){
		if(filesOwner == null){
			return null;
		}
		return Arrays.asList(new CTCFilesOwner[]{filesOwner});
	}
}
