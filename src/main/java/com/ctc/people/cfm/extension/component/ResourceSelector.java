package com.ctc.people.cfm.extension.component;

import java.util.List;

/**
 * This interface defines a standard method of getResourceContent which can be implemented to get the content of specified resource type from salesforce instance.
 * At present, this interface pe-defines two types of documents:
 * 1. Attachment :  Attachment object in salesforce
 * 2. File : ContentVersion object in salesforce
 *  
 * @author andy
 *
 */
public interface ResourceSelector {
	public static final String ATTACHMENT_RESOURCE = "Attachment";
	public static final String FILE_RESOURCE = "File";
	
	/**
	 * get the content of specified resource type from salesforce instance
	 * 
	 * @return List<ResourceContent>
	 */
	public List<ResourceContent> getResourceContents();

}
