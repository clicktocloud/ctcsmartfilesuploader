package com.ctc.people.cfm.extension.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.SfTaskUpdateFields;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.FilesUploadingException;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.ctc.people.cfm.extension.component.BulkCombinationRetriever;
import com.ctc.people.cfm.extension.component.ResourceRetriever;
import com.ctc.people.cfm.extension.component.TaskUpdater;
import com.ctc.people.cfm.service.CommonBulkFilesMaker;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;

/**
 * This class is especially designed as template to handle candidate's files, which 
 * 1. accepts the list of ActivityDoc as parameter, 
 * 2. downloads files from salesforce instance to local file system. 
 * 3. generates the standard data package of CTCFilesOwner
 * 4. invoke CommonFilesMaker
 * 5. update task back to salesforce
 
 *  
 * @author andy
 *
 */
public class BulkCandidateFilesService implements IFilesService {

static Logger logger = Logger.getLogger("com.ctc.people");
	
	public static void make(ArrayList<ActivityDoc> allDocs ){
		
		if (allDocs == null || allDocs.size() == 0) {
			logger.error("The list of ActivityDoc is empty, return back.");
			return;
		}
		
		// Get orgId and org login detail from Corporate Instance
		String orgId = allDocs.get(0).getOrgId();
		
		logger.info("Getting the configuration of Org [ " + orgId + " ] ... ");
		
		SforgEx sforg = SfHelper.getSfOrg(orgId);
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ orgId +" ], return back.");
			return;
		}
		
		logger.info("Begin to make candidate and files ... ");
		
		new BulkCandidateFilesService(allDocs, sforg).make();
		
	}
	
	public static void make(List<CTCFilesOwner> owners ){
		
		if (owners == null || owners.size() == 0) {
			logger.error("The list of owners is empty, return back.");
			return;
		}
		
		//get org configuration from corp instance
		String orgId = owners.get(0).getOrgId();
		logger.info("Getting the configuration of Org [ " + orgId + " ] ... ");
		SforgEx sforg = SfHelper.getSfOrg(orgId);
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ orgId +" ], return back.");
			return;
		}
		
		logger.info("Begin to make bulk candidates and files ... ");
		
		//execute the service of bulk uploading candidate files
		new BulkCandidateFilesService(owners,sforg).make();
		
	}
	
	protected ArrayList<ActivityDoc> allDocs;
	protected SforgEx sforg;
	protected String namespace;
	protected String orgId;
	
	protected PartnerConnection conn;
	protected String status = "Complete";
	protected String description = "Upload success";	
	

	List<CTCFilesOwner> owners;
	
	/**
	 * Constructor with list of ActivityDocs parameter
	 * @param allDocs
	 * @param sforg
	 */
	protected BulkCandidateFilesService(ArrayList<ActivityDoc> allDocs, SforgEx sforg) {
		this.allDocs = allDocs;
		this.sforg = sforg;	
	}
	
	/**
	 * Constructor with list of CTCFilesOwners
	 * @param owners
	 * @param sforg
	 */
	protected BulkCandidateFilesService(List<CTCFilesOwner> owners,SforgEx sforg) {
		this.owners = owners;
		this.sforg = sforg;	
		
	}
	
	/**
	 * verify the parameters and initialise essential variables:
	 * 1. orgId 
	 * 2. namespace - salesforce instance namespace of current org
	 * 3. conn - PartnerConnection type which is connection to salesforce instance of specified org
	 * 4. status - the status of progress
	 * 5. description - the description of status
	 */
	@Override
	public boolean init(){
		logger.info("Initialising " + this.getClass().getName() + " ... ");
		if(( allDocs == null || allDocs.size() == 0) 
				&&(owners == null || owners.size() ==0)){
			
			return false;
		}
		
		// initialise task status and description
		status = "Initialising";
		description = "Uploading in progress ... ";	
		
		// initialise orgId and namespace
		if( allDocs != null && allDocs.size() >  0 ){
			ActivityDoc doc = allDocs.get(0);
			namespace = doc.getNameSpace();
			orgId = doc.getOrgId();
			
		}else{
			namespace = owners.get(0).getNamespace();
			orgId = owners.get(0).getOrgId();
		}
		
		//try to get candidate id from ActivityDoc
		//if got one, means that candidate is existing, else if, means new candidate to be created
		
		
		logger.debug("\t orgId=" + orgId);
		logger.debug("\t namespace=" + namespace);	
		//logger.info("\t taskId=" + taskId);

		// get connection to client's instance
		conn = SfHelper.login( orgId );
		if( conn == null ){
			logger.error("\t Failed to get connection to Salesforce instance [ " + orgId +" ], abort !");
			return false;
		}
		
		logger.debug("\t Connected to Salesforce instance of org [ " + orgId +" ]");
		
		if(this.namespace == null){
			this.namespace = SalesforceAgent.getNamespace(orgId);
			if( allDocs != null && allDocs.size() >  0 ){
				for(ActivityDoc doc: allDocs){
					doc.setNameSpace(this.namespace);
				}
	
			}else{
				namespace = owners.get(0).getNamespace();
				orgId = owners.get(0).getOrgId();
			}
		}
		
		
		
		this.sforg.setConn(conn);
		
		return true;
	}
	
	/**
	 * Retrieve the CTCFilesOwner object from List<ActivityDoc> parameter and download files from salesforce to local system
	 */
	@Override
	public void retrieveFilesOwner(){
		
		//convert list of ActivityDocs to list of CTCFilesOwners 
		//and download files contents from salesforce to local files system
		if(owners == null && allDocs != null){
			logger.info("Retrieving files from the resource object of Salesforce ...");
			
			try {
				ResourceRetriever attchRtrv =new BulkCombinationRetriever(conn,
						allDocs, Constant.ATTACHMENT_FOLDER);
				
				attchRtrv.retrieve();
				owners = attchRtrv.getFilesOwners();
				
			} catch (ResourceDownloadException e) {
				logger.error(e.getMessage());
			}
			
			if(owners == null 
				|| owners.size() == 0  ){
				
				logger.error("\tFailed to retrieve files from the resource object of Salesforce, abort!");
				
				throw new FilesUploadingException("Failed to retrieve files from the resource object of Salesforce") ;
			}
		}
		
		logger.info("\tSuccess to receive [ "+ owners.size()+" ] CTCFilesOwners : " );
	}
	
	/**
	 * Invoke the make method of CommonFilesMaker to do series of actions to upload candidate files
	 */
	@Override
	public void handle(){
		
		if(owners != null ){
			logger.info("Begin to bulk uploading files for [ "+owners.size()+" ] owners");
			CommonBulkFilesMaker.make( owners ,  sforg);	
		}
	}
	
	/**
	 * Update the Task/Activity status
	 */
	@Override
	public void end(){
		if(owners != null){
			List<SfTaskUpdateFields> tasks = new ArrayList<SfTaskUpdateFields>();
			
			for(CTCFilesOwner owner : owners){
				tasks.add( new SfTaskUpdateFields(
						owner.getUserId(),
						owner.getTaskId(),
						owner.getWhoId(),
						owner.getWhatMap(),
						status,
						description,
						namespace));
			}
			
			logger.info("Begin to update tasks/activities status ... ");
			TaskUpdater.update(conn,  tasks);
			logger.info("===== Completed uploading files for org[ " + orgId  + " ] =====");
		}
	}
	
	/**
	 * Run making action as below steps:
	 * 1. init()
	 * 2. retrieveFilesOwner()
	 * 3. handle()
	 * 4. end()
	 */
	@Override
	public void make() {
		
		try{
			if( init() ){
				
				/*
				 * STEP 1:  downloads files from salesforce instance to local file system and generates the standard data package of CTCFilesOwner
				 */
				retrieveFilesOwner();
				
				if(owners != null ){
					
					/*
					 * STEP 2: invoke CommonFilesMaker to do common actions on current CTCFilesOwner object
					 */
					handle();
					
				}else{
					
					throw new FilesUploadingException("[ RetrieveFilesOwner ] : Failed to retrieve CTCFilesOwner object from parameters ");
				}
			}else{
				
				throw new FilesUploadingException("[ Init ] : Failed to initialise " + this.getClass().getName());
			}
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			/*
			 * STEP 3: update task back to salesforce
			 */
			end();
		}
	}
	

}
