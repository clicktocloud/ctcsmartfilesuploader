package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.sforce.soap.partner.PartnerConnection;

/**
 *  @deprecated please use {@link CombinationRetriever}
 * this class accepts the list of ActivityDoc objects, downloads files from salesforce to local file system 
 * and generate the standard data package of CTCFilesOwner which can be used by CommonFilesMaker component.
 * 
 * @author andy
 *
 */
public class AttachmentRetriever implements ResourceRetriever{
	
	static Logger logger = Logger.getLogger(AttachmentRetriever.class);
	
	private PartnerConnection conn;
	private ArrayList<ActivityDoc> allDocs;
	private CTCFilesOwner filesOwner;
	private String folder;
	private String namespace;
	
	
	public AttachmentRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder){
		this.conn = conn;
		this.allDocs = allDocs;
		this.folder = folder;
		filesOwner= new CTCFilesOwner();
		CTCFileUtils.checkFolder(folder);
	}
	
	public AttachmentRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder, String namespace){
		this(conn, allDocs, folder);
		this.namespace = namespace;
		
	}
	
	@Override
	public void retrieve() {
		logger.info("Start to download files from Salesforce. ");
		logger.debug("total files = " + allDocs.size());
		
		for(ActivityDoc doc: allDocs){
			
			String resourceId = doc.getResourceId();
			if(resourceId == null || resourceId.equals("")){
				resourceId = doc.getAttachmentId();
			}
			
			logger.debug("Handling resource id=" + resourceId + ", type=" + doc.getDocType());

			
			ResourceSelector selector = new AttachmentSelector(conn, doc, namespace);
			List<ResourceContent> contents = selector.getResourceContents();
			if(contents == null || contents.size() == 0 ){
				logger.warn("Resource does not exist. Resource Id =" + doc.getResourceId());
				continue;
			}
			
			String namespace  = this.namespace;
			if(namespace == null || namespace.equals("")){
				namespace  = doc.getNameSpace();
			}
			
			ResourceRetrieverFactory.fillFilesOwner(filesOwner, contents, doc, namespace, folder);
			
		}
		logger.info("Completed downloading files from Salesforce. ");
	}

	
	
	


	public String getNamespace() {
		return namespace;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}	
	
	public List<CTCFilesOwner> getFilesOwners(){
		if(filesOwner == null){
			return null;
		}
		return Arrays.asList(new CTCFilesOwner[]{filesOwner});
	}
}
