package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 *
 * This class implements the ResourceSelector interface to get document content from Attachment object in salesforce by a ActivityDoc object
 * 
 * @author andy
 *
 */
public class AttachmentSelector implements ResourceSelector{
	
	
	static Logger logger = Logger.getLogger(AttachmentSelector.class);
	
	
	private PartnerConnection conn;
	private ActivityDoc doc;
	private String namespace;
	
	/**
	 * Constructor 
	 * @param conn - PartnerConnection
	 * @param doc - ActivityDoc
	 * @param namespace - String
	 */
	public AttachmentSelector(PartnerConnection conn, ActivityDoc doc, String namespace){
		this.conn = conn;
		this.doc = doc;
		this.namespace = namespace;
	}

	/**
	 *  get document content from Attachment object in salesforce 
	 *  
	 *  @return List<ResourceContent>
	 */
	@Override
	public List<ResourceContent> getResourceContents() {
		
		String resourceId = doc.getResourceId();
		if( resourceId == null || resourceId.equals("")){
			resourceId = doc.getAttachmentId();
		}
		
		if( resourceId == null || resourceId.equals("")){
			
			logger.error("Resource Id is empty !");
			return null;
		}
		
		String query = "SELECT Id, Name, Body " + " FROM Attachment WHERE Id = \'" + resourceId + "\'";

		QueryResult queryResults = null;
		try{
			queryResults = conn.query(query);
			if(queryResults!=null && queryResults.getSize()>0) {
				List<ResourceContent> contents = new ArrayList<ResourceContent>();
				for(SObject so:queryResults.getRecords()){
					//create a ResourceContent object to keep the document content
					ResourceContent content = new ResourceContent();
					content.setId(so.getField("Id").toString());
					content.setName(so.getField("Name").toString());
					content.setContent(so.getField("Body").toString());
				
					contents.add(content);
				}
				
				return contents;
			}
		} catch (Exception e) {
			logger.error("Failed to download resource["+resourceId+"] from Attachment object of Salesforce.");
			throw new ResourceDownloadException("Failed to download resource["+resourceId+"] from  Attachment object of." + e);
		} 
		
		return null;
	}

	public String getNamespace() {
		return namespace;
	}	

}
