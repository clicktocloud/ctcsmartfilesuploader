package com.ctc.people.cfm.extension.component;

import java.util.concurrent.Callable;

/**
 * this is a callable for uploading files
 * @author kwu
 *
 */
public class FilesUploadRunner implements Callable<Boolean> {
	private Runnable uploader;
	
	/**
	 * constructor
	 * @param docs
	 */
	public FilesUploadRunner( Runnable uploader){
		this.uploader = uploader;
	}

	public Boolean call() throws Exception {
		
		uploader.run();
		
		return null;
	}
	
	
}
