package com.ctc.people.cfm.extension.service;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.SforgEx;

public class JXTEmailToCandidateService extends CandidateFilesServiceTemplate{
	
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	public static void make(ArrayList<ActivityDoc> allDocs ){
		
		if (allDocs == null || allDocs.size() == 0) {
			logger.error("The list of ActivityDoc is empty, return back.");
			return;
		}
		
		// Get orgId and org login detail from Corporate Instance
		String orgId = allDocs.get(0).getOrgId();
		
		logger.info("Getting the configuration of Org [ " + orgId + " ] ... ");
		
		SforgEx sforg = SfHelper.getSfOrg(orgId);
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ orgId +" ], return back.");
			return;
		}
		
		logger.info("Begin to make candidate and files ... ");
		
		new JXTEmailToCandidateService(allDocs, sforg).make();
		
	}
	
	public static void make(CTCFilesOwner owner, String taskId ){
		
		if (owner == null || owner.getFiles() == null || owner.getFiles().size() == 0) {
			logger.error("The list of files is empty, return back.");
			return;
		}
		
		// Get orgId and org login detail from Corporate Instance
		String orgId = owner.getOrgId();
		
		logger.info("Getting the configuration of Org [ " + orgId + " ] ... ");
		
		SforgEx sforg = SfHelper.getSfOrg(orgId);
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ orgId +" ], return back.");
			return;
		}
		
		logger.info("Begin to make candidate and files ... ");
		
		new JXTEmailToCandidateService(owner, sforg).make();
		
	}
	
	

	private JXTEmailToCandidateService(ArrayList<ActivityDoc> allDocs,
			SforgEx sforg) {
		super(allDocs, sforg);
		
	}
	
	private JXTEmailToCandidateService(CTCFilesOwner owner,  SforgEx sforg) {
		super(owner, sforg );
		
	}
	
	public boolean init(){
		if( super.init() ){
			
			
			return true;
		}
		
		return false;
	}
	
	public void retrieveFilesOwner(){
		super.retrieveFilesOwner();
		
		//TODO retrieve more parameters
	}
	
	public void handle(){
		super.handle();
	}
	
	public void end(){
		super.end();
		
	}

}
