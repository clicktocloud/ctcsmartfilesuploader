package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.sforce.soap.partner.PartnerConnection;

/**
 *  @deprecated please use {@link BulkCombinationRetriever}
 * this class accepts the list of ActivityDoc objects, downloads files from salesforce to local file system 
 * and generate the standard data package of CTCFilesOwner which can be used by CommonFilesMaker component.
 * 
 * @author andy
 *
 */
public class BulkFilesRetriever implements ResourceRetriever{
	
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	private PartnerConnection conn;
	private ArrayList<ActivityDoc> allDocs;
	private List<CTCFilesOwner> filesOwners;
	private String folder;
	private String namespace;
	
	
	public BulkFilesRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder){
		this.conn = conn;
		this.allDocs = allDocs;
		this.folder = folder;
		filesOwners= new ArrayList<CTCFilesOwner>();
		CTCFileUtils.checkFolder(folder);
	}
	
	public BulkFilesRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder, String namespace){
		this(conn, allDocs, folder);
		this.namespace = namespace;
		
	}
	
	@Override
	public void retrieve() throws ResourceDownloadException{
		logger.debug("\t Start to download files from Salesforce. ");
		logger.debug("\t total files = " + allDocs.size());
		
		Map<String, CTCFilesOwner> ownersMap  = new HashMap<String, CTCFilesOwner>();
		
		for(ActivityDoc doc: allDocs){
			logger.debug("\t Handling resource id=" + doc.getResourceId() + ", type=" + doc.getDocType());
			
			ResourceSelector selector = new FileSelector(conn, doc, namespace);
			List<ResourceContent> contents = selector.getResourceContents();
			if(contents == null || contents.size() == 0 ){
				logger.warn("Resource does not exist. Resource Id =" + doc.getResourceId());
				continue;
			}
			
			
			String namespace  = this.namespace;
			if(namespace == null || namespace.equals("")){
				namespace  = doc.getNameSpace();
			}
			CTCFilesOwner filesOwner  = new CTCFilesOwner();
			ResourceRetrieverFactory.fillFilesOwner(filesOwner, contents, doc, namespace, folder);
			
			String ownerId = filesOwner.getOwnerId();
			if(ownerId == null || ownerId.equals("") ){
				continue;
			}
			
			CTCFilesOwner existFilesOwner = ownersMap.get( ownerId );
			if( existFilesOwner == null){
				
				ownersMap.put(ownerId, filesOwner);
			}else{
				existFilesOwner.getFiles().addAll( filesOwner.getFiles());
			}
				
		}
		
		this.filesOwners = new ArrayList<CTCFilesOwner>(ownersMap.values());
				
	}


	public String getNamespace() {
		return namespace;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}	
	
	public List<CTCFilesOwner> getFilesOwners(){
		return filesOwners;
	}
}
