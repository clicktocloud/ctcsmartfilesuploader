package com.ctc.people.cfm.extension.service;

public interface IFilesService {
	
	
	public boolean init();
	
	public void retrieveFilesOwner();
	
	public void handle();
	
	public void end();
	
	public void make();

}
