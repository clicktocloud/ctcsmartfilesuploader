package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 * This class implements the ResourceSelector interface to get document content from ContentVersion object in salesforce by a ActivityDoc object
 * 
 * @author andy
 *
 */
public class FileSelector implements ResourceSelector{
	
	static Logger logger = Logger.getLogger(FileSelector.class);
	
	
	private PartnerConnection conn;
	private ActivityDoc doc;
	private String namespace;
	
	/**
	 * Constructor 
	 * @param conn - PartnerConnection
	 * @param doc - ActivityDoc
	 * @param namespace - String
	 */
	public FileSelector(PartnerConnection conn, ActivityDoc doc, String namespace){
		this.conn = conn;
		this.doc = doc;
		this.namespace = namespace;
	}
	
	/**
	 * get document content from ContentVersion object in salesforce 
	 * 
	 * @return List<ResourceContent>
	 */
	@Override
	public List<ResourceContent> getResourceContents() {
		
		String resourceId = doc.getResourceId();
		
		if( resourceId == null || resourceId.equals("")){
			
			logger.error("Resource Id is empty !");
			return null;
		}
		
		String query = "SELECT Id,ContentDocumentId, FileExtension,VersionData FROM ContentVersion WHERE  ContentDocumentId=  \'" + resourceId + "\' AND  IsLatest = true ";
		
		
		logger.debug("FileSelector : " + query);
		QueryResult queryResults = null;
		try{
			queryResults = conn.query(query);
			
			logger.debug("FileSelector - Result size : " + queryResults.getSize());
			
			if(queryResults!=null && queryResults.getSize()>0) {
				List<ResourceContent> contents = new ArrayList<ResourceContent>();
				for(SObject so:queryResults.getRecords()){
					
					//create a ResourceContent object to keep the document content.
					ResourceContent content = new ResourceContent();
					//keep ContentDocumentId
					content.setId(so.getField("ContentDocumentId").toString());
					//keep VersionData
					content.setContent(so.getField("VersionData").toString());
					
					String fileExtension = "";
					if(so.getField("FileExtension") != null){
						fileExtension = so.getField("FileExtension").toString();
					}
					String fileName = doc.getResourceName();
					if(! StringUtils.isEmpty(fileName ) &&  !StringUtils.isEmpty( fileExtension)){
						if(! fileName.toLowerCase().endsWith("."+fileExtension.toLowerCase())){
							fileName += "."+fileExtension;
						}
					}
					
					//keep resource name
					content.setName(fileName);
					
					contents.add(content);
				}
				
				return contents;
			}
			
		} catch (Exception e) {
			logger.error("Failed to download resource["+resourceId+"] from ContentVersion object of Salesforce.");
			throw new ResourceDownloadException("Failed to download resource["+resourceId+"] from ContentVersion object." + e);
		} 
		
		return null;
	}

	public String getNamespace() {
		return namespace;
	}

}
