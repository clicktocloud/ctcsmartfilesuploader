package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.sforce.soap.partner.PartnerConnection;

/**
 * this class accepts the list of ActivityDoc objects of MULTIPLE owners, downloads various resources from salesforce to local file system 
 * and generate the standard data package of CTCFilesOwner which can be used by CommonFilesMaker component.
 * 
 * @author andy
 *
 */
public class BulkCombinationRetriever implements ResourceRetriever{
	
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	private PartnerConnection conn;
	private ArrayList<ActivityDoc> allDocs;
	private List<CTCFilesOwner> filesOwners;
	private String folder;
	private String namespace;
	
	
	/**
	 * Constructor
	 * @param conn - PartnerConnection
	 * @param allDocs - ArrayList<ActivityDoc>
	 * @param folder - String
	 */
	public BulkCombinationRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder){
		this.conn = conn;
		this.allDocs = allDocs;
		this.folder = folder;
		filesOwners= new ArrayList<CTCFilesOwner>();
		CTCFileUtils.checkFolder(folder);
	}
	
	/**
	 * Constructor 
	 * @param conn - PartnerConnection
	 * @param allDocs  - ArrayList<ActivityDoc>
	 * @param folder - String
	 * @param namespace - String
	 */
	public BulkCombinationRetriever(PartnerConnection conn, ArrayList<ActivityDoc> allDocs, String folder, String namespace){
		this(conn, allDocs, folder);
		this.namespace = namespace;
		
	}
	
	@Override
	public void retrieve() throws ResourceDownloadException{
		logger.debug("\t Start to download resources from Salesforce. ");
		logger.debug("\t total resources = " + allDocs.size());
		
		//owners map
		Map<String, CTCFilesOwner> ownersMap  = new HashMap<String, CTCFilesOwner>();
		
		//retrieve resource for each ActivityDoc object
		for(ActivityDoc doc: allDocs){
			
			String resourceId = doc.getResourceId();
			if(resourceId == null || resourceId.equals("")){
				resourceId = doc.getAttachmentId();
			}
			
			logger.debug("Handling resource id=" + resourceId + ", type=" + doc.getDocType());

			//create a ResourceSelector instance due to the resource type of ActivityDoc object
			ResourceSelector selector = ResourceSelectorFactory.getResourceSelector(conn, doc, namespace);
			//get the list of ResourceContent by using the ResourceSelector object
			List<ResourceContent> contents = selector.getResourceContents();
			if(contents == null || contents.size() == 0 ){
				logger.warn("Resource does not exist. Resource Id =" + doc.getResourceId());
				continue;
			}
			
			String namespace  = this.namespace;
			if(namespace == null || namespace.equals("")){
				namespace  = doc.getNameSpace();
			}
			
			//handle these ResourceContent objects and concrete the CTCFilesOwner object
			CTCFilesOwner filesOwner  = new CTCFilesOwner();
			ResourceRetrieverFactory.fillFilesOwner(filesOwner, contents, doc, namespace, folder);
			
			String ownerId = filesOwner.getOwnerId();
			if(ownerId == null || ownerId.equals("") ){
				continue;
			}
			
			CTCFilesOwner existFilesOwner = ownersMap.get( ownerId );
			if( existFilesOwner == null){
				
				ownersMap.put(ownerId, filesOwner);
			}else{
				existFilesOwner.getFiles().addAll( filesOwner.getFiles());
			}
				
		}
		
		this.filesOwners = new ArrayList<CTCFilesOwner>(ownersMap.values());
		
		
	}


	public String getNamespace() {
		return namespace;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}	
	
	public List<CTCFilesOwner> getFilesOwners(){
		return filesOwners;
	}
}
