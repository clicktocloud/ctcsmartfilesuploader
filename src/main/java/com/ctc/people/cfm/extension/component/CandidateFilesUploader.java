package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.ActivityDocs;
import com.ctc.people.cfm.extension.service.CandidateFilesService;


/**
 * this is the client class for uploading files
 * 
 * @author kwu
 *
 */
public class CandidateFilesUploader implements Runnable{
	private ActivityDocs docs;
	static Logger logger = Logger.getLogger("com.ctc.people");

	/**
	 * constructor
	 * 
	 * @param docs
	 */
	public CandidateFilesUploader(ActivityDocs docs) {
		this.docs = docs;
	}

	public void run() {
		
		ArrayList<ActivityDoc> allDocs = docs.getActivityDocs();
		
		CandidateFilesService.make(allDocs);

	}
}
