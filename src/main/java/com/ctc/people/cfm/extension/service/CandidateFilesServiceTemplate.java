package com.ctc.people.cfm.extension.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.SfTaskUpdateFields;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.FilesUploadingException;
import com.ctc.people.cfm.extension.component.CombinationRetriever;
import com.ctc.people.cfm.extension.component.ResourceRetriever;
import com.ctc.people.cfm.extension.component.TaskUpdater;
import com.ctc.people.cfm.service.CommonFilesMaker;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;

/**
 * This class is especially designed as template to handle candidate's files, which 
 * 1. accepts the list of ActivityDoc as parameter, 
 * 2. downloads files from salesforce instance to local file system. 
 * 3. generates the standard data package of CTCFilesOwner
 * 4. invoke CommonFilesMaker
 * 5. update task back to salesforce
 
 *  
 * @author andy
 *
 */
public class CandidateFilesServiceTemplate implements IFilesService {

static Logger logger = Logger.getLogger("com.ctc.people");
	
	
	
	protected ArrayList<ActivityDoc> allDocs;
	protected SforgEx sforg;
	
	
	protected PartnerConnection conn;
	protected String status = "Complete";
	protected String description = "Upload success";	
	protected String namespace ;
	protected String taskId;
	protected String orgId;

	CTCFilesOwner owner;
	
	protected CandidateFilesServiceTemplate(ArrayList<ActivityDoc> allDocs, SforgEx sforg) {
		this.allDocs = allDocs;
		this.sforg = sforg;	
	}
	
	protected CandidateFilesServiceTemplate(CTCFilesOwner owner,SforgEx sforg) {
		this.owner = owner;
		this.sforg = sforg;	
		
	}
	
	/**
	 * Initialise make to create essential variables:
	 * 1. orgId 
	 * 2. namespace - salesforce instance namespace of current org
	 * 3. taskId - task / activity Id
	 * 4. status - the status of progress
	 * 5. description - the description of status
	 * 6. conn - PartnerConnection type which is connection to salesforce instance of specified org
	 */
	@Override
	public boolean init(){
		
		if(( allDocs == null || allDocs.size() == 0) 
				&&(owner == null || owner.getFiles() == null || owner.getFiles().size() == 0)){
			
			return false;
		}
		
		// initialise task status and description
		status = "Complete";
		description = "Upload success";	
		
		// get task common info from request
		if( allDocs != null && allDocs.size() >  0 ){
			ActivityDoc doc = allDocs.get(0);
			namespace = doc.getNameSpace();
			taskId = doc.getActivityId();
			orgId = doc.getOrgId();
			
		}else{
			namespace = owner.getNamespace();
			orgId = owner.getOrgId();
			taskId = owner.getTaskId();
		}
		
		//try to get candidate id from ActivityDoc
		//if got one, means that candidate is existing, else if, means new candidate to be created
		
		
		logger.debug("\t orgId=" + orgId);
		logger.debug("\t taskId=" + taskId);

		// get connection to client's instance
		conn = SfHelper.login( orgId );
		if( conn == null ){
			logger.error("Failed to get connection to Salesforce instance [ " + orgId +" ]");
			return false;
		}
		
		this.sforg.setConn(conn);
		
		if(this.namespace == null){
			this.namespace = SalesforceAgent.getNamespace(orgId);
			if( allDocs != null && allDocs.size() >  0 ){
				for(ActivityDoc doc: allDocs){
					doc.setNameSpace(this.namespace);
				}
			}else{
				owner.setNamespace(this.namespace);
				
			}
		}
		
		logger.debug("\t namespace=" + namespace);	
		
		
		return true;
	}
	
	/**
	 * Retrieve the CTCFilesOwner object from List<ActivityDoc> parameter
	 */
	@Override
	public void retrieveFilesOwner(){
		
		if(owner == null && allDocs != null){
			
			ResourceRetriever attchRtrv = new CombinationRetriever(conn, allDocs, Constant.ATTACHMENT_FOLDER);
			attchRtrv.retrieve();
			List<CTCFilesOwner> owners = attchRtrv.getFilesOwners();
			if(owners == null || owners.size() == 0){
				owner = null;
			}else{
				owner = owners.get( 0 );
			}
			
			if(owner == null 
				|| owner.getFiles() == null 
				|| owner.getFiles().size() == 0 ){
				
				logger.error("Failed to retrieve candidate files, return back!");
				
				throw new FilesUploadingException("Failed to retrieve candidate files") ;
			}
		}
		logger.info("Retrieved resources from Salesforce : " + owner.getFiles().size());
		
		
	}
	
	/**
	 * Invoke the make method of CommonFilesMaker to do series of actions to upload candidate files
	 */
	@Override
	public void handle(){
		
		CommonFilesMaker.make( owner ,  sforg);

	}
	
	/**
	 * Update the Task/Activity status
	 */
	@Override
	public void end(){
		if(owner != null && taskId != null){

			TaskUpdater.update(conn, Arrays.asList( new SfTaskUpdateFields[]{ new SfTaskUpdateFields(
					owner.getUserId(),
					taskId,
					owner.getWhoId(),
					owner.getWhatMap(),
					status,
					description,
					namespace) } ));
			
			logger.info("===== Completed uploading files for whoId["+owner.getWhoId()+"] org[ " + orgId  + " ] =====");
		}
	}
	
	/**
	 * Run making action as below steps:
	 * 1. init()
	 * 2. retrieveFilesOwner()
	 * 3. handle()
	 * 4. end()
	 */
	@Override
	public void make() {
		
		try{
			if( init() ){
				
				/*
				 * STEP 1:  downloads files from salesforce instance to local file system and generates the standard data package of CTCFilesOwner
				 */
				retrieveFilesOwner();
				
				if(owner != null && owner.getFiles() != null && owner.getFiles().size() > 0 ){
					
					/*
					 * STEP 2: invoke CommonFilesMaker to do common actions on current CTCFilesOwner object
					 */
					handle();
					
					status = "Complete";
					description = "Upload success";

				}else{
					logger.error("[ RetrieveFilesOwner ] : Failed to retrieve CTCFilesOwner object and its CTCFiles from parameters " );
					throw new FilesUploadingException("No file attached ! ");
				}
			}else{
				logger.error("Failed to initialise " + this.getClass().getName());
				throw new FilesUploadingException("[ Init ] : Failed to initialise " + this.getClass().getName());
			}
		}catch(Exception e){
			logger.error("Failed to handle candidate and files !", e);
			status = "Error";
			description = e.toString();
			
			if(description.length() > 255){
				description = description.substring(0, 254);
			}
			
			throw e;
		}finally{
			/*
			 * STEP 3: update task back to salesforce
			 */
			end();
		}
	}
	

}
