package com.ctc.people.cfm.extension.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.ParseResumeToCandidateRequest;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.ImportCandidateException;

/**
 * This class is especially designed to upload candidate's files, which 
 * 1. accepts the list of ActivityDoc as parameter, 
 * 2. downloads files from salesforce instance to local file system. 
 * 3. generates the standard data package of CTCFilesOwner
 * 4. invoke CommonFilesMaker
 * 5. update task back to salesforce
 *  
 * @author andy
 *
 */
public class CandidateFilesService extends CandidateFilesServiceTemplate{
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	public static void make(ArrayList<ActivityDoc> allDocs ){
		
		if (allDocs == null || allDocs.size() == 0) {
			logger.error("The list of ActivityDoc is empty, return back.");
			return;
		}
		
		// Get orgId and org login detail from Corporate Instance
		String orgId = allDocs.get(0).getOrgId();
		
		logger.info("Getting the configuration of Org [ " + orgId + " ] ... ");
		
		SforgEx sforg = SfHelper.getSfOrg(orgId);
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ orgId +" ], return back.");
			return;
		}
		
		logger.info("Begin to make candidate and files ... ");
		
		new CandidateFilesService(allDocs, sforg).make();
		
	}
	
	public static void make(CTCFilesOwner owner ){
		
		if (owner == null || owner.getFiles() == null || owner.getFiles().size() == 0) {
			logger.error("The list of files is empty, return back.");
			return;
		}
		
		// Get orgId and org login detail from Corporate Instance
		String orgId = owner.getOrgId();
		
		logger.info("Getting the configuration of Org [ " + orgId + " ] ... ");
		
		SforgEx sforg = SfHelper.getSfOrg(orgId);
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ orgId +" ], return back.");
			return;
		}
		
		logger.info("Begin to make candidate and files ... ");
		
		new CandidateFilesService(owner,sforg).make();
		
	}
	
	
	protected CandidateFilesService(ArrayList<ActivityDoc> allDocs, SforgEx sforg) {
		super(allDocs, sforg );
		
	}
	
	protected CandidateFilesService(CTCFilesOwner owner, SforgEx sforg) {
		super(owner,  sforg );
		
	}
	
	public boolean init(){
		return super.init();
	}
	
	public void retrieveFilesOwner(){
		super.retrieveFilesOwner();
	}
	
	public void handle(){
		super.handle();
	}
	
	public void end(){
		super.end();
	}
	
	@Deprecated
	public ParseResumeToCandidateRequest createParseResumeToCandidateRequest( 	CTCFile resume) {
		ParseResumeToCandidateRequest request = new ParseResumeToCandidateRequest();
		try {
			request.setUserId( resume.getOwner().getUserId());
			request.setOrgId(resume.getOwner().getOrgId() );
			request.setWhoId( resume.getOwner().getWhoId() );
			request.setDocType("Resume");
			request.setBucketName(resume.getOwner().getBucketName());
			request.setAttachmentName(resume.getOriginalFileName());
			request.setAttachmentNameWithTimeStamp(resume.getNewFileName());
			request.setEnableSkillParsing(resume.getOwner().isEnableSkillParsing());
			request.setNsPrefix(resume.getOwner().getNamespace());
			request.setSkillGroupIds(resume.getOwner().getSkillGroups());
			
			// convert the uploaded resume into bytes
			byte[] allBytes = null; 
			File tempFile = new File(resume.getPath());
			allBytes = IOUtils.toByteArray(new FileInputStream(tempFile));
	        request.setAttachment(Base64.encodeBase64(allBytes));
		} catch (Exception e) {
			logger.error("failed to create ParseResumeToCandidateRequest ! ", e);
			throw new ImportCandidateException();
		}
		return request;
	}
}


