package com.ctc.people.cfm.extension.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.adaptor.salesforce.Updater;
import com.ctc.people.cfm.entity.SfTaskUpdateFields;
import com.ctc.people.cfm.exception.TaskCreateException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

/**
 * this class updates task records
 * 
 * @author kwu
 *
 */
public class TaskUpdater implements Updater {
	
	
	
	
	public static String update( PartnerConnection conn, List<SfTaskUpdateFields> tasks ){
		
		if(tasks ==  null || tasks.size() == 0){
			return null;
		}
		
//		if(task.getId() == null || task.getId().equals("")){
//			return null;
//		}
		
		TaskUpdater updater = new TaskUpdater(conn, tasks);
		updater.setNsp(tasks.get(0).getNamespace() );
		
		return updater.update();
	}
	
	
	private PartnerConnection conn;
	private List<SfTaskUpdateFields> tasks;
	private String nsp = ""; // namespace prefix
	static Logger logger = Logger.getLogger("com.ctc.people");

	/**
	 * constructor
	 * 
	 * @param conn
	 * @param task
	 */
	public TaskUpdater(PartnerConnection conn, List<SfTaskUpdateFields> tasks) {
		this.conn = conn;
		this.tasks = tasks;
		this.nsp = tasks.get(0).getNamespace();
	}

	/**
	 * set namespace prefix
	 * 
	 * @param nsp
	 */
	public void setNsp(String nsp) {
		this.nsp = nsp;
	}

	@Override
	public String update() {
		
		String ctcAdminAccountId = SfHelper.getCTCAdminAccountId(conn);
		
		
		List<String> ids = new ArrayList<String>();
		logger.info("Start to update task(s).");
		
		List<SObject> objs = new ArrayList<SObject>();
		for(SfTaskUpdateFields task : tasks){
			if(task.getId() == null || task.getId().equals("")){
				continue;
			}
			
			SObject sobj = new SObject();
			sobj.setType("Task");
			sobj.setId(task.getId());
			
			if (task.getWhoId() != null && !task.getWhoId().equals("")) {
				sobj.setField("WhoId", task.getWhoId());
				
			}
			
			logger.debug("ctcAdminAccountId : " + ctcAdminAccountId);
			logger.debug("whatIds : " + task.getWhatIdsString());
			if(task.getWhatIdsString() != null && ctcAdminAccountId != null){
				task.setWhatIdsString( task.getWhatIdsString().replace(ctcAdminAccountId+":", "").replace(":"+ctcAdminAccountId, "").replace(ctcAdminAccountId, ""));
			}
			
			logger.debug("whatIds : " + task.getWhatIdsString());
			
			if(StringUtils.isEmpty(task.getWhatIdsString()) || task.getWhatIdsString().equalsIgnoreCase("null") ){
				
				sobj.setFieldsToNull(new String[] {"WhatId"});
							
			}else { 
				
				sobj.setField("whatId", task.getWhatIdsString());
				
				
			}
			
			sobj.setField(nsp + "Upload_Documents_Discription__c",
					task.getDescription());
			sobj.setField(nsp + "Upload_Documents_Status__c", task.getStatus());
			
			if(! StringUtils.isEmpty( task.getOwnerId())){
				sobj.setField("OwnerId", task.getOwnerId());
			}
			
			objs.add( sobj );
		}
		
		if(objs.size() == 0 ){
			return null;
		}
		
		SaveResult[] srs = null;
		try {
			
			
			
			srs = SfHelper.massUpdate(conn, objs.toArray(new SObject[]{}));
			
			for(SaveResult sr : srs){
				if( sr.isSuccess() ){
					ids.add( sr.getId());
				}else if (!sr.isSuccess() && sr.getErrors() != null) {
					for (com.sforce.soap.partner.Error error : sr.getErrors()) {
						logger.warn(Arrays.toString(error.getFields()) + " : "
								+ error.getMessage());
					}
				}
			}
			
			
		} catch (ConnectionException e) {
			logger.error("Failed to update task(s). " + e);
			throw new TaskCreateException("Failed to update task(s). " + e);
		}
		
		logger.info("Completed updating task(s).");
		
		return ids.toString();
	}

}
