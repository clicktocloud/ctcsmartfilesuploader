package com.ctc.people.cfm.extension.component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.ctc.people.cfm.util.UploadedFile;
import com.sforce.soap.partner.PartnerConnection;

public class ResourceRetrieverFactory {
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	
	public static final String BULKATTACHMENTS_RESOURCE = "BulkAttachments";
	public static final String BULKFILES_RESOURCE = "BulkFiles";
	
	private static Map<String, Class<?>> registeredRetrievers = new HashMap<String, Class<?>>();
	
	static{
		registeredRetrievers.put(ResourceSelector.ATTACHMENT_RESOURCE, AttachmentRetriever.class);
		registeredRetrievers.put(BULKATTACHMENTS_RESOURCE, BulkAttachmentsRetriever.class);
		registeredRetrievers.put(ResourceSelector.FILE_RESOURCE, FileRetriever.class);
		registeredRetrievers.put(BULKFILES_RESOURCE, BulkFilesRetriever.class);
		
	}

	public static ResourceRetriever getRetrieverInstance( PartnerConnection conn, ArrayList<ActivityDoc> allDocs,  String folder, String namespace){
		if(allDocs == null || allDocs.size() == 0){
			return null;
		}
		
		String resourceType = getResourceType( allDocs );
		
		return getRetrieverInstance(resourceType, conn, allDocs,folder, namespace );
	}
	
	public static ResourceRetriever getRetrieverInstance( PartnerConnection conn, ArrayList<ActivityDoc> allDocs,  String folder){
		if(allDocs == null || allDocs.size() == 0){
			return null;
		}
		
		String resourceType = getResourceType( allDocs );
		
		return getRetrieverInstance(resourceType, conn, allDocs,folder );
	}
	
	public static ResourceRetriever getRetrieverInstance( String resourceType,  PartnerConnection conn, ArrayList<ActivityDoc> allDocs,  String folder){
		Class<?> retrieverClass = registeredRetrievers.get(resourceType);
		if( retrieverClass == null)
			return null;
		
		Class<?>[] types = {PartnerConnection.class , ArrayList.class, String.class };
		Object[] objs = {conn, allDocs, folder};
		
		try {
			Constructor<?> cons = retrieverClass.getDeclaredConstructor(types);
			return (ResourceRetriever) cons.newInstance( objs );
		} catch (NoSuchMethodException e) {
			
			logger.error( e);
			
		} catch (SecurityException e) {
			logger.error( e);
			
		} catch (InstantiationException e) {
			logger.error( e);
		} catch (IllegalAccessException e) {
			logger.error( e);
		} catch (IllegalArgumentException e) {
			logger.error( e);
		} catch (InvocationTargetException e) {
			logger.error( e);
		}
		
		return null;
	}
	
	public static ResourceRetriever getRetrieverInstance( String resourceType,  PartnerConnection conn, ArrayList<ActivityDoc> allDocs,  String folder, String namespace){
		Class<?> retrieverClass = registeredRetrievers.get(resourceType);
		if( retrieverClass == null)
			return null;
		
		Class<?>[] types = {PartnerConnection.class , ArrayList.class, String.class,String.class };
		Object[] objs = {conn, allDocs, folder, namespace};
		
		try {
			Constructor<?> cons = retrieverClass.getDeclaredConstructor(types);
			return (ResourceRetriever) cons.newInstance( objs );
		} catch (NoSuchMethodException e) {
			
			logger.error( e);
			
		} catch (SecurityException e) {
			logger.error( e);
			
		} catch (InstantiationException e) {
			logger.error( e);
		} catch (IllegalAccessException e) {
			logger.error( e);
		} catch (IllegalArgumentException e) {
			logger.error( e);
		} catch (InvocationTargetException e) {
			logger.error( e);
		}
		
		return null;
	}
	
	private static String getResourceType(ArrayList<ActivityDoc> allDocs){
		if(allDocs == null || allDocs.size() == 0){
			return "";
		}
		
		ActivityDoc doc = allDocs.get(0);
		
		String resourceType = "";
		if(doc.getResourceType() != null 
				&& !doc.getResourceType().equals("") 
				&& doc.getResourceId() != null
				&& !doc.getResourceId().equals("") ){
			
			resourceType = doc.getResourceType();
			
		}else if(doc.getAttachmentId() != null && !doc.getAttachmentId().equals("")){
			resourceType = ResourceSelector.ATTACHMENT_RESOURCE;
		}
		
		return resourceType;
		
	}
	
	/**
	 * concrete a CTCFilesOwner object:
	 * 1. create local temp file for each downloaded resource from salesforce
	 * 2. set properties values of CTCFilesOwner object by using ActivityDoc object
	 * 3. create CTCFile object for each resource and assign it to the CTCFilesOwner object
	 * 
	 * @param filesOwner - CTCFilesOwner
	 * @param contents -  List<ResourceContent>
	 * @param doc - ActivityDoc
	 * @param namespace - String
	 * @param folder - String
	 * 
	 * @return CTCFilesOwner
	 */
	public static CTCFilesOwner fillFilesOwner(CTCFilesOwner filesOwner, List<ResourceContent> contents, ActivityDoc doc, String namespace, String folder){
		
		if( doc == null)
			return filesOwner;
		
		
		FileOutputStream file = null;
				
		try {
			
			//set properties values of CTCFilesOwner object by using ActivityDoc object
			String whoId = "";
			String whoIdField = "";
			String whatId = "";
			//String whatIdField = "";
			Map<String, String> whatMap = doc.getWhatMap();
			
			if(doc.getWhoMap()!= null && doc.getWhoMap().size()>0) {
				for(String Id : doc.getWhoMap().keySet()) {
					whoId = Id;
					whoIdField = doc.getWhoMap().get(Id);
				}
			}
			
			if(doc.getWhatMap()!= null && doc.getWhatMap().size()>0) {
				for(String Id : doc.getWhatMap().keySet()) {
					whatId = Id;
					break;
				}
			}
			
			//select a Id as owner Id from contact id, who id or what id.
			String ownerId = doc.getContactId();
			if(ownerId == null || ownerId.equals("")){
				ownerId = whoId;
			}
			
			if(ownerId == null || ownerId.equals("")){
				ownerId = whatId;
			}
					
			filesOwner.setUserId(doc.getUserId());
			filesOwner.setOwnerId(  ownerId );
			filesOwner.setOrgId(  doc.getOrgId() );
			filesOwner.setBucketName( doc.getAmazonS3Folder());
			filesOwner.setWhoId( whoId);
			filesOwner.setWhoIdField( whoIdField);
			//filesOwner.setWhatId( whatId);
			//filesOwner.setWhatIdField( whatIdField);
			filesOwner.setWhatMap(whatMap);
			filesOwner.setTaskId(  doc.getActivityId() );
			filesOwner.setNamespace( namespace);
			
			if(doc.getDocType().equalsIgnoreCase( Constant.RESUME_DOC_TYPE) ) {  
				filesOwner.setSkillGroups( doc.getSkillGroupIds());
				filesOwner.setEnableSkillParsing( ActivityDoc.isSkillParsing( doc.getEnableSkillParsing()));
				
			}
			
			if(contents != null){
				//create CTCFile object for each resource and assign it to the CTCFilesOwner object
				for(ResourceContent content : contents){
					
					//create local temp file for each downloaded resource from salesforce
					
					UploadedFile uploadedFile = new UploadedFile(content.getName());
					uploadedFile.setUploadFolder(folder);
					
					
					String originalName = uploadedFile.getOriginalFileName();
					
					String modifiedName = uploadedFile.getNewFileName();                        //Change the Document name to valid name
					String path = uploadedFile.getFullNewFileName();                                     //Set the document path according the folder and modified Name
					byte[] documentContent = Base64.decodeBase64( content.getContent() );  //convert the body type from base64 to byte
					file = new FileOutputStream (path);
					file.write(documentContent);
					file.close();

					
					CTCFile ctcFile= new CTCFile();
					ctcFile.setOwner(filesOwner);
					ctcFile.setResourceId( content.getId() );
					ctcFile.setOriginalFileName(originalName);
					ctcFile.setNewFileName(modifiedName);
					ctcFile.setPath(path);
					ctcFile.setType(doc.getDocType());
					ctcFile.setSize( uploadedFile.getFileSize() );
					
					filesOwner.getFiles().add( ctcFile );	
					
				}
			}
			
			
		} catch (Exception e) {
			logger.error("Failed to fill CTCFilesOwner object." + e );
			throw new ResourceDownloadException("Failed to fill CTCFilesOwner object." + e);
		} finally{
			if(file != null){
				try {
					file.close();
				} catch (IOException e) {
					
				}
			}
		}
		
		return filesOwner;
	}
}
