package com.ctc.people.cfm.extension.component;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.ActivityDoc;
import com.sforce.soap.partner.PartnerConnection;

/**
 * This factory class provides a group of methods to create an instance of ResourceSelector depending on the resource type of ActivityDoc object
 * At present, this factory can create one of resource selectors:
 * 1. AttachmentSelector 
 * 2. FileSelector 
 * 
 * @author andy
 *
 */
public class ResourceSelectorFactory {
	static Logger logger = Logger.getLogger("com.ctc.people");
	private static Map<String, Class<?>> registeredSelectors = new HashMap<String, Class<?>>();
	
	//initialize the list of registered selectors
	//to support more types of resource selectors, we can extend this list
	static{
		registeredSelectors.put(ResourceSelector.ATTACHMENT_RESOURCE, AttachmentSelector.class);
		registeredSelectors.put(ResourceSelector.FILE_RESOURCE, FileSelector.class);
	}
	
	/**
	 * create an instance of ResourceSelector depending on the resource type of ActivityDoc object
	 * 
	 * @param conn - PartnerConnection
	 * @param doc - ActivityDoc
	 * @param namespace - String
	 * @return ResourceSelector
	 */
	public static ResourceSelector getResourceSelector(PartnerConnection conn, ActivityDoc doc, String namespace){
		//get resource type
		String resourceType = doc.getResourceType();
		if(resourceType == null || resourceType.equals("")){
			resourceType = ResourceSelector.ATTACHMENT_RESOURCE;
		}
		
		//select the corresponding class of resource type
		Class<?> selectorClass = registeredSelectors.get(resourceType);
		if( selectorClass == null)
			return null;
		
		//build the parameters types and values of constructor
		Class<?>[] types = {PartnerConnection.class ,ActivityDoc.class,String.class };
		Object[] objs = {conn, doc, namespace};
		
		try {
			//create an instance of resource selector by using reflection
			Constructor<?> cons = selectorClass.getDeclaredConstructor(types);
			ResourceSelector selector = (ResourceSelector) cons.newInstance( objs );
			
			logger.debug("ResourceSelector : " + selector.getClass().getSimpleName());
			return selector;
		} catch (Exception e) {
			logger.error(e);
		}
		
		return null;
	}
	
}
