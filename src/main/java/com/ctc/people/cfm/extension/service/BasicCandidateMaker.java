package com.ctc.people.cfm.extension.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.adaptor.aws.rds.RDSConnection;
import com.ctc.people.cfm.adaptor.aws.s3.FileDownloader2S3;
import com.ctc.people.cfm.adaptor.daxtra.CtcResumeParser;
import com.ctc.people.cfm.adaptor.other.FilesRemover;
import com.ctc.people.cfm.adaptor.salesforce.ContactUpdater;
import com.ctc.people.cfm.adaptor.salesforce.EducationCreator;
import com.ctc.people.cfm.adaptor.salesforce.EmploymentCreator;
import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.adaptor.salesforce.SkillsCreator;
import com.ctc.people.cfm.component.CTCFileRetriever;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.DaxtraAccount;
import com.ctc.people.cfm.entity.OwnerParcel;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.FilesUploadingException;
import com.ctc.people.cfm.exception.SalesforceException;
import com.ctc.people.cfm.util.CFMHelper;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;

/**
 * This class accepts a group of CTCFilesOwner as parameter and uploads their files as bulk
 *  
 * @author Andy
 *
 */
public class BasicCandidateMaker {
	static Logger logger = Logger.getLogger("com.ctc.people");
	final private static String PROPERTY_PARSER_ACCOUNT = "account";
	final private static String PROPERTY_PARSER_URL = "wsdlURL";
	
	
	public static void make(List<CTCFilesOwner> owners){
		if(owners == null || owners.size() == 0)
			return;
		
		SforgEx sforg = SfHelper.getSfOrg(owners.get(0).getOrgId());
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ owners.get(0).getOrgId() +" ], return back.");
			return;
		}
		
		make(owners, sforg);
		
	}
	public static void make(List<CTCFilesOwner> owners,String bucketName){
		if(owners == null || owners.size() == 0)
			return;
		
		SforgEx sforg = SfHelper.getSfOrg(owners.get(0).getOrgId());
		
		if(sforg == null){
			logger.error("Failed to get the configuration of Org [ "+ owners.get(0).getOrgId() +" ], return back.");
			return;
		}
		
		if(!StringUtils.isEmpty(bucketName)){
			sforg.setBucketName(bucketName);
		}
		
		make(owners, sforg);
		
	}
	public static void  make(List<CTCFilesOwner> owners, SforgEx sforg){
		
		
		
		new BasicCandidateMaker(owners, sforg).make();
		
	}
	
	private List<CTCFilesOwner> owners;
	private SforgEx sforg;
	
	/**
	 * Constructor 
	 * @param owners
	 * @param sforg
	 */
	public BasicCandidateMaker(List<CTCFilesOwner> owners, SforgEx sforg) {
		this.owners = owners;
		this.sforg = sforg;	
	}
	
	/**
	 * 
	 * @throws FilesUploadingException
	 */
	public void make() throws FilesUploadingException{
	
		logger.info("****************************************");
		logger.info("Basic Candidates Maker is running...");
		
		String orgId = owners.get(0).getOrgId();

		// get connection to client's instance
		PartnerConnection conn = sforg.getConn();
		if( conn == null ){
			conn = SfHelper.login( orgId );
			sforg.setConn(conn);
		}
		
		if( conn == null ){
			logger.error("Failed to get connection to Salesforce instance [ " + orgId +" ]");
			throw new FilesUploadingException( "Failed to get connection to Salesforce instance [ " + orgId +" ]" ) ;
		}
		
		String namespace = SalesforceAgent.getNamespace(orgId);
		
		List< OwnerParcel > parcels = new ArrayList<OwnerParcel>();
		for(CTCFilesOwner owner : owners){
			
			if(owner.getNamespace() == null){
				owner.setNamespace(namespace);
			}
			
			OwnerParcel parcel = make(owner, sforg);
			if(parcel != null){
				parcels.add( parcel );
			}
		}
		
		List<CTCFile> allfiles = OwnerParcel.getAllFiles(parcels);
		
		try {
		
			//mass upsert candidates profiles and relevant informations, such as skills, employments and educations
			List<OwnerParcel> parcelsWithCandidate = OwnerParcel.getParcelsWithCandidate(parcels);
			massUpsertCandidates( conn, parcelsWithCandidate );
			
			
		
		} catch (Exception e) {
			logger.error(orgId + ": Failed to upload files " +e );
			throw new FilesUploadingException("Failed to upload files !",e);
		}finally{
			FilesRemover.remove(allfiles);
			
			logger.info("Basic Candidate Maker service end ! ");
			logger.info("****************************************");
		}
	}
	
	
	private Properties buildDaxtraProperties( String orgId){
		DaxtraAccount da = RDSConnection.connectToRDS( orgId ) ;
		
		
		//set properties for daxtra account
		Properties properties = new Properties();
		if (da != null) {
			
			properties.setProperty( PROPERTY_PARSER_ACCOUNT , da.getDaxtraaccount());
			properties.setProperty(PROPERTY_PARSER_URL, da.getDaxtrawsdlurl());
			logger.info("Use org daxtra account.");
		} else {
			String daxtraAccount = CFMHelper.getValueFromPropertyFile(
					PROPERTY_PARSER_ACCOUNT, 
					Constant.PROPERTIES_FILENAME);
			String wsdlURL = CFMHelper.getValueFromPropertyFile(
					PROPERTY_PARSER_URL,
					Constant.PROPERTIES_FILENAME);
			
			properties.setProperty(PROPERTY_PARSER_ACCOUNT, daxtraAccount);
			properties.setProperty(PROPERTY_PARSER_URL, wsdlURL);
			
		}
		
		return properties;
	}
	
	
	private OwnerParcel make(CTCFilesOwner owner, SforgEx sforg) throws SalesforceException{
		
		OwnerParcel parcel = new OwnerParcel();
		
		parcel.setOwner(owner);
		
		
		FileDownloader2S3.download(sforg, owner.getFiles());
		
		
		/*
		 * Extract all files of current CTCFilesOwner data object, try to find out the resume file if existing 
		 * and create temp physical files on local file system if they does not exist
		 */
		logger.info("Retrieving files from standard data package of CTCFilesOwner object");
		CTCFileRetriever fileRtrv = new CTCFileRetriever(
				owner, Constant.ATTACHMENT_FOLDER);
		fileRtrv.retrieve();
		
		List<CTCFile> files = fileRtrv.getFiles();
		CTCFile resume = fileRtrv.getResume();

		if( files == null || files.size() == 0 ){
			logger.error("Failed to retrieve any file, abort!");
			throw new FilesUploadingException( "Failed to retrieve any file ! ");
		}
		
		logger.info("\t Total files number : " + files.size());
		

		// parse resume
		// do the following only if the resume is not null
		if (resume != null) {
			
			parcel.setResume( resume );
			
			//parse resume via Daxtra
			resume.setSforg(sforg);
			Properties properties = buildDaxtraProperties( owner.getOrgId() );
			CtcResumeParser parser = new CtcResumeParser(properties, resume);
			parser.parse();
			Candidate candidate = parser.getCandidate();
			
			candidate.setId( resume.getOwner().getWhoId());
			
			parcel.setCandidate(candidate);
	
		}
		
		return parcel;
	}
	
	
	private List<OwnerParcel> massUpsertCandidates(PartnerConnection conn, List<OwnerParcel> parcels){
		
		if(parcels == null || parcels.size() == 0 )
			return null;
		
		//upsert contact profiel
		List<OwnerParcel> successParcels = ContactUpdater.massUpsert(conn, parcels);
		
		if(successParcels == null || successParcels.size() == 0)
			return null;
		
		OwnerParcel.attachAllFilesToCandidates(successParcels);
		
		if(successParcels.get(0).getOwner().isEnableSkillParsing()){
			//upsert skills
			SkillsCreator.massUpsert(conn, successParcels);
			
			//upsert employmentHistory
			EmploymentCreator.create(conn, successParcels);
			
			//upsert educationHisotry
			EducationCreator.create(conn, successParcels);
		}
		
		
		
		return successParcels;
		
	}
	
}
