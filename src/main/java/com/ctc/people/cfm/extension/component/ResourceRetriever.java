package com.ctc.people.cfm.extension.component;

import java.util.List;

import com.ctc.people.cfm.entity.CTCFilesOwner;

/**
 * This interface declares two methods to retrieve resources from salesforce and get all of the CTCFilesOwners owning these resources
 * 
 * @author andy
 *
 */
public interface ResourceRetriever {

	/**
	 * retrieve resources from salesforce
	 */
	void retrieve();
	
	/**
	 * get all of the CTCFilesOwners owning these resources
	 * 
	 * @return List<CTCFilesOwner>
	 */
	List<CTCFilesOwner> getFilesOwners();
	
}
