package com.ctc.people.cfm.component;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.clicktocloud.resumeparser.exception.DaxtraResumeParserException;
import com.ctc.entity.Candidate;
import com.ctc.people.cfm.adaptor.aws.rds.RDSConnection;
import com.ctc.people.cfm.adaptor.aws.s3.UploadS3Runnable;
import com.ctc.people.cfm.adaptor.daxtra.CtcResumeParser;
import com.ctc.people.cfm.adaptor.salesforce.InsertSfRunnable;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.DaxtraAccount;
import com.ctc.people.cfm.monitor.CandidateImportMonitor;
import com.ctc.people.cfm.util.CFMHelper;

/**
 * This class is foundation class, which execute the whole processes of parsing resume, upload resume to s3 and save candidate to salesforce.
 * For higher performance, this class launches two concurrently threads:
 * one for uploading resume to s3 
 * another for saving candidate to salesforce
 * 
 * @author Andy and Jack
 *
 */
public class SingleCandidateParser {
	private static Logger logger = Logger.getLogger(SingleCandidateParser.class);
	final private static String PROPERTY_PARSER_ACCOUNT = "account";
	final private static String PROPERTY_PARSER_URL = "wsdlURL";
	
	
	/**
	 * Parse a resume file
	 * 
	 * @param resume
	 * @param errorMsgMap
	 * @param successfulCandidateMap
	 * @param threadList
	 * @throws IOException
	 * @throws DaxtraResumeParserException
	 */
	public void parse(CTCFile resume, CandidateImportMonitor importMonitor ) {

		logger.debug("skillGroupList =" + resume.getOwner().getSkillGroups() );
		
		//get Daxtra account
		logger.info("Get a daxtra parser account");
		DaxtraAccount daxtraAcc = RDSConnection.connectToRDS(resume.getOwner().getOrgId());
		//set properties for daxtra account
		Properties properties = new Properties();
		if (daxtraAcc != null) {
			
			properties.setProperty( PROPERTY_PARSER_ACCOUNT , daxtraAcc.getDaxtraaccount());
			properties.setProperty(PROPERTY_PARSER_URL, daxtraAcc.getDaxtrawsdlurl());
			logger.info("Use org daxtra account.");
		} else {
			String daxtraAccount = CFMHelper.getValueFromPropertyFile(
					PROPERTY_PARSER_ACCOUNT, 
					Constant.PROPERTIES_FILENAME);
			String wsdlURL = CFMHelper.getValueFromPropertyFile(
					PROPERTY_PARSER_URL,
					Constant.PROPERTIES_FILENAME);
			
			properties.setProperty(PROPERTY_PARSER_ACCOUNT, daxtraAccount);
			properties.setProperty(PROPERTY_PARSER_URL, wsdlURL);
			logger.info("Use default daxtra account.");
		}

		//call Daxtra Parser remotely to parse resume to Candidate object
		CtcResumeParser parser = new CtcResumeParser(properties,resume);
		parser.parse();
		//get candidate object
		Candidate candidate = parser.getCandidate();
		
		logger.info("Parsed to a Candidate:");
		logger.info("\t FirstName : "+ candidate.getFirstName());
		logger.info("\t LastName : "+ candidate.getLastName());
		logger.info("\t Email : "+ candidate.getEmail());
		
		logger.debug("Candidate skill groups size: " + candidate.getSkills().size());
		logger.debug("Candidate employments size: " + candidate.getEmploymentHistory().size());
		logger.debug("Candidate educations size: " + candidate.getEducationHistory().size());

		String whoId = resume.getOwner().getWhoId();
		logger.debug("whoId : " + whoId);
		boolean candidateExist = !(whoId == null || whoId.equals(""));
		if (candidateExist || handleUnparsedFields(candidate, resume.getNewFileName(), resume.getOwner().getOrgId(), importMonitor )) {
			
			// S3 upload thread
			logger.info("Running the thread of uploading to S3 !");
			UploadS3Runnable s3uploadT = new UploadS3Runnable( resume,  importMonitor);
			Thread t1 = new Thread(s3uploadT);
			t1.setName("S3Thread_" +  resume.getNewFileName());
			// add the thread created to threadList
			importMonitor.addThread(t1);
			
			// Salesforce insertion thread
			logger.info("Running the thread of upserting to Salesforce !");
			InsertSfRunnable insertSfT = new InsertSfRunnable(candidate,resume, importMonitor);
			Thread t2 = new Thread(insertSfT);
			t2.setName("SFThread_" + resume.getNewFileName());

			// add the thread created to threadList
			importMonitor.addThread(t2);
			
			t1.start();
			t2.start();

			
		}
	}
	
	
	/**
	 * 
	 * if first name and last name are null, set them as "unknown"
	 * if no email, no first name or no last name, return false
	 * 
	 * @param c
	 * @param errorMsgMap
	 * @param uploadedResume
     * @param orgId
	 * @return
	 */
	public boolean handleUnparsedFields(Candidate c,  String resumeName, String orgId, CandidateImportMonitor importMonitor){
		
		if(c.getFirstName() != null && c.getLastName() == null && c.getEmail() != null){
			c.setLastName("unknown");
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap, resumeName, "ParserError", "Failed to parse the last name.");
			logger.warn("Daxtra parser failed to get the last name from the resume.\tOrg Id:" + orgId + "\tFile:" + resumeName);
		}
		
		else if(c.getFirstName() == null && c.getLastName() != null && c.getEmail() != null){
			c.setFirstName("unknown");
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap, resumeName, "ParserError", "Failed to parse the first name.");
			logger.warn("Daxtra parser failed to get the first name from the resume.\tOrg Id:" + orgId + "\tFile:" + resumeName);	
				
		}
		
		else if(c.getFirstName() == null && c.getLastName() == null && c.getEmail() != null){
			c.setFirstName("unknown");
			c.setLastName("unknown");
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap, resumeName, "ParserError", "Failed to parse candidate names.");
			logger.warn("Daxtra parser failed to get the first name and last name from the resume.\tOrg Id:" + orgId + "\tFile:" + resumeName);			
		}
		
		else if(c.getEmail() == null){
			
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap, resumeName, "NoEmailError", "Failed to parse first name, last name or email.");
			importMonitor.addErrorMsg(resumeName, "NoEmailError", "Failed to parse first name, last name or email.");
			logger.error("Email is not parsed.\t" + "Org Id:" + orgId + "\tFile:" + resumeName);
			return false;
		}
		
		return true;
	}
	
	
}
