package com.ctc.people.cfm.component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.util.UploadedFile;

/**
 * this class is responsible for extracting all files of current CTCFilesOwner data object, 
 * try to find out the resume file if existing and create temp physical files on local file 
 * system if they does not exist. By using the class, you can easily :
 * 1. create temp physical file for each file of owner
 * 2. get all files of one owner;
 * 3. get non-resume files of one owner;
 * 4. get resume file of one onwer;
 * 5. attach all files to specified whoId
 * 6. attach non-resume files to specified whoId
 * 
 * @author andy
 *
 */
public class CTCFileRetriever {
	
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	private CTCFilesOwner owner;
	private String folder;
	private String namespace;
	
	private List<CTCFile> files;
	private CTCFile resume;
	

	/**
	 * Constructor 
	 * @param owner
	 * @param folder
	 */
	public CTCFileRetriever(CTCFilesOwner owner, String folder) {
		this.owner = owner;
		this.folder = folder;
		this.namespace = owner.getNamespace();
		files= new ArrayList<CTCFile>();
		
		//check and create the folder to hold the temp files
		CTCFileUtils.checkFolder(folder);
	}
	
	/**
	 * Constructor with specified namespace
	 * @param owner
	 * @param folder
	 * @param namespace
	 */
	public CTCFileRetriever(CTCFilesOwner owner, String folder, String namespace){
		this( owner, folder );
		
		this.namespace = namespace;
		
	}

	public void retrieve() {
		if(owner == null || owner.getFiles() == null || owner.getFiles().size() == 0 ){
			return ;
		}
		
		
		try {
			for(CTCFile ctcFile: owner.getFiles()){
				
				if(ctcFile.getOwner() == null){
					ctcFile.setOwner( owner );
				}
				
				
				
				//if file path is not set, then assign a value to path 
				if(ctcFile.getPath() == null 
						|| ctcFile.getPath().equals("") ){
					
					
					if(ctcFile.getNewFileName() == null || ctcFile.getNewFileName().equals("")) {
						ctcFile.setNewFileName( UploadedFile.makeUniqueFileName(ctcFile.getOriginalFileName()));
					}
					
					ctcFile.setPath( folder + ctcFile.getNewFileName() );	
				}
				
				//create temp physical file for each CTCFile object if it does not exist
				if(! new File( ctcFile.getPath() ).exists() ){
					
					//ignore the file whose content is null
					if(ctcFile.getFileContent() == null){
						logger.warn(" File content is empty, abort to create file: " + ctcFile.getPath());
						continue;
					}
					
					CTCFileUtils.generateFile(ctcFile.getFileContent(), ctcFile.getPath());
					
					
				}
				
				if( ctcFile.getSize() == null){
					ctcFile.setSize(UploadedFile.calculateDisplayFileSize(ctcFile.getPath()));
				}
				 

				files.add(ctcFile);                             
				
				//identify resume file
				if(ctcFile.getType()!=null && ctcFile.getType().equalsIgnoreCase(Constant.RESUME_DOC_TYPE)) {  
					resume = ctcFile;
				}
			}
		} catch (Exception e) {
			logger.error(e);
			
		}finally{
			
		}
	}

	/**
	 * get all files
	 * 
	 * @return List<CTCFile>
	 */
	public List<CTCFile> getFiles() {
		return files;
	}
	
	/**
	 * attach non resume files to specified whoId
	 * @param whoId
	 * @return List<CTCFile>
	 */
	public List<CTCFile> attachNonResumeFilesTo( String whoId ) {
		
		List<CTCFile> otherfiles = new ArrayList<CTCFile>();
		if(files == null || files.size() == 0 ){
			return otherfiles;
		}
		for (CTCFile file : files) {
			if (!file.equals(resume)) {
				if( file.getType() == null || file.getType().equals("")){
					file.setType( Constant.OTHER_DOC_TYPE );
				}
				file.getOwner().setWhoId(whoId);
				if(StringUtils.isEmpty(file.getOwner().getWhoIdField())){
					file.getOwner().setWhoIdField(namespace + Constant.WEB_DOC_FIELD_WHO_ID);
				}
				
				otherfiles.add(file);
			}
		}
		
		return otherfiles;
	}
	
	/**
	 * Attach all files to specified whoId
	 * @param whoId
	 * @return  List<CTCFile>
	 */
	public List<CTCFile> attachFilesTo( String whoId ) {
		
		if(files == null || files.size() == 0 ){
			return files;
		}
		for (CTCFile file : files) {
			
			if( file.getType() == null || file.getType().equals("")){
				file.setType( Constant.OTHER_DOC_TYPE );
			}
			file.getOwner().setWhoId(whoId);
			if(StringUtils.isEmpty(file.getOwner().getWhoIdField())){
				file.getOwner().setWhoIdField(namespace + Constant.WEB_DOC_FIELD_WHO_ID);
			}	
		}
		return files;
	}
	
	/**
	 * get non-resume files
	 * @return List<CTCFile>
	 */
	public List<CTCFile> getNonResumeFiles( ) {
		
		List<CTCFile> otherfiles = new ArrayList<CTCFile>();
		if(files == null || files.size() == 0 ){
			return otherfiles;
		}
		for (CTCFile file : files) {
			if (!file.equals(resume)) {
				if( file.getType() == null || file.getType().equals("")){
					file.setType( Constant.OTHER_DOC_TYPE );
				}
				
				otherfiles.add(file);
			}
		}
		
		return otherfiles;
	}

	/**
	 * get resume file
	 * 
	 * @return CTCFile
	 */
	public CTCFile getResume() {
		return resume;
	}
	

	public String getNamespace() {
		return namespace;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}	
	

	
}
