package com.ctc.people.cfm.component;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.exception.ResumeParsingException;
import com.ctc.people.cfm.exception.SalesforceException;
import com.ctc.people.cfm.monitor.CandidateImportMonitor;

/**
 * A runnalbe implement runs as a thread which invokes SingleCandidateParser instance to parse a resume file.
 * Please refer to {@link com.ctc.people.cfm.component.SingleCandidateParser}
 * 
 * @author andy
 *
 */
public class SingleCandidateParseRunner implements Runnable {
	
	private static Logger logger = Logger.getLogger(SingleCandidateParseRunner.class);
	
	private CandidateImportMonitor importMonitor;
    private CTCFile resume;


    /**
     * Constructor
     * 
     * @param resume
     * @param errorMap
     * @param successfulCandidate
     * @param tList
     */
	public SingleCandidateParseRunner(CTCFile resume, CandidateImportMonitor importMonitor){
		
		this.resume = resume;
		this.importMonitor = importMonitor;
		
		
	}
	
	@Override
	public void run(){
		
		SingleCandidateParser scp = new SingleCandidateParser();

		try {
			// parse resume to a single candidate
			scp.parse(this.resume, this.importMonitor);
			
		} catch(SalesforceException e){
			
			importMonitor.addErrorMsg(resume.getNewFileName(), CandidateImportMonitor.SF_ERROR, e.getMessage());
			
		} catch(ResumeParsingException e){
			importMonitor.addErrorMsg(resume.getNewFileName(), CandidateImportMonitor.PARSER_ERROR, e.getMessage());
		}
		catch(Throwable e){
			importMonitor.addErrorMsg( resume.getNewFileName(), CandidateImportMonitor.OTHER_ERROR, e.getMessage());
			logger.error("Other error for Org Id:" + resume.getOwner().getOrgId() + "\tFile:" + resume.getNewFileName() + " - "+ e.getMessage());
		} 
    }
}