package com.ctc.people.cfm.exception;

public class EducationCreateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8674123622293306467L;

	public EducationCreateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EducationCreateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EducationCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EducationCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EducationCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
