package com.ctc.people.cfm.exception;

public class ImportCandidateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ImportCandidateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImportCandidateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ImportCandidateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ImportCandidateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ImportCandidateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
