package com.ctc.people.cfm.exception;

public class WebDocCreateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5819668242631750211L;

	public WebDocCreateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WebDocCreateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public WebDocCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WebDocCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WebDocCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
