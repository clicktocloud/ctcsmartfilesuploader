package com.ctc.people.cfm.exception;

public class SkillCreateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8310284243587134497L;

	public SkillCreateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SkillCreateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SkillCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SkillCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SkillCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
