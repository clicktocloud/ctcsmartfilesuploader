package com.ctc.people.cfm.exception;

public class CandidateManagementCreateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CandidateManagementCreateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CandidateManagementCreateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CandidateManagementCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CandidateManagementCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CandidateManagementCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
