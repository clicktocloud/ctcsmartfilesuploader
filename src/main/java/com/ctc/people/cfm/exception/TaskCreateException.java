package com.ctc.people.cfm.exception;

public class TaskCreateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7408124896364806024L;

	public TaskCreateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaskCreateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TaskCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TaskCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TaskCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
