package com.ctc.people.cfm.exception;

public class FilesUploadingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FilesUploadingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FilesUploadingException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public FilesUploadingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FilesUploadingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FilesUploadingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
