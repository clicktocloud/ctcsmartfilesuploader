package com.ctc.people.cfm.exception;

public class EmploymentCreateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1972245356184606267L;

	public EmploymentCreateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmploymentCreateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EmploymentCreateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmploymentCreateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmploymentCreateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
