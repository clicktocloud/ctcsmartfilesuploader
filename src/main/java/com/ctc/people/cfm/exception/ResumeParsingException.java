package com.ctc.people.cfm.exception;

public class ResumeParsingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8835905094006230021L;

	public ResumeParsingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResumeParsingException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ResumeParsingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ResumeParsingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ResumeParsingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
