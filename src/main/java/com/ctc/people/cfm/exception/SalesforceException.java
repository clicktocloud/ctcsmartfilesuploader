package com.ctc.people.cfm.exception;

public class SalesforceException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1367645944300947775L;

	public SalesforceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SalesforceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SalesforceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SalesforceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SalesforceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
