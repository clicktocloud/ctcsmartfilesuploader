package com.ctc.people.cfm.exception;

public class ContactUpdateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContactUpdateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContactUpdateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ContactUpdateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ContactUpdateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ContactUpdateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
