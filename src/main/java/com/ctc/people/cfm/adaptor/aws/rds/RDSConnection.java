package com.ctc.people.cfm.adaptor.aws.rds;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.ctc.people.cfm.entity.DaxtraAccount;

/**
 * This class is for RDSConnect
 * It create a connection to RDS MySQL database using hibernate.cfg.xml file
 * Call RDSService.getDaxtraAccountByOrgId to return a daxtra account
 * @author Lina
 *
 */

public class RDSConnection {
	private static Logger logger = Logger.getLogger("com.ctc.people");
	
	private static Map<String, DaxtraAccount> cachedAccount = new HashMap<String, DaxtraAccount>();
	
	
	/**
	 * Connect to RDS MySQL database
	 * @param orgId
	 * @return DaxtraAccount daxtra account detail with accout name and wsdlurl
	 */
	public static synchronized DaxtraAccount connectToRDS(String orgId){
		
		DaxtraAccount account  = cachedAccount.get(orgId);
		if(account != null )
			return account;
		
		logger.info("=================================================");
		logger.info("Start connecting to RDS...");
		// the session factory
		SessionFactory factory = null;
		account = null;
		try { 
			// load mysql driver
			Class.forName("com.mysql.jdbc.Driver");
		} catch(ClassNotFoundException e) {
			logger.error("Error for load mysql driver: " + e);
			return null;
		}
		
		try {
			 // prepare a configuration file
			//SessionFactory sf = new AnnotationConfiguration() .setPersisterClassProvider(customPersisterClassProvider) .addAnnotatedClass(Order.class) .buildSessionFactory();
			
			Configuration configuration = new Configuration();
			configuration.configure(); // using hibernate.cfg.xml
			// create a service registry (for the factory)
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
			// build the factory itself
			factory = configuration.buildSessionFactory(serviceRegistry);

			// create the service object
			RDSService service = new RDSService(factory);
			if (orgId != null) {
				account = service.getDaxtraAccountByOrgId(orgId);
				
				if(account != null ){
					cachedAccount.put(account.getOrgId(), account);
				}
			}
		} catch(HibernateException e) {
			logger.error("Error for get daxtra account", e);
		} finally {
			// close database if it is open
			if (factory != null && !factory.isClosed()) {
				factory.close(); 
			}
		}
		logger.info("Finish connecting to RDS...");
		logger.info("=================================================");
		return account;
	}

}
