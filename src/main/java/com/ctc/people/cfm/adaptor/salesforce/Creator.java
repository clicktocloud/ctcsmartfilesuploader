package com.ctc.people.cfm.adaptor.salesforce;

public interface Creator {
	void create();
}
