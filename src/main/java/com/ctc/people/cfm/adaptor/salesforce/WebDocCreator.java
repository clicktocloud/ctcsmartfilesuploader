package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.JBField;
import com.ctc.people.cfm.entity.JBObject;
import com.ctc.people.cfm.exception.WebDocCreateException;
import com.ctc.people.cfm.util.CFMHelper;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;

public class WebDocCreator implements Creator {
	
	public static void create( PartnerConnection conn, List<CTCFile> files ){
		if( files == null||files.size() == 0)
			return;
		
		WebDocCreator wdCrt = new WebDocCreator(conn, files);
		wdCrt.setNsp(files.get( 0 ).getOwner().getNamespace());
		wdCrt.create();
	}
	
	private PartnerConnection conn;
	private List<CTCFile> files;
	private String nsp = ""; // namespace prefix
	static Logger logger = Logger.getLogger("com.ctc.people");

	/**
	 * constructor
	 * 
	 * @param conn
	 *            : connection to salesforce
	 * @param files
	 *            : files for creation of web docs
	 */
	public WebDocCreator(PartnerConnection conn, List<CTCFile> files) {
		this.conn = conn;
		this.files = files;
	}
	
	public WebDocCreator(PartnerConnection conn, CTCFile file) {
		this.conn = conn;
		this.files = Arrays.asList( new CTCFile[]{ file } );
	}

	/**
	 * set namespace prefix
	 * 
	 * @param nsp
	 */
	public void setNsp(String nsp) {
		this.nsp = nsp;
	}

	@Override
	public void create() {
		logger.info("Start to create web doc.");
		if(files.size() == 0 || files.get( 0 ).getOwner() == null){
			return;
		}
		
		
		List<SObject> sobjs = createSObjects( );
		
		SaveResult[] srs = null;
		
		try {
			srs = conn.create(sobjs.toArray(new SObject[] {}));
			
			for(int i=0; i<srs.length; i++){
				
				if(!srs[i].isSuccess()){
					
					com.sforce.soap.partner.Error[] errors = srs[i].getErrors();
					for(com.sforce.soap.partner.Error error : errors){
						logger.error("create web document "+Arrays.toString(error.getFields())+" : "+error.getMessage());
					}
					
					throw new WebDocCreateException(
							"Failed to create web doc files in Salesforce. " );
				}else{
					CTCFile file = files.get(i);
					if(file != null){
						file.setId(srs[i].getId());
					}
					
				}
			}
		}catch (WebDocCreateException e){
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to create web doc files in Salesforce.");
			logger.error(e);
			throw new WebDocCreateException(
					"Failed to create web doc files in Salesforce. " + e);
		}
		
		logger.info("Completed creating web doc.");
	}

	
	private List<SObject> createSObjects(){
		List<SObject> sobjs = new ArrayList<SObject>();
		
		
		for (CTCFile file : files) {
			
			SObject sobj = new SObject();
			sobjs.add(sobj);
			
			JBObject metadata = file.getMetadata();
			if(metadata != null && metadata.getFields() != null && metadata.getFields().size() > 0){
				
				logger.debug("Using metadata to create SObject for " + metadata.getObjectName());
				
				sobj.setType(metadata.getObjectName() );
				
				//List<String> existingFields = getSObjectFields(metadata.getObjectName());
				
				
				for(JBField field :  metadata.getFields()){
					//if(existingFields.contains(field.getName())){
						sobj.setField(field.getName(),field.getValue());
					//}
					
				}	
			}else{
				sobj.setType(nsp + "Web_Document__c");
			}
			
			if(sobj.getType() != null && sobj.getType().endsWith("Web_Document__c")){
				String name  = "";
				if(sobj.getField("Name") != null){
					name = (String) sobj.getField("Name");
				}
				
				if(StringUtils.isEmpty( name )){
					if(StringUtils.isEmpty(file.getAlternativeName())){
						sobj.setField("Name", CFMHelper.fixWebDocFileName( file.getOriginalFileName()));
					}else{
						sobj.setField("Name", CFMHelper.fixWebDocFileName( file.getAlternativeName()));
					}
					
				}
				
				
				boolean hasWhoId = false;
				if(file.getOwner().getWhoId() != null && file.getOwner().getWhoId() != "" && !file.getOwner().getWhoId().equalsIgnoreCase("null") ) {
					
					String whoIdFieldApiName = file.getOwner().getWhoIdField();
					if(! StringUtils.isEmpty(whoIdFieldApiName) && ! "null".equalsIgnoreCase(whoIdFieldApiName)){
//						if(! StringUtils.isEmpty(nsp) && ! whoIdFieldApiName.toLowerCase().startsWith(nsp.toLowerCase())){
//							whoIdFieldApiName = nsp + whoIdFieldApiName;
//							
//						}
						sobj.setField(whoIdFieldApiName,file.getOwner().getWhoId());
						hasWhoId = true;
					}
					
				}
				
				boolean hasWhatId = false;
				if(file.getOwner().getWhatMap() != null){
					for(String whatId : file.getOwner().getWhatMap().keySet()){
						if(whatId.equalsIgnoreCase("null")){
							continue;
						}
						String whatIdField =  file.getOwner().getWhatMap().get( whatId );
						if(! StringUtils.isEmpty(whatIdField) && ! "null".equalsIgnoreCase(whatIdField)){
//							if(! StringUtils.isEmpty(nsp) && ! whatIdField.toLowerCase().startsWith(nsp.toLowerCase())){
//								whatIdField = nsp + whatIdField;
//								
//							}
							sobj.setField(whatIdField, whatId);
							hasWhatId = true;
						}
					}	
				}
//					if(file.getOwner().getWhatId() != null && file.getOwner().getWhatId() != "" && !file.getOwner().getWhatId().equalsIgnoreCase("null") ) {
//						sobj.setField(file.getOwner().getWhatIdField(),file.getOwner().getWhatId());
//					}
				
				if(!hasWhoId && ! hasWhatId){
					logger.error("Failed to create web doc file: " + file.getOriginalFileName()+" in Salesforce, as this activity does not have whoId and WhatId");
					continue;
				}
				sobj.setField(nsp + "ObjectKey__c", file.getNewFileName());
				sobj.setField(nsp + "S3_Folder__c", file.getOwner().getBucketName());
				sobj.setField(nsp + "Type__c", file.getType());
				sobj.setField(nsp + "Document_Type__c", file.getType());
				sobj.setField(nsp + "File_Size__c", file.getSize().toString());
				
				//
				if(! StringUtils.isEmpty( file.getOwner().getUserId() )){
					//logger.info("Change owner to " + file.getOwner().getUserId());
					sobj.setField("OwnerId", file.getOwner().getUserId());
				}
			}	
		}
			
			
		return sobjs;
		
	}
	/*
	private List<String> getSObjectFields(String objectName) {
		
		List<String> fieldNames = new ArrayList<String>();
		
	    try {
	
	       
	        DescribeSObjectResult describeSObjectResult = conn.describeSObject(objectName);
	
	        // Get sObject metadata
	        if (describeSObjectResult != null) {
	        	// Get the fields
	
	        Field[] fields = describeSObjectResult.getFields();
	

	        // Iterate through each field and gets its properties

	        for (int i = 0; i < fields.length; i++) {
	
	          
	        	fieldNames.add(fields[i].getName());
	
	          }

	      }
	
	    } catch (ConnectionException ce) {
	    	logger.error(ce);
	    }
	    
	    return fieldNames;
	
	}*/

}
