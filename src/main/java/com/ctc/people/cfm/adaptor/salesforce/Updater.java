package com.ctc.people.cfm.adaptor.salesforce;

public interface Updater {
	String update();
}
