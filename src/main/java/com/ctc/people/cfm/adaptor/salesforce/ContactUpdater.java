package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.OwnerParcel;
import com.ctc.people.cfm.entity.SfContactInsertFields;
import com.ctc.people.cfm.entity.SfContactUpdateFields;
import com.ctc.people.cfm.exception.ContactUpdateException;
import com.ctc.people.cfm.exception.SalesforceException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

/**
 * this class updates contact with resume content
 * 
 * @author kwu
 * 
 */
public class ContactUpdater implements Updater {
	
	public static List<OwnerParcel> massUpsert (PartnerConnection conn, List<OwnerParcel> parcels)  {
		
		List<OwnerParcel> successfulCandidateParcels = new ArrayList<OwnerParcel>();
		
		List<SObject> sobjs = new ArrayList<SObject>();
		
		String rid = SfHelper.getRecordTypeId(conn, Constant.DEFAULT_CONTACT_RECORD_TYPE);
		
		if( parcels != null){
			for(OwnerParcel parcel : parcels){
				if( parcel.getCandidate() != null && parcel.getResume() != null){
					
					SObject sobj = buildContactSObject(parcel.getCandidate(), parcel.getResume(), rid);
					sobjs.add(sobj);
				}	
			}
		}
		if( sobjs.size() == 0 ){
			return successfulCandidateParcels;
		}

		try {
			SaveResult[] srs = null;
			
			String id = sobjs.get(0).getField("Id") == null ? "" : (String) sobjs.get(0).getField("Id");
			if(id.equals("")){
				srs = SfHelper.massCreate(conn,sobjs.toArray(new SObject[]{}));
				
			}else{
				srs = SfHelper.massUpdate(conn,sobjs.toArray(new SObject[]{}));
				
			}
			
			for(int i=0; i<srs.length; i++){
				if(srs[i].isSuccess()){
					logger.debug("successful candidate " + (i+1) +":" +  srs[i].getId() );
					parcels.get(i).getCandidate().setId(srs[i].getId());   		 // set the successful candidate id
					successfulCandidateParcels.add(parcels.get(i));     // put the successful candidate to successful candidate list
				}
				if(!srs[i].isSuccess()){
					com.sforce.soap.partner.Error[] errors = srs[i].getErrors();
					for(com.sforce.soap.partner.Error error : errors){
						logger.error("insert contacts "+Arrays.toString(error.getFields())+" : "+error.getMessage()+" candidate info: "+parcels.get(i).getCandidate());
					}
				}
			}
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			logger.error("Failed to execute mass creation of contacts");
		}	
		
		logger.debug("end of inserting contact ...");
		
		return successfulCandidateParcels;
	}
	
	public static SObject buildContactSObject( Candidate candidate, CTCFile resume, String recordType){
		SfContactUpdateFields fieldsContact = new SfContactInsertFields(candidate, resume,recordType);
		if(candidate.getId() != null && !candidate.getId().equals("")){
			fieldsContact = new SfContactUpdateFields(candidate, resume);
		}
		
		return buildContactSObject( fieldsContact);
	}
	
	
	public static SObject buildContactSObject( SfContactUpdateFields fields){
		SObject sobj = new SObject();
		sobj.setType("Contact");
		
		Map<String, Object> fvpairs = fields.buildFieldValuePairs();
		Set<Map.Entry<String,Object>> entries = fvpairs.entrySet();
		for( Map.Entry<String,Object> entry : entries){
			sobj.setField(entry.getKey(), entry.getValue());
		}
		
		return sobj;
	}
	
	
	
	public static String upsert(PartnerConnection conn, Candidate candidate, CTCFile resume, String recordType){
		SfContactUpdateFields fieldsContact = new SfContactInsertFields(candidate, resume,recordType);
		if(candidate.getId() != null && !candidate.getId().equals("")){
			fieldsContact = new SfContactUpdateFields(candidate, resume);
		}
		
		return update(conn, fieldsContact);
	}
	
	public static String update(PartnerConnection conn, SfContactUpdateFields contactFields){
		ContactUpdater contactUpdt = new ContactUpdater(conn, contactFields);
		return contactUpdt.update();
	}
	
	
	
	private PartnerConnection conn;
	private SfContactUpdateFields fields; // fields that need to be updated
	
	static Logger logger = Logger.getLogger("com.ctc.people");

	/**
	 * constructor
	 * 
	 * @param conn
	 *            : connection to salesforce
	 * @param fields
	 *            : feilds that need to be updated
	 */
	public ContactUpdater(PartnerConnection conn, SfContactUpdateFields fields) {
		this.conn = conn;
		this.fields = fields;
	}

	
	@Override
	public String update() {
		
		return upsert( this.fields );
	}
	
	private String upsert(SfContactUpdateFields fields){
		String id = null;
		logger.info("Start to upsert candidate record. ");
		try {
			SObject sobj = buildContactSObject(fields);
			
			
			SaveResult[] srs = null;
			id = sobj.getField("Id")==null ? "" : (String) sobj.getField("Id");
			if(!id.equals("")) {
				logger.debug("Update candidate record. ");
	            srs = conn.update(new SObject[] { sobj });
	           
	        }
	        else {
	        	logger.debug("Insert candidate record. ");
	            srs = conn.create(new SObject[] { sobj });
	           
	        }
			
			id = srs[0].getId();
			
			if (srs!=null && !srs[0].isSuccess() && srs[0].getErrors() != null) {
				for (com.sforce.soap.partner.Error error : srs[0].getErrors()) {
					logger.warn(Arrays.toString(error.getFields()) + " : "
							+ error.getMessage());
					throw new SalesforceException(Arrays.toString(error
							.getFields()) + " : " + error.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ContactUpdateException("Failed to update candidate record with parsed resume content and skill group. " + e);
		}
		logger.info("Completed updating candidate record. ");
		
		return id;
	}
	
	
}
