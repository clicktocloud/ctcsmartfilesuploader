package com.ctc.people.cfm.adaptor.salesforce;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.exception.SalesforceException;
import com.ctc.people.cfm.exception.UserExistException;
import com.ctc.people.cfm.util.CFMHelper;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;


/**
 * This class is the implementation of ParsingService.java
 * This class aims to insert/update candidate and corresponding web document into Salesforce.
 * 
 * 
 * @author Kevin
 *
 */
public class CandidateCreator {
	static Logger logger = Logger.getLogger("com.ctc.people"); 
	
	
	
	
	public static String create( Candidate candidate, CTCFile resume ) throws SalesforceException, UserExistException{
		CandidateCreator  candidateCreator = new CandidateCreator( candidate,  resume );
		return candidateCreator.create();
	}
	
	
	private Candidate candidate;
	private CTCFile resume;
	public CandidateCreator(Candidate candidate, CTCFile resume){
		this.candidate = candidate;
		this.resume = resume;
	}
	/**
	 * This method aims to insert the new candidate to Salesforce
	 */
	public String create( ) throws SalesforceException, UserExistException {
		logger.debug("Start to insert candidate to Salesforce... ");		

		//Added by andy yang
		PartnerConnection conn = null;
		if( this.resume.getSforg() != null && this.resume.getSforg().getConn() != null){
			conn = this.resume.getSforg().getConn();
			
		}
		if( conn == null){
			conn = SfHelper.login( this.resume.getOwner().getOrgId());
		}
		
		if(resume.getOwner().getNamespace() == null){
			resume.getOwner().setNamespace( SalesforceAgent.getNamespace( this.resume.getOwner().getOrgId() ));
		}
		
		// to get the recordtype of the candidate
		String rid = SfHelper.getRecordTypeId(conn, Constant.DEFAULT_CONTACT_RECORD_TYPE);
		
		// to check whether the candidate has been in database
		String existCandidateId = resume.getOwner().getWhoId();
		if(StringUtils.isEmpty(existCandidateId )){
			String ids = SfHelper.existUser( candidate, conn);
			if(! StringUtils.isEmpty(ids)){
				existCandidateId = ids.split(":")[0];
				
				if(resume.getOwner().getUserId() != null){
					for(String id : ids.split(":")){
						if( resume.getOwner().getUserId().equals(id)){
							existCandidateId = id;
							break;
						}
					}
				}
				
			}
			
		}
       
        if(! StringUtils.isEmpty(existCandidateId)){
        	 candidate.setId( existCandidateId );
		}

        /*
         * STEP 1: insert or update candidate profile
         */
       
        String contactId = ContactUpdater.upsert(conn, candidate, resume, rid);
        candidate.setId( contactId );
        
        logger.info("Upsert candidate profile");
		logger.debug("Candidate Id:" + candidate.getId());
        
		
		// attach resume to candidate
		resume.getOwner().setWhoId( candidate.getId() );
		if(StringUtils.isEmpty(resume.getOwner().getWhoIdField()) || "null".equals(resume.getOwner().getWhoIdField())){
			resume.getOwner().setWhoIdField(resume.getOwner().getNamespace() + "Document_Related_To__c");
		}
		
		
		logger.info("Insert resume file "+resume.getOriginalFileName()+" to web document of candidate ["+resume.getOwner().getWhoId()+"].");
		
		logger.debug("\t" + resume.getOriginalFileName());
		logger.debug("\t" + resume.getPath());
		logger.debug("\t" + resume.getNewFileName());
		logger.debug("\twhoId" + resume.getOwner().getWhoId());
		
        // if skill parsing is activated
        if( resume.getOwner().isEnableSkillParsing() ){
        	
        	logger.debug("Parsing skills is enabled for skill groups : " + CFMHelper.generateSkillGroupsString(resume.getOwner().getSkillGroups()) );
        	
            // insert skills
        	SkillsCreator.create(conn, resume, candidate.getSkills());
        	
			// create employments
        	EmploymentCreator.create(conn, resume, candidate.getEmploymentHistory());
        	
        	// create educations
			EducationCreator.create(conn, resume, candidate.getEducationHistory());
			
        }
        
        // after the successful insertion of candidate, insert the WebDocument for the candidate as well
        /*
         * STEP 2:  insert the web document
         */
        try{
	        WebDocCreator.create(conn, Arrays.asList( new CTCFile[]{ resume } ));
	        ResourceDeleter.delete(conn, Arrays.asList(new CTCFile[]{resume}));
        }catch(Exception e){
        	logger.error(e);
        }

        if(!StringUtils.isEmpty(existCandidateId)) {
            throw new UserExistException(candidate.getId());
        }
		return contactId;
	}
	
	
		
}
