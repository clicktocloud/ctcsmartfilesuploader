package com.ctc.people.cfm.adaptor.salesforce;

import java.io.File;
import java.util.Observable;

import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.exception.SalesforceException;
import com.ctc.people.cfm.exception.UserExistException;
import com.ctc.people.cfm.monitor.CandidateImportMonitor;

/**
 * This class executes a series of actions to save candidate and its relevant informations to salesforce:
 * 
 * @author andy
 *
 */
public class InsertSfRunnable extends Observable implements Runnable{
	private static Logger logger = Logger.getLogger(InsertSfRunnable.class);
	
	private Candidate candidate;
	private CTCFile resume;
	
	private CandidateImportMonitor topOb ;

	
	public InsertSfRunnable(Candidate c, CTCFile resume,  CandidateImportMonitor topOb) {
		this.candidate = c;
		this.resume = resume;
		this.topOb =topOb;
		
		addObserver(topOb);
		
	}
	
	@Override
	public void run(){
		
		File file = new File(resume.getPath() ) ;
		
		try {
			logger.info("SF upsert starts ......");

			//upsert candidate to Salesforce
			CandidateCreator.create(candidate,  resume);
			
			// put the new candidate into the Map which will be used in the main thread
			topOb.addSuccessfulCandidate( candidate.getId(), candidate);
			
			logger.info("SF upsert ends ......");
								
			
					
		    
		} catch (SalesforceException e) {
			logger.error("Instance:"+ resume.getOwner().getOrgId() + "\t" + "file:" + resume.getPath() + "\t" + e);
			topOb.addErrorMsg(resume.getNewFileName(), CandidateImportMonitor.SF_ERROR, "Failed to insert candidate " + candidate.getFirstName() + " " + candidate.getLastName() + " to Salesforce.");
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap, resume.getNewFileName(), "SFError", "Failed to insert candidate " + candidate.getFirstName() + " " + candidate.getLastName() + " to Salesforce.");
		} catch (UserExistException e){
			logger.warn("Instance:"+ resume.getOwner().getOrgId() + "\t" + "file:" +  resume.getPath() + "\tDuplicate Contact Id:" +e.getMessage());
			topOb.addErrorMsg(resume.getNewFileName(), CandidateImportMonitor.USR_ERROR, e.getMessage());
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap, resume.getNewFileName(), "UsrError", e.getMessage());
		} catch (Exception e){
			logger.error("Instance:"+ resume.getOwner().getOrgId() + "\t" + "file:" +  resume.getPath() + " : " + e);
			topOb.addErrorMsg(resume.getNewFileName(), CandidateImportMonitor.OTHER_ERROR, e.getMessage());
			
		} finally {
			
			candidate.setFile(file);
			
			// notify the observer after the insertion.
			setChanged();
			notifyObservers(file);

        }
    }
}
