package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.exception.EmploymentCreateException;
import com.ctc.people.cfm.util.CFMHelper;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 * this class retrieves all employment records under a specific contact
 * @author kwu
 *
 */
public class EmploymentRetriever  implements Retriever{
	static Logger logger = Logger.getLogger("com.ctc.people");

	public static List<String> retrieve(PartnerConnection conn, List<String> contactIds, String namespace){
		
		EmploymentRetriever retriever = new EmploymentRetriever(conn, contactIds);
		retriever.setNsp(namespace);
		retriever.retrieve();
		
		
		return retriever.getEmploymentIds();
		
	}
	
	
	public static List<String> retrieve(PartnerConnection conn, String contactId, String namespace){
		
		EmploymentRetriever retriever = new EmploymentRetriever(conn, contactId);
		retriever.setNsp(namespace);
		retriever.retrieve();
		
		
		return retriever.getEmploymentIds();
		
	}

	private String nsp = ""; // namespace prefix
	private PartnerConnection conn;
	private List<String> contactIds;
	private List<String> employmentIds;
	
	/**
	 * constructor
	 * 
	 * @param conn: connection to salesforce
	 * @param contactId: contact id 
	 */
	public EmploymentRetriever(PartnerConnection conn, String contactId) {
		this.conn = conn;
		this.contactIds = Arrays.asList( new String[]{contactId});
		employmentIds = new ArrayList<String>();
	}
	
	
	public EmploymentRetriever(PartnerConnection conn, List<String> contactIds) {
		this.conn = conn;
		this.contactIds = contactIds;
		employmentIds = new ArrayList<String>();
	}

	public void setNsp(String nsp) {
		this.nsp = nsp;
	}

	public void retrieve() {
		logger.info("Start to retrieve existing employment history records. ");
		
		if(contactIds == null || contactIds.size() == 0){
			return ;
		}
		
		String idsString  = CFMHelper.arrayToString(contactIds, ",", "'");
		String query = "select Id from " + nsp + "Employment_History__c where "
				+ nsp + "Contact__c in ( " + idsString + ")";
		QueryResult results = null;
		try {
			results = conn.query(query);
			boolean done = false;
			if (results.getSize() > 0) {
				while (!done) {
					for (SObject sobj : results.getRecords()) {
						employmentIds.add(sobj.getField("Id")
								.toString());
					}
					if (results.isDone()) {
						done = true;
					} else {
						results = conn.queryMore(results
								.getQueryLocator());
					}
				}
			}
		} catch (Exception e) {
			logger.error("Failed to fetch existing employment history" + e.getMessage()
					+ ". candidate ids: " + idsString);
			throw new EmploymentCreateException("Failed to fetch existing employment history records. " + e);
		}
		logger.info("Completed retrieving existing employment history records. ");
	}

	/**
	 * get the list of all employment records
	 * @return
	 */
	public List<String> getEmploymentIds() {
		return employmentIds;
	}

	
}
