package com.ctc.people.cfm.adaptor.aws.s3;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jets3t.service.S3Service;
import org.jets3t.service.model.S3Object;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.S3Exception;

/**
 * this class download files from S3
 * 
 * @author andy
 *
 */
public class FileDownloader2S3  {
	
	public static void download(SforgEx sforg, List<CTCFile> files){
		if (files == null || files.size() == 0) {
			return;
		}
		
		S3Service s3Service = S3ConnectionFactory.getInstance()
				.getS3Connection(sforg.getS3accesskey(),
						sforg.getS3secretkey());
		
		FileDownloader2S3 downloader = new FileDownloader2S3(s3Service,
				sforg.getBucketName(), files);
		
		downloader.download();
	}
	
	private S3Service s3Service;
	private String bucketName;
	private List<CTCFile> files;
	static Logger logger = Logger.getLogger("com.ctc.people");


	/**
	 * constructor
	 * 
	 * @param s3Service
	 * @param bucketName
	 */
	public FileDownloader2S3(S3Service s3Service, String bucketName, List<CTCFile> files) {
		this.s3Service = s3Service;
		this.bucketName = bucketName;
		this.files = files;
	}


	public void download() {
		logger.info("Start to download files from S3.");
		
		if(files ==null || files.size()==0){
			throw new S3Exception("The list of files for downloading from Amazon S3 is empty.");
		}
		
		for (CTCFile file : files) {
			S3Object s3Object = null;
			try {
				//File localFile = new File(file.getPath());
				
				s3Object = s3Service.getObject(bucketName, file.getNewFileName());
				if(s3Object != null){
					
					file.setFileContent( IOUtils.toByteArray(s3Object.getDataInputStream()));
					
					//FileUtils.copyInputStreamToFile(s3Object.getDataInputStream(), localFile);
					
				}else{
					logger.error("File does not exist in S3 : " + bucketName+"/"+ file.getNewFileName());
				}
				logger.info("Downloaded file from "+bucketName+" : "+ file.getNewFileName());
			}catch (Exception e) {
				e.printStackTrace();
				logger.error("Failed to download files from "+bucketName+" S3. " + e);
				throw new S3Exception(
						"Failed to download files from "+bucketName+"S3. " );
			}finally{
				if( s3Object != null){
					try {
						s3Object.closeDataInputStream();
					} catch (IOException e) {
						
					}
				}
			}
			
		}
		logger.info("Completed downloading files from S3.");
	}

}
