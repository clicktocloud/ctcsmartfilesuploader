package com.ctc.people.cfm.adaptor.aws.s3;

import java.io.File;
import java.util.Arrays;
import java.util.Observable;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.adaptor.salesforce.SfHelper;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.monitor.CandidateImportMonitor;



public class UploadS3Runnable extends Observable implements Runnable {

	private static Logger logger = Logger.getLogger(UploadS3Runnable.class);
	
	private CTCFile file;
	private String newFileName; 
	private CandidateImportMonitor topObserver;
	
	public UploadS3Runnable( CTCFile file,CandidateImportMonitor topob){
		this.file = file;
		this.topObserver = topob;
		
		this.newFileName = file.getNewFileName();
		
		addObserver(topob);
	}
	
	@Override
	public void run(){
		try {
			logger.info("S3 upload starts ......");
			
			// to get the start time of resume parse.
			long startTimeOfUploadingResume = System.currentTimeMillis();
			// to get the instance object
			SforgEx sforg = file.getSforg();
			if(sforg ==null){
				sforg = SfHelper.getSfOrg( this.file.getOwner().getOrgId() );
			}
			
			// to upload resumes to S3
			FileUploader2S3.upload(sforg, Arrays.asList( new CTCFile[]{ this.file }));
			
			// to get the end time of resume parse and calculate the time consumed for uploading each resume
			long endTimeOfUploadingResume = System.currentTimeMillis();
			logger.info("S3 upload ends ......");
			logger.info("Run time for S3 upload: " + (endTimeOfUploadingResume-startTimeOfUploadingResume) + " ms");

		} catch (Exception e) {	
			logger.error("Instance:" + this.file.getOwner().getOrgId() + "\t" + "file:" + this.file.getPath() + ". " + e);
			topObserver.addErrorMsg(newFileName , CandidateImportMonitor.S3_ERROR, "Failed to upload " + newFileName + " to server - " + e);
			//errorMsgMap = CFMHelper.setErrorMsgMap(errorMsgMap,shortFileName , "S3Error", "Failed to upload " + shortFileName + " to server");
		} finally{
			// notify the observer 
			setChanged();
			notifyObservers(new File( this.file.getPath() ));
		   
		}
	}
}
