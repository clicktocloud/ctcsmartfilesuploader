package com.ctc.people.cfm.adaptor.salesforce;

import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.exception.EducationCreateException;
import com.sforce.soap.partner.DeleteResult;
import com.sforce.soap.partner.PartnerConnection;

/**
 * this class deletes education records by IDs
 * @author andy
 *
 */
public class EducationDeleter implements Deleter {
	private PartnerConnection conn;
	private List<String> ids;
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	public static void delete( PartnerConnection conn, List<String> ids ){
		new EducationDeleter(conn, ids).delete();
	}

	/**
	 * constructor
	 * 
	 * @param conn: connection to salesforce 
	 * @param ids: list of ids of education history
	 */
	public EducationDeleter(PartnerConnection conn, List<String> ids) {
		this.ids = ids;
		this.conn = conn;
	}

	@Override
	public void delete() {
		if(ids == null || ids.size() == 0)
			return;
		
		logger.info("Start to delete previously existing education history recrods [ "+ids.size()+" ]");
		
		DeleteResult[] results = null;
		try {
			results = SfHelper
					.massDelete(conn, ids.toArray(new String[ids.size()]));
		} catch (Exception ex) {
			logger.error("Error when deleting outdated education records. ");
			throw new EducationCreateException("Failed to delete previously existing education history records.");
		}

		for (int i = 0; i < results.length; i++) {
			if (!results[i].isSuccess()) {
				com.sforce.soap.partner.Error[] errors = results[i].getErrors();
				for (com.sforce.soap.partner.Error error : errors) {
					logger.error(error.getMessage());
				}
			}
		}
		logger.info("Completed deleting previously existing education history recrods. ");
	}

}
