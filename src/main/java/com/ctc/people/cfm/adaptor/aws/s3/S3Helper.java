package com.ctc.people.cfm.adaptor.aws.s3;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;

import com.ctc.common.util.file.CTCFileUtils;
import com.ctc.people.cfm.exception.S3Exception;

/**
 * this is a helper class for S3 related actions
 * @author kwu
 *
 */
public class S3Helper {

	/**
	 * get s3 bucket given bucket name and s3 connection
	 * 
	 * @param s3Service
	 * @param bucketName
	 * @return
	 */
	public static S3Bucket getBucketFromName(S3Service s3Service,
			String bucketName) {
		S3Bucket bucket = null;
		try {
			S3Bucket[] buckets = s3Service.listAllBuckets();
			for (S3Bucket b : buckets) {
				if (b.getName().equals(bucketName)) {
					bucket = b;
					break;
				}
			}
		} catch (S3ServiceException ex) {
			throw new S3Exception(
					"Failed to get S3 Bucket from S3. BucketName = "
							+ bucketName + ". " + ex);
		}
		return bucket;
	}
	
	/**
	 * Added my Kevin, 19.07.2012
	 * 
	 * This method aims to set content type for the file being uploaded.
	 * 
	 * @param fileObject
	 * @param f
	 */
	public static S3Object setContentType(S3Object fileObject, File f){
		String file = f.toString().toLowerCase();
		
		if(file.endsWith("doc")){
			
			fileObject.setContentType("application/msword");
			
		}else if(file.endsWith("docx")){
			
			fileObject.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			
		}else if(file.endsWith("pdf")){
			
			fileObject.setContentType("application/pdf");
			
		}else if(file.endsWith("txt")){
			
			fileObject.setContentType("application/msword");
			
		}else if(file.endsWith("rtf")){
			
			fileObject.setContentType("application/rtf");
			
		}else{
			// do nothing
		}	
		return fileObject;
	}
	
	/**
	 * To upload the resume to AWS S3
	 * 
	 * @param bucketname
	 * @param awsAccessKey
	 * @param awsSecretKey
	 * @param uploadedFile
	 * @throws S3ServiceException
	 */
	public static void uploadS3(String bucketname, String awsAccessKey, String awsSecretKey, File uploadedFile) throws S3ServiceException{
		
		String contentType = CTCFileUtils.getFileContentType(uploadedFile);
		
		AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey, awsSecretKey);
		S3Service s3Service = new RestS3Service(awsCredentials);
		S3Object fileObject = null;
		
		try {
			fileObject = new S3Object(uploadedFile);
			fileObject.setName(uploadedFile.getName());
			fileObject.setAcl(AccessControlList.REST_CANNED_AUTHENTICATED_READ);
			fileObject.setContentType(contentType);
			s3Service.putObject(bucketname, fileObject);
		} catch (NoSuchAlgorithmException e) {
			
			throw new S3ServiceException(e);
		} catch (IOException e) {
			
			throw new S3ServiceException(e);
		} catch (NullPointerException e){
			
			throw new S3ServiceException(e);
		}
	}
}
