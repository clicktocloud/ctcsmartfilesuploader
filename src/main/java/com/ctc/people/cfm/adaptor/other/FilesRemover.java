package com.ctc.people.cfm.adaptor.other;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.CTCFile;


/**
 * this class delete all downloaded file
 * @author kwu
 *
 */
public class FilesRemover implements Remover{
	
	public static void remove( List<CTCFile> files ){
		if( files == null || files.size() == 0 ){
			return ;
		}
		
		FilesRemover remover = new FilesRemover(files);
		remover.remove();
	}
	
	private List<CTCFile> files;
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	/**
	 * constructor
	 * 
	 * @param files
	 */
	public FilesRemover(List<CTCFile> files){
		this.files = files;
	}
	
	@Override
	public void remove() {
		logger.info("Start to remove temp files.");
		
		for(CTCFile file:files){
			File f = new File(file.getPath());
			if(f.exists()){
				f.delete();
				logger.debug(f + " got deleted.");
			}
		}
		logger.info("Completed removing temp files.");
	}

}
