package com.ctc.people.cfm.adaptor.salesforce;

public interface Deleter {
	void delete();
}
