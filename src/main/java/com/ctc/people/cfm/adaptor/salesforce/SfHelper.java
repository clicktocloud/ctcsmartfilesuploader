package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.entity.ActivityDoc;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.SalesforceException;
import com.ctc.people.cfm.exception.UserExistException;
import com.ctc.people.cfm.util.CFMHelper;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.ctc.salesforce.enhanced.domain.OrgConfiguration;
import com.ctc.salesforce.enhanced.service.OrgConfigurationReader;
import com.sforce.soap.partner.DeleteResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;


public class SfHelper {
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	private static Map<String,String> contactRecordTypes = new HashMap<String,String>();
	private static Map<String,String> ctcAdminAccountIds = new HashMap<String,String>();
	
	public static PartnerConnection login(String orgId){
		PartnerConnection conn = null;;
		try{
			long begin = System.currentTimeMillis();
			conn = SalesforceAgent.getConnectionByOAuth(orgId);
			
			logger.debug("Time cost of getting connection to salesforce instance via oauth : " + (System.currentTimeMillis() - begin));
			
		}catch (ConnectionException e) {
        		logger.error("Cannot setup the Salesforce OAuth connection for org: " + orgId + ". ", e);
			throw new SalesforceException("ConnectionException : orgId=" + orgId + ". "+ e );
        }
        if (conn == null) {
    			logger.error("Cannot setup the Salesforce OAuth connection for org: " + orgId + ". ");
    			throw new SalesforceException("ConnectionException : orgId=" + orgId + ". ");
        }
        return conn;
	}	
	
	/**
	 * Get request org detail from Corporate Instance
	 * 
	 * @param orgId
	 * @return SfOrg
	 * 
	 */
	public static SforgEx getSfOrg(String orgId) {
		SforgEx sforg = null;
		// get connection to corporate instance
		String corpOrgId = CFMHelper.getValueFromPropertyFile("corp_orgid", Constant.PROPERTIES_FILENAME);
		String namespace = CFMHelper.getValueFromPropertyFile("corp_namespace", Constant.PROPERTIES_FILENAME);
		long begin = System.currentTimeMillis();
		OrgConfigurationReader.getCorpReader(corpOrgId, namespace);
		logger.debug("Time cost of getting connection to corp instance via oauth : " + (System.currentTimeMillis() - begin));
		OrgConfiguration config = OrgConfigurationReader.getOrgConfig( orgId );
		if(config != null){
			sforg = new SforgEx();
			
			sforg.setOrgid( config.getOrgId());
			sforg.setS3accesskey( config.getS3AccessKey());
			sforg.setS3secretkey(config.getS3KeySecret());
			sforg.setBucketName( config.getS3Bucket());
			
			sforg.setSkillParsing(false);
		}else{
			logger.error("Failed to get salesforce org = " + orgId);
			throw new SalesforceException("Failed to get saleforce org="+ orgId);
		}
		
		
		return sforg;
	}
	
	/**
	 * mass create sf records
	 * 
	 * @param conn
	 * @param sObjects
	 * @return
	 * @throws ConnectionException
	 */
	public static SaveResult[] massCreate(PartnerConnection conn,
			SObject[] sObjects) throws ConnectionException {
		List<SObject> tmpSObjects = new ArrayList<SObject>();
		List<SaveResult[]> srsList = new ArrayList<SaveResult[]>();
		SaveResult[] srsFinal = new SaveResult[sObjects.length];
		int recordsSize = 0;

		// create new records
		for (SObject sobj : sObjects) {
			tmpSObjects.add(sobj);
			recordsSize++;

			if (tmpSObjects.size() == 200 || recordsSize == sObjects.length) {
				SaveResult[] srsTmp = conn.create(tmpSObjects
						.toArray(new SObject[] {}));
				srsList.add(srsTmp);
				tmpSObjects.clear();
			}
		}

		// generate the SaveResult[]
		int srsIndex = 0;
		for (SaveResult[] srsTmp : srsList) {
			for (int i = 0; i < srsTmp.length; i++) {
				srsFinal[srsIndex] = srsTmp[i];
				srsIndex++;
			}
		}
		return srsFinal;
	}

	/**
	 * mass update sf records
	 * 
	 * @param conn
	 * @param sObjects
	 * @return
	 * @throws ConnectionException
	 */
	public static SaveResult[] massUpdate(PartnerConnection conn,
			SObject[] sObjects) throws ConnectionException {
		List<SObject> tmpSObjects = new ArrayList<SObject>();
		List<SaveResult[]> srsList = new ArrayList<SaveResult[]>();
		SaveResult[] srsFinal = new SaveResult[sObjects.length];

		int recordsSize = 0;

		// update records
		for (SObject sobj : sObjects) {
			tmpSObjects.add(sobj);
			recordsSize++;

			if (tmpSObjects.size() == 200 || recordsSize == sObjects.length) {
				SaveResult[] srsTmp = conn.update(tmpSObjects
						.toArray(new SObject[] {}));
				srsList.add(srsTmp);
				tmpSObjects.clear();
			}
		}

		// generate the SaveResult[]
		int srsIndex = 0;
		for (SaveResult[] srsTmp : srsList) {
			for (int i = 0; i < srsTmp.length; i++) {
				srsFinal[srsIndex] = srsTmp[i];
				srsIndex++;
			}
		}
		return srsFinal;
	}

	/**
	 * mass delete sf records
	 * 
	 * @param conn
	 * @param ids
	 * @return
	 * @throws ConnectionException
	 */
	public static DeleteResult[] massDelete(PartnerConnection conn, String[] ids)
			throws ConnectionException {
		List<String> tmpIds = new ArrayList<String>();
		List<DeleteResult[]> srsList = new ArrayList<DeleteResult[]>();
		DeleteResult[] srsFinal = new DeleteResult[ids.length];

		int recordsSize = 0;

		// delete records
		for (String id : ids) {
			tmpIds.add(id);
			recordsSize++;

			if (tmpIds.size() == 200 || recordsSize == ids.length) {
				DeleteResult[] srsTmp = conn.delete(tmpIds
						.toArray(new String[] {}));
				srsList.add(srsTmp);
				tmpIds.clear();
			}
		}

		// generate the SaveResult[]
		int srsIndex = 0;
		for (DeleteResult[] srsTmp : srsList) {
			for (int i = 0; i < srsTmp.length; i++) {
				srsFinal[srsIndex] = srsTmp[i];
				srsIndex++;
			}
		}
		return srsFinal;
	}
	
	/**
	 * get all skill IDs from salesforce
	 * 
	 * @param skillsExtIds
	 * @param conn
	 * @param namespacePrefix
	 * @return
	 * @throws ConnectionException
	 */
	public static ArrayList<String> getSkillSFids(ArrayList<String> skillsExtIds, PartnerConnection conn, String namespacePrefix) throws ConnectionException{
		QueryResult queryresult = null;
		ArrayList<String> skillSFids = new ArrayList<String>();
		String SkillexIdsliststring  = Arrays.toString(skillsExtIds.toArray()).replaceAll("\\[|\\]", "").replaceAll(", ","\',\'");
		String querystring = "select Id from " + namespacePrefix +"Skill__c where "+ namespacePrefix	+ "Ext_Id__c IN (\'" + SkillexIdsliststring + "\')";
		try{
			queryresult = conn.query(querystring);
			boolean done = false;
			if(queryresult.getSize()>0){
				while(!done){
					for(SObject sobj: queryresult.getRecords()){
						skillSFids.add(sobj.getField("Id").toString());
					}
					if(queryresult.isDone()){
						done = true;
					}
					else{
						queryresult = conn.queryMore(queryresult.getQueryLocator());
					}
				}
			}
		}
		catch(ConnectionException e){
			
		}
		return skillSFids;
	}
	
	
	
	/**
	 * generate a string with all ids from a set of ids
	 * 
	 * @param idList
	 * @return
	 */
	public static String generateIdString(ArrayList<ActivityDoc> allDocs) {
		String idsStr = "";

		for (ActivityDoc doc : allDocs) {
			if (idsStr.equals("")) {
				idsStr = "'" + doc.getAttachmentId() + "'";
			} else {
				idsStr += ",'" + doc.getAttachmentId() + "'";
			}
		}
		return idsStr;
	}
	
	
    
    /**
     * to check whether the candidate has been in the database
     *
     * @param candidate
     * @param conn
     * @throws SalesforceException
     * @throws UserExistException
     */
    public static String existUser(Candidate candidate, PartnerConnection conn) throws SalesforceException,UserExistException{
        String firstName = candidate.getFirstName();
        String lastName = candidate.getLastName();
        String email = candidate.getEmail();
        String existCandidateId = "";

        // replace single quote for firstname and lastname
        if(firstName.contains("'")){
            firstName = firstName.replace("'","\\'");
        }
        if(lastName.contains("'")){
            lastName = lastName.replace("'","\\'");
        }


        if(StringUtils.isBlank(email)){
            return existCandidateId;
        }
        QueryResult queryResult = null;
        String queryString = "select Id from Contact where Email='"+email+"' and FirstName='" + firstName + "' and LastName='" + lastName + "'";
        //logger.debug("queryString"+queryString);
        try {
            queryResult = conn.query(queryString);
        } catch (ConnectionException e) {
            logger.error(e);
            throw new SalesforceException(e);
        }
        if(queryResult.getSize()>0){
        	 existCandidateId = getResult(queryResult);
            logger.warn("User is existing in Salesforce." + "\t" + "duplicate IDs:" + existCandidateId);
            //throw new UserExistException(getResult(queryResult)+"");
           
        }
        return existCandidateId;
    }
	
    /**
	 * To get all duplicate candidates
	 * 
	 * @param qr
	 * @return
	 */
	public static String getResult(QueryResult qr){
		String contactIdSeperatedByColon = "";
		for(int i = 0; i < qr.getRecords().length;i++){
			if(i==qr.getRecords().length-1){
				contactIdSeperatedByColon += qr.getRecords()[i].getId();
			}else{
				contactIdSeperatedByColon += qr.getRecords()[i].getId() + ":";
			}	
		}
		logger.debug("queryResult String:" + contactIdSeperatedByColon);
		return contactIdSeperatedByColon;
	}
	
	
	public static String getCTCAdminAccountId(PartnerConnection conn)  {
		
		String accountName = Constant.ACCOUNT_ADMIN_NAME;
		
		synchronized (ctcAdminAccountIds) {
			String id = null;
			try {
				String orgId = conn.getUserInfo().getOrganizationId();
				id = ctcAdminAccountIds.get(orgId +":" +accountName);
				if (id == null) {
				
					QueryResult queryResult = conn
							.query("Select Id From Account where Name='"
									+ accountName + "' limit 1");
					if (queryResult.getSize() > 0) {
						id = queryResult.getRecords()[0].getId();
						if (id != null)
							ctcAdminAccountIds.put(orgId +":" +accountName, id);
					} 
				
				}
			} catch (Exception e) {
				logger.error("username=" + conn.getConfig().getUsername()
						+ "\tAccount " + Constant.ACCOUNT_ADMIN_NAME + " does not exist");
				
			}
			
			return id;
		}
		
		
	}
	
	/**
	 * This method aims to get the id of record type needed
	 * 
	 * @param conn
	 * @param recordTypeName
	 * @return
	 * @throws SalesforceException
	 */
	public static String getRecordTypeId(PartnerConnection conn, String recordTypeName) throws SalesforceException {
		
		synchronized (contactRecordTypes) {
			String id = null;
			try {
				String orgId = conn.getUserInfo().getOrganizationId();
				id = contactRecordTypes.get(orgId +":" +recordTypeName);
				if (id == null) {
				
					QueryResult queryResult = conn
							.query("Select Id, DeveloperName From RecordType where DeveloperName='"
									+ recordTypeName + "' limit 1");
					if (queryResult.getSize() > 0) {
						id = queryResult.getRecords()[0].getId();
						if (id != null)
							contactRecordTypes.put(orgId +":" +recordTypeName, id);
					} else {
						logger.error("username="
								+ conn.getConfig().getUsername() + "\t"
								+ "this record type name does not exist");
						throw new SalesforceException(
								"this record type name does not exist");
					}
				
				}
			} catch (Exception e) {
				logger.error("username=" + conn.getConfig().getUsername()
						+ "\t" + "this record type name does not exist");
				throw new SalesforceException(e);
			}
			
			return id;
		}
		
		
	}

	 /**
     * To retrieve all old employment records
     *
     * @param contactId
     * @param conn
     * @return
     * @throws ConnectionException
     */
    public static ArrayList<String> getOldEmploymentHistoryList(String contactId, PartnerConnection conn, String namespacePrefix) throws ConnectionException{
        QueryResult queryresult = null;
        ArrayList<String> employmentHistoyRecordList = new ArrayList<String>();
        String queryString = "select Id from " + namespacePrefix +	"Employment_History__c where " + namespacePrefix +"Contact__c = \'" + contactId + "\'";
        try{
            queryresult = conn.query(queryString);
            boolean done = false;
            if(queryresult.getSize()>0){
                while(!done){
                    for(SObject sobj: queryresult.getRecords()){
                        employmentHistoyRecordList.add(sobj.getField("Id").toString());
                    }
                    if(queryresult.isDone()){
                        done = true;
                    }
                    else{
                        queryresult = conn.queryMore(queryresult.getQueryLocator());
                    }
                }
            }
        }
        catch(ConnectionException e){
            logger.error("Fetch existing Employment History"+e.getMessage()+". candidate id: "+ contactId);
        }
        return employmentHistoyRecordList;
    }
    
    /**
     * This method aims to delete old employment history records
     *
     * @param conn
     * @param contactId
     */
    public static void deleteOutdatedEmploymentHistory(PartnerConnection conn, String contactId, ArrayList<String> existingEmplymentHistory){
        //Delete old Employment History records
        logger.info("start of deleting employment history records...");
        DeleteResult[] drs = null;
        try{
            drs = massDelete(conn, existingEmplymentHistory.toArray(new String[existingEmplymentHistory.size()]));
        }catch(Exception ex){
            logger.error("Error when deleting outdated employment records. ");
        }

        for(int i=0; i<drs.length; i++){
            if(!drs[i].isSuccess()){
                com.sforce.soap.partner.Error[] errors = drs[i].getErrors();
                for(com.sforce.soap.partner.Error error : errors){
                    logger.error("Error when deleting employment "+ Arrays.toString(error.getFields())+" : "+error.getMessage()+". candidate id: "+ contactId);
                }
            }
        }
        logger.info("end of deleting employment history records...");
    }
    
    /**
     * To retrieve all old education records
     *
     * @param contactId
     * @param conn
     * @return
     * @throws ConnectionException
     */
    public static ArrayList<String> getOldEducationHistoryList(String contactId, PartnerConnection conn , String namespacePrefix) throws ConnectionException{
        QueryResult queryresult = null;
        ArrayList<String> educationHistoyRecordList = new ArrayList<String>();
        String queryString = "select Id from " + namespacePrefix +	"Education_History__c where " + namespacePrefix +"Contact__c = \'" + contactId + "\'";
        try{
            queryresult = conn.query(queryString);
            boolean done = false;
            if(queryresult.getSize()>0){
                while(!done){
                    for(SObject sobj: queryresult.getRecords()){
                        educationHistoyRecordList.add(sobj.getField("Id").toString());
                    }
                    if(queryresult.isDone()){
                        done = true;
                    }
                    else{
                        queryresult = conn.queryMore(queryresult.getQueryLocator());
                    }
                }
            }
        }
        catch(ConnectionException e){
            logger.error("Fetch existing Education History"+e.getMessage()+". candidate id: "+ contactId);
        }
        return educationHistoyRecordList;
    }
    
    /**
     * This method aims to delete old education history records
     *
     * @param conn
     * @param contactId
     */
    public static void deleteOutdatedEducationHistory(PartnerConnection conn, String contactId, ArrayList<String> existingEducationHistory){
        logger.info("start of deleting education history records...");
        DeleteResult[] drs = null;
        try{
            drs = massDelete(conn, existingEducationHistory.toArray(new String[existingEducationHistory.size()]));
        }catch(Exception ex){
            logger.error("Error when deleting outdated education records. ");
        }

        for(int i=0; i<drs.length; i++){
            if(!drs[i].isSuccess()){
                com.sforce.soap.partner.Error[] errors = drs[i].getErrors();
                for(com.sforce.soap.partner.Error error : errors){
                    logger.error("Error when deleting education "+Arrays.toString(error.getFields())+" : "+error.getMessage()+". candidate id: "+ contactId);
                }
            }
        }
        logger.info("end of deleting education history records...");
    }
    
    public static ArrayList<String> getSkillSfIds(ArrayList<String> skillExtIdList, PartnerConnection conn, String namespacePrefix) throws ConnectionException{
		logger.info("start to get skills salesforce ids ....");
        QueryResult queryResult = null;
		ArrayList<String> skillSfIdList = new ArrayList<String>();
		
		// to assemble the condition for the soql query
		String queryCondition = "";
		String query = "";
		logger.debug("GetSkillSFIds + skillExtIdList: " + skillExtIdList);
		for(int i = 0; i < skillExtIdList.size();i++){
			if(i==0 && skillExtIdList.size() ==1){
				queryCondition = queryCondition + "'"+skillExtIdList.get(i)+"'";
				
			}else if(i==(skillExtIdList.size()-1)){
				queryCondition = queryCondition + "'"+skillExtIdList.get(i)+"'";
				
			}else{
				queryCondition = queryCondition + "'"+skillExtIdList.get(i)+"', "; 
			}
		}
		
		if(queryCondition.equals("")){
			logger.debug("no skills matched in salesforce.");
			return null;
		}
		
		
		query = "select Id from " + namespacePrefix +"Skill__c where " + namespacePrefix+"Ext_Id__c IN ("+ queryCondition + ")";
		queryResult = conn.query(query);
		boolean done = false;
		if (queryResult.getSize() > 0) {
			while(!done){
				for(SObject sobj: queryResult.getRecords()){
					skillSfIdList.add(sobj.getField("Id").toString());
				}
				if(queryResult.isDone()){
					done = true;
				}else{
					queryResult = conn.queryMore(queryResult.getQueryLocator());
				}
			}
		}
        logger.info("end getting skills salesforce ids ....");
		return skillSfIdList;
	}
    
}
