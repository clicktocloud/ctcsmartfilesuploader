package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.sforce.soap.partner.DeleteResult;
import com.sforce.soap.partner.PartnerConnection;

/**
 * this class provides a method to delete resource records from salesforce by IDs
 * 
 * @author andy
 *
 */
public class ResourceDeleter implements Deleter {
	private PartnerConnection conn;
	private List<CTCFile> resourceFiles;
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	/**
	 * delete the resources referenced by the all of CTCFile objects from salesforce
	 * @param conn
	 * @param resourceFiles
	 */
	public static void delete( PartnerConnection conn, List<CTCFile> resourceFiles ){
		new ResourceDeleter(conn, resourceFiles).delete();
	}

	/**
	 * Constructor
	 * @param conn - PartnerConnection
	 * @param resourceFiles -  List<CTCFile>
	 */
	public ResourceDeleter(PartnerConnection conn, List<CTCFile> resourceFiles) {
		this.resourceFiles = resourceFiles;
		this.conn = conn;
	}

	@Override
	public void delete() {
		
		if(resourceFiles == null || resourceFiles.size() == 0)
			return;
		
		
		
		//extract all of ids of resources
		List<String> resourceIds = new ArrayList<String>();
		for(CTCFile rf : resourceFiles){
			String rid = rf.getResourceId();
			if( rid != null && ! rid.equals("")){
				resourceIds.add(rid);
				
			}
		}
		
		logger.info("Start to delete resources [ "+resourceIds.size()+" ]");
		
		if(resourceIds.size() == 0){
			return;
		}
		
		//delete all resources by ids
		DeleteResult[] results = null;
		try {
			results = SfHelper
					.massDelete(conn, resourceIds.toArray(new String[resourceIds.size()]));
		} catch (Exception ex) {
			logger.error("Error when deleting resources . ");
			throw new ResourceDownloadException("Error when deleting resources .");
		}

		//log error informations
		int success = resourceFiles.size();
		for (int i = 0; i < results.length; i++) {
			if (!results[i].isSuccess()) {
				success --;
				com.sforce.soap.partner.Error[] errors = results[i].getErrors();
				for (com.sforce.soap.partner.Error error : errors) {
					logger.error("[ " + results[i].getId() +" ] - " + error.getMessage());
				}
			}
		}
		logger.info("Completed deleting resource ["+success+"] ");
	}

}
