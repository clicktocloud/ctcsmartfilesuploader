package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.people.cfm.exception.EducationCreateException;
import com.ctc.people.cfm.util.CFMHelper;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 * this class retrieves all education records under a specific contact
 * @author lina
 *
 */
public class EducationRetriever implements Retriever{
	
	static Logger logger = Logger.getLogger("com.ctc.people");

	
	public static List<String> retrieve(PartnerConnection conn, List<String> contactIds, String namespace){
		
		EducationRetriever retriever = new EducationRetriever(conn, contactIds);
		retriever.setNsp(namespace);
		retriever.retrieve();
		
		
		return retriever.getEducationIds();
		
	}
	
	public static List<String> retrieve(PartnerConnection conn, String contactId, String namespace){
		
		EducationRetriever retriever = new EducationRetriever(conn, contactId);
		retriever.setNsp(namespace);
		retriever.retrieve();
		
		
		return retriever.getEducationIds();
		
	}
	
	private String nsp = ""; // namespace prefix
	private PartnerConnection conn;
	private List<String> contactIds;
	private List<String> educationIds;
	
	/**
	 * constructor
	 * 
	 * @param conn: connection to salesforce
	 * @param contactId: contact id 
	 */
	public EducationRetriever(PartnerConnection conn, String contactId) {
		this.conn = conn;
		this.contactIds = Arrays.asList( new String[]{contactId});
		educationIds = new ArrayList<String>();
	}
	
	public EducationRetriever(PartnerConnection conn, List<String> contactIds) {
		this.conn = conn;
		this.contactIds = contactIds;
		educationIds = new ArrayList<String>();
	}

	public void setNsp(String nsp) {
		this.nsp = nsp;
	}


	public void retrieve() {
		logger.info("Start to retrieve existing education history records. ");
		
		if(contactIds == null || contactIds.size() == 0){
			return ;
		}
		
		String idsString  = CFMHelper.arrayToString(contactIds, ",", "'");
		String query = "select Id from " + nsp + "Education_History__c where "
				+ nsp + "Contact__c in ( " + idsString + ")";
		
		QueryResult results = null;
		try {
			results = conn.query(query);
			boolean done = false;
			if (results.getSize() > 0) {
				while (!done) {
					for (SObject sobj : results.getRecords()) {
						educationIds.add(sobj.getField("Id")
								.toString());
					}
					if (results.isDone()) {
						done = true;
					} else {
						results = conn.queryMore(results
								.getQueryLocator());
					}
				}
			}
		} catch (Exception e) {
			logger.error("Failed to fetch existing education history" + e.getMessage()
					+ ". candidate ids: " + idsString);
			throw new EducationCreateException("Failed to fetch existing education history records. " + e);
		}
		
		logger.info("Completed retrieving existing education history records. ");
	}

	/**
	 * get the list of all education records
	 * @return
	 */
	public List<String> getEducationIds() {
		return educationIds;
	}

	
}
