package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.entity.Education;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.OwnerParcel;
import com.ctc.people.cfm.exception.EducationCreateException;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 * this class create educations records
 * 
 * @author andy
 * 
 */
public class EducationCreator implements Creator {
	
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	
	public static void create (PartnerConnection conn, List<OwnerParcel> parcels)  {
		if (parcels != null && parcels.size() > 0 && parcels.get( 0 ).getOwner()!=null) {
			
			List<String> existingEducationIDs  = null;
			
			String namespace = parcels.get( 0 ).getOwner().getNamespace();
			List<String> candidateIds = OwnerParcel.getCandidateIds(parcels);
			if(candidateIds.size() > 0 ){
				// get existing educations
				existingEducationIDs =  EducationRetriever.retrieve( conn,candidateIds, namespace);
			}
			
			
			// create education records
			EducationCreator educationsCrt = new EducationCreator(conn, parcels);
			educationsCrt.create();

			// delete previously existing educations
			if(educationsCrt.isSuccess() && existingEducationIDs != null && existingEducationIDs.size() > 0){
				EducationDeleter.delete( conn, existingEducationIDs);
			}	
		}	
	}
		
		
	public static void create(PartnerConnection conn,CTCFile resume, List<Education> educations){
		if (educations != null && educations.size() > 0) {
			
			List<OwnerParcel> ownerParcels = new ArrayList<OwnerParcel>();
			
			OwnerParcel parcel = new OwnerParcel();
			
			Candidate c = new Candidate();
			c.setId( resume.getOwner().getWhoId() );
			c.setEducationHistory( (ArrayList<Education>) educations );
			parcel.setCandidate(c);
			
			CTCFilesOwner owner = new CTCFilesOwner();
			owner.setNamespace(resume.getOwner().getNamespace());
			parcel.setOwner(owner);
			
			ownerParcels.add( parcel);
			
			create(conn, ownerParcels);
		}			
	}
	
	private PartnerConnection conn;
	private List<OwnerParcel> ownerParcels;
	
	private boolean success;
	

	/**
	 * constructor
	 * 
	 * @param conn: connection to salesforce
	 * @param educations: 
	 */
	public EducationCreator(PartnerConnection conn,
			List<Education> educations, String contactId, String namespace) {
		this.conn = conn;
		
		ownerParcels = new ArrayList<OwnerParcel>();
		
		OwnerParcel parcel = new OwnerParcel();
		
		Candidate c = new Candidate();
		c.setId( contactId );
		c.setEducationHistory( (ArrayList<Education>) educations );
		parcel.setCandidate(c);
		
		CTCFilesOwner owner = new CTCFilesOwner();
		owner.setNamespace(namespace);
		parcel.setOwner(owner);
		
		ownerParcels.add( parcel);
		
	}
	public EducationCreator(PartnerConnection conn, List<OwnerParcel> parcels){
		this.conn = conn;
		this.ownerParcels = parcels;
	}
	

	@Override
	public void create() {
		
		success = false;
		
		logger.info("Start to create new education history records. ");
		List<Education> successEducations = new ArrayList<Education>();
		
		if( ownerParcels == null || ownerParcels.size() ==0 ){
			return ;
		}
		
		try {
			
			List<SObject> sobjs = new ArrayList<SObject>();
			List<String> candidateIds = new ArrayList<String>();
			List<Education> totalEducations = new ArrayList<Education>();
			String namespace = ownerParcels.get(0).getOwner().getNamespace();
			
			//build the list of SObjects to upsert
			for(OwnerParcel parcel : ownerParcels){
				String candidateId  = parcel.getCandidate().getId();
				candidateIds.add(candidateId);
				List<Education> es = parcel.getCandidate().getEducationHistory();
				
				if(es != null){
					totalEducations.addAll(es);
					for (Education ed : es) {
						SObject sobj = new SObject();
						sobj.setType(namespace	 + "Education_History__c");
						sobj.setField(namespace +	"Contact__c", 			parcel.getCandidate().getId());
						sobj.setField(namespace +	"School_Name__c", 		ed.getSchoolName());
						sobj.setField(namespace +	"School_Type__c",		ed.getSchoolType());
						sobj.setField(namespace +	"Degree_Name__c", 		ed.getDegreeName());
						sobj.setField(namespace +	"Start_Date__c", 		ed.getStartDate());
						sobj.setField(namespace +	"End_Date__c", 			ed.getEndDate());
						sobjs.add(sobj);
					}
				}	
			}
			
			//insert new list of educations
			if(totalEducations.size()  > 0){
				
				SaveResult[] srs =  SfHelper.massCreate(conn,sobjs.toArray(new SObject[]{}));
				
				for(int i=0; i<srs.length; i++){
					Education ed = totalEducations.get(i);
					if(srs[i].isSuccess()){
						
						//logger.debug("successful education " + (i+1) +":" +  srs[i].getId() );
						
						successEducations.add( ed );    
					}
					if(!srs[i].isSuccess()){
						com.sforce.soap.partner.Error[] errors = srs[i].getErrors();
						for(com.sforce.soap.partner.Error error : errors){
							logger.error("create education "+Arrays.toString(error.getFields())+" : "+error.getMessage());
						}
					}
				}
			}
			
			success = true;

		} catch (Exception e) {
			
			logger.error("e");
			throw new EducationCreateException(
					"Failed to create education history records. " + e);
		}	
		
		logger.debug("Completed creating educations [ "+successEducations.size()+" ]");
		
	}
	
	
	public boolean isSuccess() {
		return success;
	}


}