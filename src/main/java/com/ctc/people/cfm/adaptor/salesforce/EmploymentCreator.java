package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.entity.Education;
import com.ctc.entity.Employment;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.OwnerParcel;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

/**
 * this class create employments records
 * 
 * @author andy
 * 
 */
public class EmploymentCreator implements Creator {
	
	static Logger logger = Logger.getLogger("com.ctc.people");

	
	public static void create (PartnerConnection conn, List<OwnerParcel> parcels)  {
		if (parcels != null && parcels.size() > 0 && parcels.get( 0 ).getOwner()!=null) {
			
			List<String> existingEmploymentIDs  = null;
			
			String namespace = parcels.get( 0 ).getOwner().getNamespace();
			List<String> candidateIds = OwnerParcel.getCandidateIds(parcels);
			if(candidateIds.size() > 0 ){
				// get existing educations
				existingEmploymentIDs =  EmploymentRetriever.retrieve( conn,candidateIds, namespace);
			}
			
			
			// create education records
			EmploymentCreator creator = new EmploymentCreator(conn, parcels);
			creator.create();

			// delete previously existing educations
			if(creator.isSuccess() && existingEmploymentIDs != null && existingEmploymentIDs.size() > 0){
				EmploymentDeleter.delete( conn, existingEmploymentIDs);
			}	
		}	
	}
	
	public static void create(PartnerConnection conn, CTCFile resume,List<Employment> employments){
		
		List<OwnerParcel> ownerParcels = new ArrayList<OwnerParcel>();
		
		OwnerParcel parcel = new OwnerParcel();
		
		Candidate c = new Candidate();
		c.setId( resume.getOwner().getWhoId() );
		c.setEmploymentHistory( (ArrayList<Employment>) employments );
		parcel.setCandidate(c);
		
		CTCFilesOwner owner = new CTCFilesOwner();
		owner.setNamespace(resume.getOwner().getNamespace());
		parcel.setOwner(owner);
		
		ownerParcels.add( parcel);
		
		create(conn, ownerParcels);
	}
	
	
	private PartnerConnection conn;
	private List<OwnerParcel> ownerParcels;
	
	private boolean success;
	
	/**
	 * constructor
	 * 
	 * @param conn: connection to salesforce
	 * @param educations: 
	 */
	public EmploymentCreator(PartnerConnection conn,
			List<Education> educations, String contactId, String namespace) {
		this.conn = conn;
		
		ownerParcels = new ArrayList<OwnerParcel>();
		
		OwnerParcel parcel = new OwnerParcel();
		
		Candidate c = new Candidate();
		c.setId( contactId );
		c.setEducationHistory( (ArrayList<Education>) educations );
		parcel.setCandidate(c);
		
		CTCFilesOwner owner = new CTCFilesOwner();
		owner.setNamespace(namespace);
		parcel.setOwner(owner);
		
		ownerParcels.add( parcel);
		
	}
	public EmploymentCreator(PartnerConnection conn, List<OwnerParcel> parcels){
		this.conn = conn;
		this.ownerParcels = parcels;
	}


	@Override
	public void create() {
		

		success = false;
		
		logger.info("Start to create new employment history records. ");
		
		List<Employment> successEmployments = new ArrayList<Employment>();
		
		if( ownerParcels == null || ownerParcels.size() ==0){
			return ;
		}
		
		try {
			
			List<SObject> sobjs = new ArrayList<SObject>();
			List<String> candidateIds = new ArrayList<String>();
			List<Employment> totalEmployments = new ArrayList<Employment>();
			String namespace = ownerParcels.get(0).getOwner().getNamespace();
			
			//build the list of SObjects to upsert
			for(OwnerParcel parcel : ownerParcels){
				String candidateId  = parcel.getCandidate().getId();
				candidateIds.add(candidateId);
				List<Employment> es = parcel.getCandidate().getEmploymentHistory();
				
				if(es != null){
					totalEmployments.addAll(es);
					for (Employment em : es) {
						SObject sobj = new SObject();
						sobj.setType(namespace + "Employment_History__c");
						sobj.setField(namespace + "Contact__c", candidateId);
						sobj.setField(namespace + "Description__c", em.getDescription());
						sobj.setField(namespace + "End_Date__c", em.getEndDate());
						sobj.setField(namespace + "Location__c", em.getLocation());
						sobj.setField(namespace + "Organisation__c", em.getOrgName());
						sobj.setField(namespace + "Position_Type__c", em.getPositionType());
						sobj.setField(namespace + "Start_Date__c", em.getStartDate());
						sobj.setField(namespace + "Title__c", em.getTitles().toString()
								.subSequence(1, em.getTitles().toString().length() - 1));
						sobjs.add(sobj);
					}
				}	
			}
			
			
			//insert new list of employments
			if(totalEmployments.size() > 0){
				SaveResult[] srs =  SfHelper.massCreate(conn,sobjs.toArray(new SObject[]{}));
				
				for(int i=0; i<srs.length; i++){
					Employment em = totalEmployments.get(i);
					if(srs[i].isSuccess()){
						
						//logger.debug("successful employment " + (i+1) +":" +  srs[i].getId() );
						
						successEmployments.add( em );    
					}
					if(!srs[i].isSuccess()){
						com.sforce.soap.partner.Error[] errors = srs[i].getErrors();
						for(com.sforce.soap.partner.Error error : errors){
							logger.error("create employment "+Arrays.toString(error.getFields())+" : "+error.getMessage());
						}
					}
				}
			}
			
			success = true;
			
		} catch (ConnectionException e) {
			
			e.printStackTrace();
			logger.error("Failed to execute mass creation of employments");
		}	
		logger.info("Completed creating employments  [ "+successEmployments.size()+" ]");
	}

	public boolean isSuccess() {
		return success;
	}

}
