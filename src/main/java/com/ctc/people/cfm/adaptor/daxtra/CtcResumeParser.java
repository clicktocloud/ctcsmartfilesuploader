package com.ctc.people.cfm.adaptor.daxtra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.clicktocloud.resumeparser.main.DaxtraResumeParser;
import com.ctc.common.util.file.DocReader;
import com.ctc.entity.Candidate;
import com.ctc.entity.Education;
import com.ctc.entity.Employment;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.exception.ResumeParsingException;
import com.ctc.people.cfm.util.CFMHelper;


/**
 * this class parses the resume given
 * 
 * @author kwu
 *
 */
public class CtcResumeParser implements Parser {
	private Candidate candidate;
	private Properties properties;
	private List<Employment> employments;
	private List<Education> educations;
	private Map<String, ArrayList<String>> skills;
	private String resumeContent;
	private CTCFile resume;
	static Logger logger = Logger.getLogger("com.ctc.people");

	/**
	 * constructor
	 * 
	 * @param properties
	 * @param resume
	 */
	public CtcResumeParser(Properties properties, CTCFile resume) {
		this.properties = properties;
		this.resume = resume;
	}

	@Override
	public void parse() {
		logger.info("Start to parse resume. ");
		
		
		logger.debug("properties=" + properties);
		logger.debug("resume=" + resume);

		try {
			// the selection of parsing engine is decided in the property file
			String parsingEngine = CFMHelper.getValueFromPropertyFile(
					"parsingengine", Constant.PROPERTIES_FILENAME);
			if ("daxtra".equalsIgnoreCase(parsingEngine)) {
				
				DaxtraResumeParser parser = new DaxtraResumeParser(new File(
						resume.getPath()), properties);
				
				candidate = parser.getCandidate();
				employments = parser.getEmploymentHistory();
				educations = parser.getEducationHistory();
				skills = parser.getSkills();
				resumeContent = parser.getResumeContent();
			} else {
				DocReader reader = new DocReader(new File(resume.getPath()));
				String contentTemp = reader.chooseReader();
				resumeContent = contentTemp.replaceAll(
						"[\\x00-\\x1F\\x7F-\\xFF&&[^\\t\\n\\r]]", " ");
			}

			// trim the resume content if it is oversized
			if (resumeContent.length() > 131072) {
				resumeContent = resumeContent.substring(0, 131060);
			}
		} catch (Throwable e) {
			
			throw new ResumeParsingException("Failed to parse resume. " + e);
		}
		logger.info("Completed parsing resume. ");
	}

	public List<Employment> getEmployments() {
		return employments;
	}
	
	public List<Education> getEducations() {
		return educations;
	}	

	public Map<String, ArrayList<String>> getSkills() {
		return skills;
	}

	public String getResumeContent() {
		return resumeContent;
	}

	public Candidate getCandidate() {
		return candidate;
	}

}
