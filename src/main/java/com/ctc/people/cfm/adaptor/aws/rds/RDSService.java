package com.ctc.people.cfm.adaptor.aws.rds;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ctc.people.cfm.entity.DaxtraAccount;


/**
 * Query Class for RDS connection
 * @author Lina
 *
 */


public class RDSService {
	private SessionFactory factory = null;
	private static Logger logger = Logger.getLogger("com.ctc.people");
	
	/**
	 * Setup a RDSService with an open session factory.
	 * 
	 * @param factory
	 */
	public RDSService(SessionFactory factory) {
		this.factory = factory;
	}
	
	
	/**
	 * Get the factory
	 * @return SessionFactory
	 */
	public SessionFactory getFactory() {
		return factory;
	}
	
	
	/**
	 * Get Daxtra username and password by salesforce org Id 
	 * @param orgId
	 * @return DaxtraAccount daxtra account detail selected from the database based on the orgId, if no account found return null
	 */
	public DaxtraAccount getDaxtraAccountByOrgId(String orgId) {
		DaxtraAccount daxtraAcc = null;

		// open a session to the database
		Session session = this.getFactory().openSession();
		Transaction transaction = null;

		try {
			// start a transaction
			transaction = session.beginTransaction();
			// attempt to get daxtra account from the databse
			daxtraAcc = (DaxtraAccount) session
					    .createQuery("from DaxtraAccount d where d.orgId = :orgId")
					    .setString("orgId", orgId)
					    .uniqueResult();
			// write the transaction
			transaction.commit();
		} catch(HibernateException e) {
			// if our transaction still lives, close it
			if(transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
		
			logger.error("Error for query Daxtra account: " + e);
		} finally {
			// close the session
			if(session.isOpen()) {
				session.close();
			}
		}
		return daxtraAcc;
	}
	
	@SuppressWarnings("unchecked")
	public  List<DaxtraAccount> getAllDaxtraAccounts(  ) {
		
		List<DaxtraAccount> accounts = null;
		
		// open a session to the database
		Session session = this.getFactory().openSession();
		Transaction transaction = null;

		try {
			// start a transaction
			transaction = session.beginTransaction();
			// attempt to get daxtra account from the databse
			accounts = (List<DaxtraAccount>) session
					    .createQuery("from DaxtraAccount d ").list();
			
			
			// write the transaction
			transaction.commit();
		} catch(HibernateException e) {
			// if our transaction still lives, close it
			if(transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
		
			logger.error("Error for query Daxtra account: " + e);
		} finally {
			// close the session
			if(session.isOpen()) {
				session.close();
			}
		}
		return accounts;
	}
	
}
