package com.ctc.people.cfm.adaptor.aws.s3;

import org.apache.log4j.Logger;
import org.jets3t.service.S3Service;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.security.AWSCredentials;

import com.ctc.people.cfm.exception.S3Exception;




/**
 * This class provides connections to various entities
 * 
 * @author kwu
 *
 */
public class S3ConnectionFactory {
	static Logger logger = Logger.getLogger("com.ctc.people");
	private static S3ConnectionFactory connectionFactory = null;

	/**
	 * 
	 * @return
	 */
	public static S3ConnectionFactory getInstance() {
		if (connectionFactory == null) {
			connectionFactory = new S3ConnectionFactory();
		}
		return connectionFactory;
	}
	
	/**
	 * get s3 connection 
	 * given amazon credential
	 * 
	 * @param awsAccessKey
	 * @param awsSercretKey
	 * @return
	 */
	public S3Service getS3Connection(String awsAccessKey, String awsSercretKey){
		S3Service s3Service = null;
    	try{
        	AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey, awsSercretKey);
            s3Service = new RestS3Service(awsCredentials);
    	} catch (Exception ex ){
			throw new S3Exception("Failed to create connection to S3. "+ex);
    	}
    	
    	return s3Service;
	}
}
