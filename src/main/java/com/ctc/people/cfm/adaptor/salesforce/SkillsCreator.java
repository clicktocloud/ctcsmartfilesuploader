package com.ctc.people.cfm.adaptor.salesforce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.OwnerParcel;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 * this class create skills for a specific candidate
 * 
 * @author Andy
 *
 */
public class SkillsCreator implements Creator {
	static Logger logger = Logger.getLogger("com.ctc.people");
	
	/**
	 * create all skills of multiple owners per once
	 * @param conn
	 * @param parcels
	 * @return List<String> skills created successfully
	 */
	public static List<String> massUpsert (PartnerConnection conn, List<OwnerParcel> parcels)  {
		
		SkillsCreator skillsCrt = new SkillsCreator(conn, parcels );
		
		return skillsCrt.create( conn, parcels );
	}
	
	
	public static void create(PartnerConnection conn, CTCFile resume,
			HashMap<String, ArrayList<String>> skillsFromDaxtraParsing){

		if (skillsFromDaxtraParsing != null
				&& skillsFromDaxtraParsing.size() > 0) {
			
			SkillsCreator skillsCrt = new SkillsCreator(conn, resume, skillsFromDaxtraParsing);
			skillsCrt.create();
		}
	}
	
	
	private PartnerConnection conn;
	private List<OwnerParcel> ownerParcels;
	
	/**
	 * Constructor 
	 * @param conn
	 * @param resume
	 * @param skillsFromDaxtraParsing
	 */
	public SkillsCreator(PartnerConnection conn, CTCFile resume,
			HashMap<String, ArrayList<String>> skillsFromDaxtraParsing) {
		this.conn = conn;
		this.ownerParcels = new ArrayList<OwnerParcel>();
		
		OwnerParcel ownerParcel = new OwnerParcel();
		ownerParcel.setResume( resume );
		ownerParcel.setOwner( resume.getOwner() );
		
		Candidate c = new Candidate();
		c.setId( resume.getOwner().getWhoId() );
		c.setSkills( skillsFromDaxtraParsing );
		ownerParcel.setCandidate( c );
		
		ownerParcels.add(ownerParcel);
		
	}
	
	public SkillsCreator(PartnerConnection conn, List<OwnerParcel> ownerParcels){
		this.conn = conn;
		this.ownerParcels = ownerParcels;
	}

	
	@Override
	public void create() {
		
		create(this.conn, this.ownerParcels);
	
	}
	
	private  List<String> create(PartnerConnection conn, List<OwnerParcel> parcels)  {
		
		List<String> successSkills = new ArrayList<String>();
		
		if( parcels == null ){
			return null;
		}
		
		try {
			List<String> totalSkillSfIdList = new ArrayList<String>();
			
			List<SObject> sobjs = new ArrayList<SObject>();
			
			for(OwnerParcel parcel : parcels){
				
				
				//get skills belonging to specified skill groups
				ArrayList<String> filteredSkills = filterSkillsByGroups( parcel.getCandidate().getSkills(), parcel.getOwner().getSkillGroups());
				//get SF id of each skill
				List<String> skillSfIdList = null;
				if( filteredSkills != null && filteredSkills.size() > 0){
					skillSfIdList = SfHelper.getSkillSFids(filteredSkills, conn, parcel.getOwner().getNamespace());
				}
				
				
				//build the list of skill SObjects which will be saved to salesforce
				if(skillSfIdList != null){
					
					totalSkillSfIdList.addAll(skillSfIdList);
					
					sobjs.addAll( buildSkillSObjectListOfOneCandidate( skillSfIdList, parcel.getCandidate().getId(), parcel.getResume().getOwner().getNamespace()) );
					
				}	
			}
			
			if(totalSkillSfIdList.size() == 0){
				return null;
			}
			
			SaveResult[] srs =  SfHelper.massCreate(conn,sobjs.toArray(new SObject[]{}));
				
			
			for(int i=0; i<srs.length; i++){
				String skill = totalSkillSfIdList.get(i);
				if(srs[i].isSuccess()){
					
					//logger.debug("successful skill " + (i+1) +":" +  srs[i].getId()+" / " + skill );
					
					successSkills.add( skill );  
				}
				if(!srs[i].isSuccess()){
					com.sforce.soap.partner.Error[] errors = srs[i].getErrors();
					for(com.sforce.soap.partner.Error error : errors){
						logger.error("Error insert candidate skills "
								+ Arrays.toString(error.getFields())
								+ " : " + error.getMessage()
								+ ". candidate id: " + sobjs.get(i).getId());
					}
				}
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("Failed to execute mass creation of skills");
			logger.error(e);
		}	
		
		logger.debug("Completed mass creating skills [ "+successSkills.size()+" ]");
		
		return successSkills;
	}
	
	
	private static ArrayList<String> filterSkillsByGroups(HashMap<String,ArrayList<String>> skillsFromDaxtraParsing, List<String> skillGroupCriteriaFromSalesforce){
		
		ArrayList<String> filteredSkills = new ArrayList<String>();
		
		// if no specific skill groups selected, skip the method
		if (skillsFromDaxtraParsing == null
				|| skillGroupCriteriaFromSalesforce == null) {
			return filteredSkills;
		}
		
		try {
			for (String skillGroupNameFromDaxtra : skillsFromDaxtraParsing
					.keySet()) {
				// the skill group criteria from salesforce doesn't contain this
				// group, skip it
				if (!skillGroupCriteriaFromSalesforce
						.contains(skillGroupNameFromDaxtra)) {
					continue;
				}

				// get all external ids
				List<String> allSkillsFromThisGroup = skillsFromDaxtraParsing
						.get(skillGroupNameFromDaxtra);
				if( allSkillsFromThisGroup != null){
					filteredSkills.addAll( allSkillsFromThisGroup );
				}
				
			}

		} catch (Exception e) {
			logger.error(e);
			
		}
		
		return filteredSkills;
		
	}
	
	private static List<SObject> buildSkillSObjectListOfOneCandidate(List<String> skillSfIdList, String candidateId, String namespace){
		List<SObject> sobjs = new ArrayList<SObject>();
		for (String tempSkill : skillSfIdList) {
			SObject sobj = new SObject();
			sobj.setType(namespace + "Candidate_Skill__c");
			sobj.setField(namespace + "Candidate__c", candidateId );
			sobj.setField(namespace + "Skill__c", tempSkill);
			sobjs.add(sobj);
		}
		
		return sobjs;
	}
	
	
	
}
