package com.ctc.people.cfm.adaptor.aws.s3;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.jets3t.service.S3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;

import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.SforgEx;
import com.ctc.people.cfm.exception.S3Exception;

/**
 * this class uploads files to S3
 * 
 * @author kwu
 *
 */
public class FileUploader2S3 implements Uploader {
	
	public static void upload(SforgEx sforg, List<CTCFile> files){
		if (files == null || files.size() == 0) {
			return;
		}
		
		S3Service s3Service = S3ConnectionFactory.getInstance()
				.getS3Connection(sforg.getS3accesskey(),
						sforg.getS3secretkey());
		
		FileUploader2S3 uploader = new FileUploader2S3(s3Service,
				sforg.getBucketName(), files);
		
		uploader.upload();
	}
	
	private S3Service s3Service;
	private String bucketName;
	private List<CTCFile> files;
	static Logger logger = Logger.getLogger("com.ctc.people");


	/**
	 * constructor
	 * 
	 * @param s3Service
	 * @param bucketName
	 */
	public FileUploader2S3(S3Service s3Service, String bucketName, List<CTCFile> files) {
		this.s3Service = s3Service;
		this.bucketName = bucketName;
		this.files = files;
	}

	@Override
	public void upload() {
		logger.info("Start to upload files to S3.");
		
		if(files ==null || files.size()==0){
			throw new S3Exception("The list of files for uploading to Amazon S3 is empty.");
		}
		
		S3Bucket bucket = null;
		try {
			bucket = S3Helper.getBucketFromName(s3Service, bucketName);
		} catch (Exception ex) {
			logger.error("Failed to create S3 bucket. ");
			throw new S3Exception(
					"Failed to get S3 Bucket from S3. BucketName = "
							+ bucketName + ". " + ex);
		}

		for (CTCFile file : files) {
			
			try {
				File localFile = new File(file.getPath());
				S3Object fileObject = new S3Object(localFile);
				fileObject = S3Helper.setContentType(fileObject,localFile);
				fileObject = new S3Object(bucket, localFile);
				s3Service.putObject(bucket, fileObject);
				logger.info("Uploaded file = "+ localFile.getAbsolutePath());
			}catch (Exception e) {
				logger.error("Failed to upload files to S3. ");
				throw new S3Exception(
						"Failed to upload files to S3. " + e);
			}
			
		}
		logger.info("Completed uploading files to S3.");
	}

}
