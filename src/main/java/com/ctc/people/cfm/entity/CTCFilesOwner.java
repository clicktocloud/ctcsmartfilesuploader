package com.ctc.people.cfm.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CTCFilesOwner {
	
	
	private List<CTCFile> files = new ArrayList<CTCFile>();
	
	private String ownerId;
	private String orgId;
	private String namespace;
	private String whoId; 
	private String whoIdField;
	//private String whatId;
	//private String whatIdField;
	private String bucketName;
	
	private Map<String,String> whatMap;
	
	//optional
	private String taskId;
	
	private String userId;
	
	
	private List<String> skillGroups = new ArrayList<String>();
	
	private boolean enableSkillParsing =false;

	private SforgEx sforg ;
	
	//private List<JBObject> jbObjects = null;
	
	public CTCFilesOwner(){
		
	}

	public String getOwnerId() {
		return ownerId;
	}



	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}



	public List<CTCFile> getFiles() {
		return files;
	}

	

	public void setFiles(List<CTCFile> files) {
		this.files = files;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getWhoId() {
		return whoId;
	}

	public void setWhoId(String whoId) {
		this.whoId = whoId;
	}

	public String getWhoIdField() {
		return whoIdField;
	}

	public void setWhoIdField(String whoIdField) {
		this.whoIdField = whoIdField;
	}

//	public String getWhatId() {
//		return whatId;
//	}
//
//	public void setWhatId(String whatId) {
//		this.whatId = whatId;
//	}
//
//	public String getWhatIdField() {
//		return whatIdField;
//	}
//
//	public void setWhatIdField(String whatIdField) {
//		this.whatIdField = whatIdField;
//	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public SforgEx getSforg() {
		return sforg;
	}

	public void setSforg(SforgEx sforg) {
		this.sforg = sforg;
	}
	

	public List<String> getSkillGroups() {
		return skillGroups;
	}

	public void setSkillGroups(List<String> skillGroups) {
		this.skillGroups = skillGroups;
	}

	public boolean isEnableSkillParsing() {
		return enableSkillParsing;
	}

	public void setEnableSkillParsing(boolean enableSkillParsing) {
		this.enableSkillParsing = enableSkillParsing;
	}

	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}





	public Map<String, String> getWhatMap() {
		return whatMap;
	}



	public void setWhatMap(Map<String, String> whatMap) {
		this.whatMap = whatMap;
	}



//	public List<JBObject> getJbObjects() {
//		return jbObjects;
//	}
//
//
//
//	public void setJbObjects(List<JBObject> jbObjects) {
//		this.jbObjects = jbObjects;
//	}



	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bucketName == null) ? 0 : bucketName.hashCode());
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result + ((orgId == null) ? 0 : orgId.hashCode());
		result = prime * result + ((sforg == null) ? 0 : sforg.hashCode());
//		result = prime * result + ((whatId == null) ? 0 : whatId.hashCode());
//		result = prime * result
//				+ ((whatIdField == null) ? 0 : whatIdField.hashCode());
		result = prime * result + ((whoId == null) ? 0 : whoId.hashCode());
		result = prime * result
				+ ((whoIdField == null) ? 0 : whoIdField.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CTCFilesOwner other = (CTCFilesOwner) obj;
		if (bucketName == null) {
			if (other.bucketName != null)
				return false;
		} else if (!bucketName.equals(other.bucketName))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		if (orgId == null) {
			if (other.orgId != null)
				return false;
		} else if (!orgId.equals(other.orgId))
			return false;
		if (sforg == null) {
			if (other.sforg != null)
				return false;
		} else if (!sforg.equals(other.sforg))
			return false;
//		if (whatId == null) {
//			if (other.whatId != null)
//				return false;
//		} else if (!whatId.equals(other.whatId))
//			return false;
//		if (whatIdField == null) {
//			if (other.whatIdField != null)
//				return false;
//		} else if (!whatIdField.equals(other.whatIdField))
//			return false;
		if (whoId == null) {
			if (other.whoId != null)
				return false;
		} else if (!whoId.equals(other.whoId))
			return false;
		if (whoIdField == null) {
			if (other.whoIdField != null)
				return false;
		} else if (!whoIdField.equals(other.whoIdField))
			return false;
		return true;
	}
	

	
	

}
