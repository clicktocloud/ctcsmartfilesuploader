package com.ctc.people.cfm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * POJO Class for table Daxtradaxtraaccount 
 * In Recruitment database 
 * 
 * @author Lina
 *
 */


@Entity
@Table(name = "Daxtraaccount")
public class DaxtraAccount {
	
	@Id
	@Column(name = "orgId")
	private String orgId;
	@Column(name = "daxtraaccount")
	private String daxtraaccount;
	@Column(name = "daxtrawsdlurl")
	private String daxtrawsdlurl;
	
	public String getDaxtraaccount() {
		return daxtraaccount;
	}
	public void setDaxtraaccount(String daxtraAccount) {
		this.daxtraaccount = daxtraAccount;
	}
	public String getDaxtrawsdlurl() {
		return daxtrawsdlurl;
	}
	public void setDaxtrawsdlurl(String wsdlURL) {
		this.daxtrawsdlurl = wsdlURL;
	}
	
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
