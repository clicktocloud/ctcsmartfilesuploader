package com.ctc.people.cfm.entity;

import java.util.Map;

import com.ctc.entity.Candidate;

public class SfContactInsertFields extends SfContactUpdateFields{
	
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String mobilePhone;
	private String mailingStreet;
	private String mailingCity;
	private String mailingState;
	private String mailingPostalCode;
	private String mailingCountry;
	private String recordTypeId;
	
	
	public SfContactInsertFields(Candidate candidate, CTCFile resume , String recordType){
		super(candidate, resume);
		//firstName
		setFirstName(candidate.getFirstName());
		//lastName
		setLastName( candidate.getLastName());
		//email
		setEmail( candidate.getEmail());
		//phone
		setPhone( candidate.getPhone());
		//mobilePhone
		setMobilePhone( candidate.getMobile());
		//mailingStreet
		setMailingStreet( candidate.getStreet());
		//mailingCity
		setMailingCity( candidate.getCity());
		//mailingState
		setMailingState( candidate.getState());
		//mailingPostalCode
		setMailingPostalCode( candidate.getPostcode());
		//mailingCountry
		setMailingCountry( candidate.getFullCountry());
		
		if(recordType != null && !recordType.equals("")){
			setRecordTypeId(recordType);
		}
	
		
	}
	
	public Map<String, Object> buildFieldValuePairs(){
		Map<String, Object> fvpairs = super.buildFieldValuePairs();
		
		fvpairs.put("FirstName", getFirstName());
		fvpairs.put("LastName", getLastName());
		fvpairs.put("Email", getEmail());
		fvpairs.put("Phone", getPhone());
		fvpairs.put("MobilePhone", getMobilePhone());
		fvpairs.put("MailingStreet", getMailingStreet());
		fvpairs.put("MailingCity", getMailingCity());
		fvpairs.put("MailingState", getMailingState());
		fvpairs.put("MailingPostalCode", getMailingPostalCode());
		fvpairs.put("MailingCountry", getMailingCountry());
		fvpairs.put("RecordTypeId", getRecordTypeId());
		
		return fvpairs;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getMailingStreet() {
		return mailingStreet;
	}
	public void setMailingStreet(String mailingStreet) {
		this.mailingStreet = mailingStreet;
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	public String getMailingState() {
		return mailingState;
	}
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}
	public String getMailingPostalCode() {
		return mailingPostalCode;
	}
	public void setMailingPostalCode(String mailingPostalCode) {
		this.mailingPostalCode = mailingPostalCode;
	}
	public String getMailingCountry() {
		return mailingCountry;
	}
	public void setMailingCountry(String mailingCountry) {
		this.mailingCountry = mailingCountry;
	}

	public String getRecordTypeId() {
		return recordTypeId;
	}

	public void setRecordTypeId(String recordTypeId) {
		this.recordTypeId = recordTypeId;
	}
	
	
	
}
