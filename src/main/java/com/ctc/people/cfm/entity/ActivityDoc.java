/**
 *
 */
package com.ctc.people.cfm.entity;

import java.util.ArrayList;
import java.util.Map;



/**
 * @author ZX
 */

public class ActivityDoc {
	
	
	
	/**
	 * check if the org is using skill paring
	 * @param orgid
	 * @return
	 */
	public static Boolean isSkillParsing(String flag){
		if(flag.equalsIgnoreCase("true")){
			return true;
		}else{
			return false;
		}
	}
	
	public static String getWhoId( ActivityDoc doc){
		if(doc == null || doc.getWhoMap() == null){
			return "";
		}
		
		String whoId = "";
		if(doc.getWhoMap()!= null && doc.getWhoMap().size()>0) {
			for(String Id : doc.getWhoMap().keySet()) {
				whoId = Id;
			}
		}
		
		return whoId;
	}
	
    private String orgId;//
    private String activityId;
    private String docType;//
    private String contactId;//
    private String attachmentId;
    private String attachmentName;//
    private String amazonS3Folder;//
    private String nameSpace;//
    private String enableSkillParsing;//
    private Boolean isEmailService;
    private ArrayList<String> skillGroupIds;//
    private Map<String, String> whoMap;
    private Map<String, String> whatMap;
    
    //extend fileds for generic resources
    private String resourceId;
    private String resourceName;
    private String resourceType;
    
    //extend fields for userid
    private String userId;
    
  

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getAmazonS3Folder() {
        return amazonS3Folder;
    }

    public void setAmazonS3Folder(String amazonS3Folder) {
        this.amazonS3Folder = amazonS3Folder;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public ArrayList<String> getSkillGroupIds() {
        return skillGroupIds;
    }

    public void setSkillGroupIds(ArrayList<String> skillGroupIds) {
        this.skillGroupIds = skillGroupIds;
    }

    public String getEnableSkillParsing() {
        return enableSkillParsing;
    }

    public void setEnableSkillParsing(String enableSkillParsing) {
        this.enableSkillParsing = enableSkillParsing;
    }

    public Map<String, String> getWhoMap() {
        return whoMap;
    }

    public void setWhoMap(Map<String, String> whoMap) {
        this.whoMap = whoMap;
    }

    public Map<String, String> getWhatMap() {
        return whatMap;
    }

    public void setWhatMap(Map<String, String> whatMap) {
        this.whatMap = whatMap;
    }


    public Boolean getIsEmailService() {
        return isEmailService;
    }

    public void setIsEmailService(Boolean isEmailService) {
        this.isEmailService = isEmailService;
    }

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	


}
