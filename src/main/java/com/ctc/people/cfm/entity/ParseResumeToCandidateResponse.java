package com.ctc.people.cfm.entity;

/**
 * Created by andy.
 */
public class ParseResumeToCandidateResponse {
	private String code;
    private String candidateId;
    private Boolean isCandidateExist;
    private String message;
    
    public ParseResumeToCandidateResponse(String code, String message){
    	this.code = code;
    	this.message = message;
    	
    }
    
    public ParseResumeToCandidateResponse(){
    	
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public Boolean getIsCandidateExist() {
        return isCandidateExist;
    }

    public void setIsCandidateExist(Boolean isCandidateExist) {
        this.isCandidateExist = isCandidateExist;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
