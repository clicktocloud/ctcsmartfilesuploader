package com.ctc.people.cfm.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * This is the POJO class for the request send to DaxtraParsing Web Service for email service function
 *
 * @author lina
 *
 */
@JsonSerialize
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ParseResumeToCandidateRequest {
	
	@JsonProperty(value = "userId")
	private String userId;

    @JsonProperty(value = "orgId")
    private String orgId;

    @JsonProperty(value = "docType")
    private String docType;

    @JsonProperty(value = "attachmentName")
    private String attachmentName;
    
    @JsonProperty(value = "attachmentNameWithTimeStamp")
    private String attachmentNameWithTimeStamp;

    @JsonProperty(value = "bucketName")
    private String bucketName;

    @JsonProperty(value = "nsPrefix")
    private String nsPrefix;

    @JsonProperty(value = "enableSkillParsing")
    private boolean enableSkillParsing;

    @JsonProperty(value = "skillGroupIds")
    private List<String> skillGroupIds;

    @JsonProperty(value = "attachment")
    private byte[] attachment;             // convert the uploaded attachment into bytes
    
    @JsonProperty(value = "whoId")
    private String whoId;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getNsPrefix() {
        return nsPrefix;
    }

    public void setNsPrefix(String nsPrefix) {
        this.nsPrefix = nsPrefix;
    }

   

    public List<String> getSkillGroupIds() {
        return skillGroupIds;
    }

    public void setSkillGroupIds(List<String> skillGroupIds) {
        this.skillGroupIds = skillGroupIds;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

	public String getAttachmentNameWithTimeStamp() {
		return attachmentNameWithTimeStamp;
	}

	public void setAttachmentNameWithTimeStamp(String attachmentNameWithTimeStamp) {
		this.attachmentNameWithTimeStamp = attachmentNameWithTimeStamp;
	}

	public boolean isEnableSkillParsing() {
		return enableSkillParsing;
	}

	public void setEnableSkillParsing(boolean enableSkillParsing) {
		this.enableSkillParsing = enableSkillParsing;
	}

	public String getWhoId() {
		return whoId;
	}

	public void setWhoId(String whoId) {
		this.whoId = whoId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
