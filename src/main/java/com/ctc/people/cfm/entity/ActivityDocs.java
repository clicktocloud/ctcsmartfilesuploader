/**
 * 
 */
package com.ctc.people.cfm.entity;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ZX
 *
 */
@XmlRootElement	
public class ActivityDocs {
	private ArrayList<ActivityDoc> activityDocs=new ArrayList<ActivityDoc>();
	@XmlElement
	public void setActivityDoc(ArrayList<ActivityDoc> activityDocs){
		this.activityDocs= activityDocs;
	}
	public ArrayList<ActivityDoc> getActivityDocs(){
		return this.activityDocs;
	
	}

}
