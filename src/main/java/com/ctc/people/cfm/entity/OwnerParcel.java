package com.ctc.people.cfm.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ctc.entity.Candidate;

public  class OwnerParcel{
	
	public static List<String> getCandidateIds(List<OwnerParcel> parcels){
		List<String> ids = new ArrayList<String>();
		for( OwnerParcel parcel : parcels){
			
			
			if(parcel.getCandidate() != null && parcel.getCandidate().getId() != null && !parcel.getCandidate().getId().equals("")){
				ids.add(parcel.getCandidate().getId() );
			}
		}
		
		return ids;
	}
	public static List<OwnerParcel> getParcelsWithCandidate(List<OwnerParcel> parcels){
		List<OwnerParcel> parcelsWithCandidate = new ArrayList<OwnerParcel>();
		for( OwnerParcel parcel : parcels){
			if(parcel.getCandidate() != null){
				parcelsWithCandidate.add(parcel);
			}
		}
		
		return parcelsWithCandidate;
	}
	
	public static List<CTCFile> attachAllFilesToCandidates( List<OwnerParcel> parcels){
		List<CTCFile> files = new ArrayList<CTCFile>();
		if( parcels != null ){
			for( OwnerParcel parcel : parcels){
				
				parcel.getOwner().setWhoId( parcel.getCandidate().getId());
				
				if(StringUtils.isEmpty(parcel.getOwner().getWhoIdField())){
					parcel.getOwner().setWhoIdField(parcel.getOwner().getNamespace() + Constant.WEB_DOC_FIELD_WHO_ID);
				}	
				List<CTCFile> oneOwnerfiles = parcel.getOwner().getFiles();
				if( oneOwnerfiles != null && oneOwnerfiles.size() > 0){
					files.addAll(oneOwnerfiles);
				}
			}
		}
		
		return files;
	}
	
	public static List<CTCFile> getAllFiles( List<OwnerParcel> parcels){
		List<CTCFile> files = new ArrayList<CTCFile>();
		if( parcels != null ){
			for( OwnerParcel parcel : parcels){
				
				List<CTCFile> oneOwnerfiles = parcel.getOwner().getFiles();
				if( oneOwnerfiles != null && oneOwnerfiles.size() > 0){
					files.addAll(oneOwnerfiles);
				}
			}
		}
		
		return files;
	}
	
	
	
	private Candidate candidate;
	private CTCFilesOwner owner;
	private CTCFile resume;
	
	private List<String> skillSfIds;
	
	public OwnerParcel(){
		
	}
	
	public OwnerParcel(Candidate candidate , CTCFilesOwner owner){
		this.candidate = candidate;
		this.owner = owner;
		
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public CTCFilesOwner getOwner() {
		return owner;
	}

	public void setOwner(CTCFilesOwner owner) {
		this.owner = owner;
	}

	public List<String> getSkillSfIds() {
		return skillSfIds;
	}

	public void setSkillSfIds(List<String> skillSfIds) {
		this.skillSfIds = skillSfIds;
	}

	public CTCFile getResume() {
		return resume;
	}

	public void setResume(CTCFile resume) {
		this.resume = resume;
	}
	
}