package com.ctc.people.cfm.entity;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JBObject {
	
	
	private String objectName;
	
	private Map<String,JBField>  fieldsMap = new HashMap<String,JBField>();

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	
	public Collection<JBField> getFields() {
		return fieldsMap.values();
	}

	public void setFields(List<JBField> fields) {
		if(fields != null){
			for(JBField f : fields){
				fieldsMap.put(f.getName(), f);
			}
		}
		
	}
	
	public void addField(JBField field) {
		if(field != null && field.getName() != null){
			this.fieldsMap.put(field.getName(), field);
		}
		
	}
	
	public void addField(String name, Object value ) {
		addField( new JBField(name, value));
		
	}
	
	public JBObject setField(String name, Object value ) {
		addField( new JBField(name, value));
		return this;
		
	}
	

}
