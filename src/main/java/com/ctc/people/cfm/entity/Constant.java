package com.ctc.people.cfm.entity;

import java.io.File;

import com.ctc.people.cfm.util.CFMHelper;


/**
 * this class contains all constant variables 
 * @author kwu
 *
 */
public class Constant {
	public static final String ACCOUNT_ADMIN_NAME = "ctcpeopleAdmin(Do not Delete)";
	public static final String DEFAULT_CONTACT_RECORD_TYPE = "Independent_Candidate";
	final public static String PROPERTIES_FILENAME = "information.properties";
	final public static String RESUME_DOC_TYPE = "Resume";
	final public static String OTHER_DOC_TYPE = "Other Document";
	final public static String WEB_DOC_FIELD_WHO_ID = "Document_Related_To__c";
	final public static String TOMCAT_BASE = System.getProperty("catalina.base") == null ? "":System.getProperty("catalina.base");
	final public static String ATTACHMENT_FOLDER ;
	
	static{
		String tempFolder = CFMHelper.getValueFromPropertyFile("tempFolder", PROPERTIES_FILENAME);
		if(tempFolder != null){
			tempFolder = tempFolder.replace("${catalina.base}",TOMCAT_BASE);
			if(tempFolder.endsWith(File.separator)){
				
			}else{
				tempFolder = tempFolder + File.separator;
			}
			
			ATTACHMENT_FOLDER = tempFolder;
			
		}else{
			String f = System.getProperty("java.io.tmpdir");
			
			if(f.endsWith(File.separator)){
				
			}else{
				f = f + File.separator;
			}
			ATTACHMENT_FOLDER = f;
		}
		
	}
		
	
}

