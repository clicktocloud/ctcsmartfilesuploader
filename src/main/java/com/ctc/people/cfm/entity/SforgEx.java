package com.ctc.people.cfm.entity;

import com.ctc.entity.Sforg;
import com.sforce.soap.partner.PartnerConnection;

public class SforgEx extends Sforg {

	private boolean uploadOtherFilesEnabled;
	private boolean skillParsing;
	
	//only used to forward salesforce connection
	private PartnerConnection conn;

	public boolean isUploadOtherFilesEnabled() {
		return uploadOtherFilesEnabled;
	}

	public void setUploadOtherFilesEnabled(boolean uploadOtherFilesEnabled) {
		this.uploadOtherFilesEnabled = uploadOtherFilesEnabled;
	}
	

	
	@Deprecated
	public String getSfusername(){
		return super.getSfusername();
	}
	@Deprecated
	public String getSfpassword(){
		return super.getSfpassword();
	}
	@Deprecated
	public String getSftoken(){
		return super.getSftoken();
	}

	public boolean isSkillParsing() {
		return skillParsing;
	}

	public void setSkillParsing(boolean skillParsing) {
		this.skillParsing = skillParsing;
	}

	public PartnerConnection getConn() {
		return conn;
	}

	public void setConn(PartnerConnection conn) {
		this.conn = conn;
	}
}
