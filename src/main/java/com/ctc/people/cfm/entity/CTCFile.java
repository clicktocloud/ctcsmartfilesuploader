package com.ctc.people.cfm.entity;



public class CTCFile {
	
	private String resourceId;
	private String originalFileName;
	private String newFileName;
	private String alternativeName;
	private String type;
	private String size;
	private String path;
	private String id;
	
	private byte[] fileContent;
	
	private JBObject metadata;
	
	private CTCFilesOwner owner;
	
	
	
	private SforgEx sforg ;

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getNewFileName() {
		return newFileName;
	}

	public void setNewFileName(String newFileName) {
		this.newFileName = newFileName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


	public SforgEx getSforg() {
		return sforg;
	}

	public void setSforg(SforgEx sforg) {
		this.sforg = sforg;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public CTCFilesOwner getOwner() {
		return owner;
	}

	public void setOwner(CTCFilesOwner owner) {
		this.owner = owner;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public JBObject getMetadata() {
		return metadata;
	}

	public void setMetadata(JBObject metadata) {
		this.metadata = metadata;
	}

	public String getAlternativeName() {
		return alternativeName;
	}

	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}
	
	
	
}
