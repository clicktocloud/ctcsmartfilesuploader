package com.ctc.people.cfm.entity;

import java.util.Map;

public class SfTaskUpdateFields {
	
	
	
	private String id;
	private String whoId;
	private String whatIdsString="" ;
	private String status;
	private String description;
	private String namespace = "";
	private String ownerId = "";
	
	public SfTaskUpdateFields(String ownerId,String taskId, String whoId, Map<String,String> whatMap, String status, String description,String namespace){
		this( taskId,  whoId,  whatMap,  status,  description, namespace);
		setOwnerId(ownerId);
	}
	
	public SfTaskUpdateFields(String taskId, String whoId, Map<String,String> whatMap, String status, String description,String namespace){
		setId(taskId);
		setWhoId(whoId);
		
		setStatus(status);
		setDescription(description);
		setNamespace(namespace);
		if(whatMap != null){
			for(String whatId : whatMap.keySet()){
				if(whatId.equalsIgnoreCase("null")){
					continue;
				}
				
				if(whatIdsString == null || whatIdsString.equals("")){
					whatIdsString = whatId;
				}else{
					whatIdsString += ":" + whatId;
				}
			}
		}
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWhoId() {
		return whoId;
	}
	public void setWhoId(String whoId) {
		this.whoId = whoId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getWhatIdsString() {
		return whatIdsString;
	}

	public void setWhatIdsString(String whatIdsString) {
		this.whatIdsString = whatIdsString;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	
	
}
