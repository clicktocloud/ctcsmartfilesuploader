package com.ctc.people.cfm.entity;

import java.util.HashMap;
import java.util.Map;

import com.ctc.entity.Candidate;
import com.ctc.people.cfm.util.CFMHelper;

public class SfContactUpdateFields {
	private String whoId;
	private String whatId;
	private String fileName;
	private String content;
	private String skillGroups;
	
	private String namespace;
	
	public SfContactUpdateFields(Candidate candidate, CTCFile resume){
		//namespace
		setNamespace(resume.getOwner().getNamespace());
		//whoid
		if(candidate.getId() != null && ! candidate.getId().equals("")){
			setWhoId( candidate.getId() );
		}
		//fileName
		setFileName( resume.getOriginalFileName());

        //content
		String content =candidate.getResumeContent();
		if(content.length()>31990){
			content = content.substring(0, 31990);
		}
		setContent( content);
		//skillGroups
		setSkillGroups(CFMHelper.generateSkillGroupsString(resume.getOwner().getSkillGroups()));
		
		
	}
	
	public Map<String, Object> buildFieldValuePairs(){
		Map<String, Object> fvpairs = new HashMap<String, Object>();
		fvpairs.put(getNamespace() + "Resume__c", getContent());
		fvpairs.put(getNamespace() + "Resume_Source__c", getFileName());
		fvpairs.put(getNamespace() + "Skill_Group__c", getSkillGroups());
		
		if(getWhoId()!=null && !getWhoId().equals("")) {
			fvpairs.put("Id", getWhoId());
		}
		
		return fvpairs;
	}
	
	public String getWhoId() {
		return whoId;
	}
	public void setWhoId(String whoId) {
		this.whoId = whoId;
	}
	public String getWhatId() {
		return whatId;
	}
	public void setWhatId(String whatId) {
		this.whatId = whatId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSkillGroups() {
		return skillGroups;
	}
	public void setSkillGroups(String skillGroups) {
		this.skillGroups = skillGroups;
	}
	
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
}
